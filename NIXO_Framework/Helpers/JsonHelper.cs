﻿namespace NIXO_Framework.Helpers
{
    using Newtonsoft.Json;
    using System;
    using System.Globalization;
    using System.IO;
    using System.Text;
    public static class JsonHelper
    {
        /// <summary>
        /// Serializa el objeto a un JSON formateado para log, incluyendo el nombre del tipo cuando es un objeto compuesto.
        /// </summary>
        public static string SerializeForLog(object value)
        {
            if (value == null)
                return "";

            var jsonSerializer = new JsonSerializer()
            {
                TypeNameHandling = TypeNameHandling.Objects
                //ContractResolver = new AllPropertiesResolver()
            };

            var sb = new StringBuilder(128);
            using (var sw = new StringWriter(sb, CultureInfo.InvariantCulture))
            using (var jsonWriter = new JsonTextWriter(sw))
            {
                jsonWriter.Formatting = Formatting.None;

                try
                {
                    jsonSerializer.Serialize(jsonWriter, value);
                }
                catch (Exception)
                {
                    return "";
                }
                return sw.ToString();
            }
        }
    }
}
