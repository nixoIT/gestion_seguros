﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace NIXO_Framework.Services
{
    public static class Utiles
    {
        public static string GetDescription(this Enum e)
        {
            FieldInfo field = e.GetType().GetField(e.ToString());
            if (field != null)
            {
                object[] attribs =
                  field.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attribs.Length > 0)
                    return (attribs[0] as DescriptionAttribute).Description;
            }
            return e.ToString();
        }
    }
}
