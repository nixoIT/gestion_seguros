﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace NIXO_Framework
{
    public class Seguridad
    {
        public static string GetHashSha256(string Text)
        {
            SHA256 sha = new SHA256CryptoServiceProvider();
            byte[] dataBytes = Encoding.UTF8.GetBytes(Text);
            byte[] resultBytes = sha.ComputeHash(dataBytes);

            string hashString = string.Empty;
            foreach (byte x in resultBytes)
            {
                hashString += String.Format("{0:x2}", x);
            }

            return hashString;
        }

		public static string BlankPassword = GetHashSha256("Inicial1");
    }

    public enum Niveles
    {
        SysAdmin = 1,
        Administrador = 2,
        Empleado = 3,
        Productor = 4,
        Cliente = 5
    }
}
