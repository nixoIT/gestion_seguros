﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace NIXO_Framework.Services
{
    public class EmailManager
    {
        public static void AppSettings(out string UserID, out string Password, out string SMTPPort, out string Host)
        {
            UserID = ConfigurationManager.AppSettings.Get("UserID");
            Password = ConfigurationManager.AppSettings.Get("Password");
			SMTPPort = ConfigurationManager.AppSettings.Get("SMTPPort");
			Host = ConfigurationManager.AppSettings.Get("Host");
            
        }
        public static void SendEmail(string To, string Subject, string Body)
        {
			string from = ConfigurationManager.AppSettings["Credentials_From"];
			string pass = ConfigurationManager.AppSettings["Credentials_Password"];
			string smtpPort = ConfigurationManager.AppSettings["SMTPPort"];
			string host = ConfigurationManager.AppSettings["Host"];

			var mail = new System.Net.Mail.MailMessage();
            mail.To.Add(To);
            mail.From = new MailAddress(from);
            mail.Subject = Subject;
            mail.Body = Body;
            var smtp = new SmtpClient
            {
                Host = host,
                Port = Convert.ToInt16(smtpPort),
                Credentials = new NetworkCredential(from, pass),
                EnableSsl = true
            };
            smtp.Send(mail);
        }

		public static void SendEmailRecovery(string Subject, string Body)
		{
			string from = ConfigurationManager.AppSettings["Credentials_From"];
			string To = ConfigurationManager.AppSettings["Credentials_To"];
			string pass = ConfigurationManager.AppSettings["Credentials_Password"];
			string smtpPort = ConfigurationManager.AppSettings["SMTPPort"];
			string host = ConfigurationManager.AppSettings["Host"];

			var mail = new System.Net.Mail.MailMessage();
			mail.To.Add(To);
			mail.From = new MailAddress(from);
			mail.Subject = Subject;
			mail.Body = Body;
			mail.IsBodyHtml = true;
			var smtp = new SmtpClient
			{
				Host = host,
				Port = Convert.ToInt16(smtpPort),
				Credentials = new NetworkCredential(from, pass),
				EnableSsl = true,

			};
			smtp.Send(mail);
		}
	}
}
