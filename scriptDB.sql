USE [nps]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 10/31/2018 10:07:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Apellido] [nvarchar](100) NOT NULL,
	[Nombre] [nvarchar](100) NULL,
	[DomicilioCalle] [nvarchar](150) NOT NULL,
	[DomicilioNumero] [nvarchar](100) NOT NULL,
	[TelefonoFijo] [nchar](11) NULL,
	[TelefonoMovil] [nchar](10) NULL,
	[Email] [varchar](100) NULL,
	[IdMediosdeCobranza] [int] NULL,
	[DomicilioCobroCalle] [nvarchar](150) NULL,
	[DomicilioCobroNumero] [nvarchar](100) NULL,
	[Carpeta] [nvarchar](150) NULL,
	[FechaAlta] [datetime] NOT NULL,
	[Borrado] [datetime] NULL,
	[FechaNacimiento] [datetime] NULL,
	[DNI] [varchar](9) NULL,
	[CUIT] [varchar](11) NULL,
	[CodigoLocalidad] [int] NOT NULL,
	[IdGrupodeCobranza] [int] NULL,
	[EsCabezadeGrupo] [bit] NULL,
	[CodigoLocalidadCobro] [int] NULL,
	[CodigoProvincia] [int] NOT NULL,
	[CodigoProvinciaCobro] [int] NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 10/31/2018 10:07:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Cuit] [varchar](11) NULL,
	[Telefono] [varchar](14) NULL,
	[Direccion] [varchar](150) NULL,
	[Web] [varchar](300) NULL,
	[ActividadPrincipal] [varchar](50) NULL,
	[ActividadSecundaria] [varchar](50) NULL,
	[Borrado] [datetime] NULL,
 CONSTRAINT [PK_Empresas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Legajos]    Script Date: 10/31/2018 10:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Legajos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ubicacion] [varchar](200) NULL,
	[Path] [varchar](200) NULL,
	[Borrado] [datetime] NULL,
	[Nombre] [varchar](200) NOT NULL,
 CONSTRAINT [PK_Legajo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Localidades]    Script Date: 10/31/2018 10:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Localidades](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [int] NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[CodigoProvincia] [int] NOT NULL,
	[CodigoPostal] [varchar](8) NULL,
 CONSTRAINT [PK_Localidades] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MediosdeCobranza]    Script Date: 10/31/2018 10:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediosdeCobranza](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[Borrado] [datetime] NULL,
 CONSTRAINT [PK_MediosdeCobranza] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Niveles]    Script Date: 10/31/2018 10:07:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Niveles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nivel] [varchar](15) NOT NULL,
	[Borrado] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NivelesUsuarios]    Script Date: 10/31/2018 10:07:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NivelesUsuarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdNivel] [int] NOT NULL,
	[Borrado] [datetime] NULL,
 CONSTRAINT [PK_Niveles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObservacionesClientes]    Script Date: 10/31/2018 10:07:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ObservacionesClientes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Observacion] [varchar](max) NOT NULL,
	[Id_Cliente] [int] NOT NULL,
	[Borrado] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ObservacionesSiniestros]    Script Date: 10/31/2018 10:07:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ObservacionesSiniestros](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Observacion] [varchar](max) NOT NULL,
	[Id_Siniestro] [int] NOT NULL,
	[Borrado] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Polizas]    Script Date: 10/31/2018 10:07:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Polizas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Numero] [varchar](50) NULL,
	[IdCliente] [int] NOT NULL,
	[IdEmpresa] [int] NOT NULL,
	[Cobertura] [nvarchar](100) NOT NULL,
	[Suplemento] [int] NOT NULL,
	[Vencimiento] [datetime] NOT NULL,
	[Borrado] [datetime] NULL,
	[IdProductor] [int] NOT NULL,
	[Importada] [bit] NOT NULL,
	[IdRama] [int] NOT NULL,
 CONSTRAINT [PK_Polizas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productores]    Script Date: 10/31/2018 10:07:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productores](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Cuit] [varchar](11) NULL,
	[Telefono] [varchar](14) NULL,
	[TelefonoAlternativo] [varchar](14) NULL,
	[Direccion] [varchar](150) NULL,
	[Email] [varchar](100) NULL,
	[Web] [varchar](300) NULL,
	[ActividadPrincipal] [varchar](50) NULL,
	[ActividadSecundaria] [varchar](50) NULL,
	[Borrado] [datetime] NULL,
 CONSTRAINT [PK_Productores] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Provincias]    Script Date: 10/31/2018 10:07:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Provincias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Codigo] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Provincias] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ramas]    Script Date: 10/31/2018 10:07:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ramas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NOT NULL,
	[Borrado] [datetime] NULL,
	[Codigo] [int] NOT NULL,
	[Id_Empresa] [int] NOT NULL,
 CONSTRAINT [PK__Ramas__3214EC071F352448] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Requerimientos]    Script Date: 10/31/2018 10:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Requerimientos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Requerimiento] [varchar](50) NOT NULL,
	[Borrado] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RequerimientosEmpresas]    Script Date: 10/31/2018 10:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequerimientosEmpresas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Requerimiento] [int] NOT NULL,
	[Id_Empresa] [int] NOT NULL,
	[Borrado] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Siniestros]    Script Date: 10/31/2018 10:07:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Siniestros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NULL,
	[FechaSiniestro] [datetime] NULL,
	[idTipoSiniestro] [int] NULL,
	[ApellidoNombreTercero] [nvarchar](50) NULL,
	[TelefonoTercero] [nchar](11) NULL,
	[IdCompañiaTercero] [int] NULL,
	[HayDenuncia] [bit] NOT NULL,
	[HayFoto] [bit] NOT NULL,
	[HayTarjetaVerde] [bit] NOT NULL,
	[HayPresupuesto] [bit] NOT NULL,
	[FechaPresupuesto] [datetime] NULL,
	[HayCarnet] [bit] NOT NULL,
	[HayCertCobertura] [bit] NOT NULL,
	[Observaciones] [varchar](100) NULL,
	[Carpeta] [int] NULL,
	[PresentaReclamoTercero] [bit] NOT NULL,
	[Terminado] [bit] NOT NULL,
	[FechaFin] [datetime] NULL,
	[Borrado] [datetime] NULL,
 CONSTRAINT [PK_Siniestros] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SiniestrosRequerimientosEmpresas]    Script Date: 10/31/2018 10:07:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiniestrosRequerimientosEmpresas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdSiniestro] [int] NOT NULL,
	[IdRequerimientoEmpresa] [int] NOT NULL,
	[Valor] [bit] NOT NULL,
 CONSTRAINT [PK_SiniestrosRequerimientosEmpresas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TiposDeSiniestros]    Script Date: 10/31/2018 10:07:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TiposDeSiniestros](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NULL,
	[Borrado] [datetime] NOT NULL,
 CONSTRAINT [PK_TiposDeSiniestros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 10/31/2018 10:07:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](20) NOT NULL,
	[Apellido] [varchar](20) NOT NULL,
	[Borrado] [datetime] NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Pass] [varchar](100) NOT NULL,
 CONSTRAINT [PK__Usuarios__3214EC074D38B3D7] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Polizas] ADD  CONSTRAINT [DF_Polizas_Importada]  DEFAULT ((0)) FOR [Importada]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_Localidades] FOREIGN KEY([CodigoLocalidad])
REFERENCES [dbo].[Localidades] ([Codigo])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_Localidades]
GO
ALTER TABLE [dbo].[Clientes]  WITH CHECK ADD  CONSTRAINT [FK_Clientes_MediosdeCobranza] FOREIGN KEY([IdMediosdeCobranza])
REFERENCES [dbo].[MediosdeCobranza] ([Id])
GO
ALTER TABLE [dbo].[Clientes] CHECK CONSTRAINT [FK_Clientes_MediosdeCobranza]
GO
ALTER TABLE [dbo].[Localidades]  WITH CHECK ADD  CONSTRAINT [FK_Localidades_Provincias] FOREIGN KEY([CodigoProvincia])
REFERENCES [dbo].[Provincias] ([Codigo])
GO
ALTER TABLE [dbo].[Localidades] CHECK CONSTRAINT [FK_Localidades_Provincias]
GO
ALTER TABLE [dbo].[NivelesUsuarios]  WITH CHECK ADD  CONSTRAINT [Nivel] FOREIGN KEY([IdNivel])
REFERENCES [dbo].[Niveles] ([Id])
GO
ALTER TABLE [dbo].[NivelesUsuarios] CHECK CONSTRAINT [Nivel]
GO
ALTER TABLE [dbo].[NivelesUsuarios]  WITH CHECK ADD  CONSTRAINT [Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([Id])
GO
ALTER TABLE [dbo].[NivelesUsuarios] CHECK CONSTRAINT [Usuario]
GO
ALTER TABLE [dbo].[ObservacionesClientes]  WITH CHECK ADD  CONSTRAINT [FK_ObservacionesClientes_Cliente] FOREIGN KEY([Id_Cliente])
REFERENCES [dbo].[Clientes] ([Id])
GO
ALTER TABLE [dbo].[ObservacionesClientes] CHECK CONSTRAINT [FK_ObservacionesClientes_Cliente]
GO
ALTER TABLE [dbo].[Polizas]  WITH CHECK ADD  CONSTRAINT [FK_Polizas_Clientes] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Clientes] ([Id])
GO
ALTER TABLE [dbo].[Polizas] CHECK CONSTRAINT [FK_Polizas_Clientes]
GO
ALTER TABLE [dbo].[Polizas]  WITH CHECK ADD  CONSTRAINT [FK_Polizas_Empresas] FOREIGN KEY([IdEmpresa])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Polizas] CHECK CONSTRAINT [FK_Polizas_Empresas]
GO
ALTER TABLE [dbo].[Polizas]  WITH CHECK ADD  CONSTRAINT [FK_Polizas_Productores] FOREIGN KEY([IdProductor])
REFERENCES [dbo].[Productores] ([Id])
GO
ALTER TABLE [dbo].[Polizas] CHECK CONSTRAINT [FK_Polizas_Productores]
GO
ALTER TABLE [dbo].[Polizas]  WITH CHECK ADD  CONSTRAINT [FK_Polizas_Ramas] FOREIGN KEY([IdRama])
REFERENCES [dbo].[Ramas] ([Id])
GO
ALTER TABLE [dbo].[Polizas] CHECK CONSTRAINT [FK_Polizas_Ramas]
GO
ALTER TABLE [dbo].[Ramas]  WITH CHECK ADD  CONSTRAINT [FK_Ramas_Empresas] FOREIGN KEY([Id_Empresa])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Ramas] CHECK CONSTRAINT [FK_Ramas_Empresas]
GO
ALTER TABLE [dbo].[RequerimientosEmpresas]  WITH CHECK ADD  CONSTRAINT [FK_RequerimientosEmpresas_Empresas] FOREIGN KEY([Id_Empresa])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[RequerimientosEmpresas] CHECK CONSTRAINT [FK_RequerimientosEmpresas_Empresas]
GO
ALTER TABLE [dbo].[RequerimientosEmpresas]  WITH CHECK ADD  CONSTRAINT [FK_RequerimientosEmpresas_Requerimientos] FOREIGN KEY([Id_Requerimiento])
REFERENCES [dbo].[Requerimientos] ([Id])
GO
ALTER TABLE [dbo].[RequerimientosEmpresas] CHECK CONSTRAINT [FK_RequerimientosEmpresas_Requerimientos]
GO
ALTER TABLE [dbo].[Siniestros]  WITH CHECK ADD  CONSTRAINT [FK_Siniestros_Clientes] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Clientes] ([Id])
GO
ALTER TABLE [dbo].[Siniestros] CHECK CONSTRAINT [FK_Siniestros_Clientes]
GO
ALTER TABLE [dbo].[Siniestros]  WITH CHECK ADD  CONSTRAINT [FK_Siniestros_Empresas] FOREIGN KEY([IdCompañiaTercero])
REFERENCES [dbo].[Empresas] ([Id])
GO
ALTER TABLE [dbo].[Siniestros] CHECK CONSTRAINT [FK_Siniestros_Empresas]
GO
ALTER TABLE [dbo].[Siniestros]  WITH CHECK ADD  CONSTRAINT [FK_Siniestros_TiposDeSiniestros] FOREIGN KEY([idTipoSiniestro])
REFERENCES [dbo].[TiposDeSiniestros] ([Id])
GO
ALTER TABLE [dbo].[Siniestros] CHECK CONSTRAINT [FK_Siniestros_TiposDeSiniestros]
GO
ALTER TABLE [dbo].[SiniestrosRequerimientosEmpresas]  WITH CHECK ADD  CONSTRAINT [FK_SiniestrosRequerimientosEmpresas_RequerimientosEmpresas] FOREIGN KEY([IdRequerimientoEmpresa])
REFERENCES [dbo].[RequerimientosEmpresas] ([Id])
GO
ALTER TABLE [dbo].[SiniestrosRequerimientosEmpresas] CHECK CONSTRAINT [FK_SiniestrosRequerimientosEmpresas_RequerimientosEmpresas]
GO
ALTER TABLE [dbo].[SiniestrosRequerimientosEmpresas]  WITH CHECK ADD  CONSTRAINT [FK_SiniestrosRequerimientosEmpresas_Siniestros] FOREIGN KEY([IdSiniestro])
REFERENCES [dbo].[Siniestros] ([id])
GO
ALTER TABLE [dbo].[SiniestrosRequerimientosEmpresas] CHECK CONSTRAINT [FK_SiniestrosRequerimientosEmpresas_Siniestros]
GO
