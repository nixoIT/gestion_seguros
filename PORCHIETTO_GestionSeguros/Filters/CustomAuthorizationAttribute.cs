﻿using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Filters
{
    public class CustomAuthorizationAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) && !filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
            {
                // Check for authorization
                if (filterContext.HttpContext.Session["username"] == null)
                {
                    HandleUnauthorizedRequest(filterContext);
                }
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectResult("/Login?returnUrl=" + filterContext.HttpContext.Request.Url.ToString().Trim());

        }
    }
}