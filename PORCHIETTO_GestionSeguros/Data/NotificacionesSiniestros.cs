//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PORCHIETTO_GestionSeguros.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class NotificacionesSiniestros
    {
        public int id { get; set; }
        public int IdSiniestro { get; set; }
        public string Mensaje { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public Nullable<System.DateTime> FechaFin { get; set; }
        public Nullable<System.DateTime> Borrado { get; set; }
        public Nullable<int> IdUsuario { get; set; }
        public bool VisibleCliente { get; set; }
    
        public virtual Usuarios Usuarios { get; set; }
        public virtual Siniestros Siniestros { get; set; }
    }
}
