﻿$(function () {
    $('#FechaSiniestro').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
        locale: 'es'
    });

    $("select[name=IdEmpresa]").change(function () {

        var id = $('select[name=IdEmpresa]').val();

        $.ajax({
            type: "GET",
            async: false,
            url: "/Ramas/ObtenerRamasEmpresa",
            data: { IdEmpresa: id },
            success: function (data) {
                let dropdown = $('select[name=IdRama]');

                dropdown.empty();

                dropdown.append('<option selected="true" disabled>&lt;Seleccione una Rama&gt;</option>');
                dropdown.prop('selectedIndex', 0);

                $.each(data, function (key, entry) {
                    dropdown.append($('<option></option>').attr('value', entry.Value).text(entry.Text));
                });
            }
        });
    });

    $("select[name=IdCompañiaTercero]").change(function () {

        var id = $('select[name=IdCompañiaTercero]').val();

        $.ajax({
            type: "GET",
            async: false,
            url: "/Ramas/ObtenerRamasEmpresa",
            data: { IdEmpresa: id },
            success: function (data) {
                let dropdown = $('select[name=IdRamaTercero]');

                dropdown.empty();

                dropdown.append('<option selected="true" disabled>&lt;Seleccione una Rama&gt;</option>');
                dropdown.prop('selectedIndex', 0);

                $.each(data, function (key, entry) {
                    dropdown.append($('<option></option>').attr('value', entry.Value).text(entry.Text));
                });
            }
        });
    });

    //if ($('select[name=IdCompañiaTercero]').val() != null) {
    //    var id = $('select[name=IdCompañiaTercero]').val();

    //    $.ajax({
    //        type: "GET",
    //        async: false,
    //        url: "/Ramas/ObtenerRamasEmpresa",
    //        data: { IdEmpresa: id },
    //        success: function (data) {
    //            let dropdown = $('select[name=IdRamaTercero]');

    //            dropdown.empty();

    //            dropdown.append('<option selected="true" disabled>&lt;Seleccione una Rama&gt;</option>');
    //            dropdown.prop('selectedIndex', 0);

    //            $.each(data, function (key, entry) {
    //                dropdown.append($('<option></option>').attr('value', entry.Value).text(entry.Text));
    //            });
    //        }
    //    });
    //}

    $('#IdCliente').selectpicker({
        liveSearch: true,
        liveSearchPlaceholder: "Ingrese el nombre a buscar...",
        noneResultsText: "No se encontraron resultados para {0}"
    });
});