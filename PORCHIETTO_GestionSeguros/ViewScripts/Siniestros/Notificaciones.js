﻿$(function () {
    $('#fechaInicioNotif').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
        locale: 'es'
    });
    $('#fechaFinNotif').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
        locale: 'es'
    });

    $("#guardarNotificacion").click(function () {
        agregarNotificacion();
    });

    $("#guardarNotificacionCliente").click(function () {
        agregarNotificacionCliente();
    });
});

function EliminarNotificacionSiniestro(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Siniestros/EliminarNotificacionSiniestro",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
}

function agregarNotificacion() {
    var idSiniestro = $("#IdSiniestro").val();
    var fechaInicio = $("#fechaInicio").val();
    var fechaFin = $("#fechaFin").val();
    var mensaje = $("#mensaje").val();
    var cbxCliente = $('#cboxCliente')[0].checked;
    $.ajax({
        type: "POST",
        async: false,
        url: "/Siniestros/AgregarNotificacion",
        data: {
            IdSiniestro: idSiniestro,
            FechaInicio: fechaInicio,
            FechaFin: fechaFin,
            Mensaje: mensaje,
            VisibleCliente: cbxCliente
        },
        success: function (data) {
            $("#modalNotificaciones").modal("hide");
            location.reload();
        }
    });
}

function agregarNotificacionCliente() {
    var idSiniestro = $("#Id").val();
    var mensaje = $("#mensaje").val();
    $.ajax({
        type: "POST",
        async: false,
        url: "/Siniestros/AgregarNotificacionCliente",
        data: {
            IdSiniestro: idSiniestro,
            Mensaje: mensaje
        },
        success: function (data) {
            $("#modalNotificacionesCliente").modal("hide");
            location.reload();
        }
    });
}