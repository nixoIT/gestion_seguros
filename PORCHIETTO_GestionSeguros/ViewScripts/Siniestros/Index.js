﻿$.blockUI({
    message: 'Espere. Obteniendo Siniestros...',
    css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#111',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .8,
        color: '#fff'
    },
    overlayCSS: { backgroundColor: '#FFF' }
});

$(function () {
    setTimeout($.unblockUI, 1500);
});

function EliminarSiniestro(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Siniestros/EliminarSiniestro",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};

function FinalizarSiniestro(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Siniestros/FinalizarSiniestro",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};

function DetalleCliente(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Clientes/DatosDeContactoCliente",
        data: { IdCliente: id },
        success: function (data) {
            $("#cliente").text(data.Cliente);
            $("#telefono").text(data.Telefono);
            $("#celular").text(data.Celular);
            $("#email").text(data.Email);
            if (data.Celular == "") {
                $("#wsp").hide();
            }
            else {
                $("#wsp").show();
                $("#wsp").attr("href", "https://api.whatsapp.com/send?phone=54" + data.Celular);
            }
            $("#modalCliente").modal("show");
        }
    });
}

$(function () {
    $("[name=IdProductor]").change(function () {
        var id = $("[name=IdProductor]").val();
        if (id === '') {
            id = 0;
        }
        window.location.href = "/Siniestros/Index/" + id;
    });

});