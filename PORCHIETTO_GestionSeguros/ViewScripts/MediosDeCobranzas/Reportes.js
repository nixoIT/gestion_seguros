﻿$.blockUI({
    message: 'Espere. Obteniendo Datos...',
    css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#111',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .8,
        color: '#fff'
    },
    overlayCSS: { backgroundColor: '#FFF' }
});

$(function () {
    $("[name=IdMedio]").change(function () {
        var idMedio = $("[name=IdMedio]").val();
        var idProductor = $("[name=IdProductor]").val();
        if (idMedio === '') {
            idMedio = 0;
        }
        if (idProductor === '') {
            idProductor = 0;
        }
        window.location.href = "/MediosDeCobranzas/Reportes/" + idMedio + "/" + idProductor;

    });

    $("[name=IdProductor]").change(function () {
        var idMedio = $("[name=IdMedio]").val();
        var idProductor = $("[name=IdProductor]").val();
        if (idMedio === '') {
            idMedio = 0;
        }
        if (idProductor === '') {
            idProductor = 0;
        }
        window.location.href = "/MediosDeCobranzas/Reportes/" + idMedio + "/" + idProductor;

    });



    //Para el PDF
    $("#export").click(function () {
        var MedioValue = $('#ddpMedio option:selected').val();
        if (MedioValue === '') {
            MedioValue = 0;
        }
        var ProductorValue = $('#ddpProductor option:selected').val();
        if (ProductorValue === '' || ProductorValue === undefined) {
            ProductorValue = 0;
        }
        var url = $("#RedirectPDF").val();
        if (!window.open(url.replace('__idMedio__', MedioValue).replace('__idProductor__', ProductorValue), 'Reporte PDF')) {
            alert("No se ha podido mostrar el PDF. Permita ventanas emergentes para esta sitio.");
        }
        location.reload();
    });

    setTimeout($.unblockUI, 1500);
});