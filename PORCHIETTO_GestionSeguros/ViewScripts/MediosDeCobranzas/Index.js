﻿function EliminarMedio(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/MediosDeCobranzas/EliminarMediosDeCobranza",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};