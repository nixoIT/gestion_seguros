﻿function EliminarCliente(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Usuarios/EliminarUsuario",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};

function BlanquearPassModal(id, usuario) {
    $("#idUsuario").text(id);
    $("#usuario").text(usuario);
    document.getElementById('OpenModal').click();
};

function BlanquearPass() {
    let id = $("#idUsuario").text();
    $.ajax({
        type: "GET",
        async: false,
        url: "/Usuarios/BlanquearPass",
        data: { Id: id },
        success: function (data) {
            document.getElementById('CloseModal').click();
            console.log('password blanqueada correctamente idUsuario:' + id);
        }
    });
};