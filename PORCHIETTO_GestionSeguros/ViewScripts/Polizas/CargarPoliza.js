﻿$(function () {
    $('#cargararchivo').click(function () {
        $('#errorlabel').html('');

        if ($('#errorlabel').val() !== undefined && $('#errorlabel').val() !== '') {
            $.blockUI({
                message: 'Espere. Procesando...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#111',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .8,
                    color: '#fff'
                },
                overlayCSS: { backgroundColor: '#FFF' }
            });
        }
    });

    $('#errorlabel').change(function () {
        $.unblockUI();
    });
});