﻿$(function () {
    $('#fechaInicioNotif').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
        locale: 'es'
    });
    $('#fechaFinNotif').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
        locale: 'es'
    });

    $("#guardarNotificacion").click(function () {
        agregarNotificacion();
    });
});

function EliminarNotificacionPolizas(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Polizas/EliminarNotificacionPoliza",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
}

function agregarNotificacion() {
    var idPoliza = $("#IdPoliza").val();
    var fechaInicio = $("#fechaInicio").val();
    var fechaFin = $("#fechaFin").val();
    var mensaje = $("#mensaje").val();
    var cbxCliente = $('#cboxCliente')[0].checked;
    $.ajax({
        type: "POST",
        async: false,
        url: "/Polizas/AgregarNotificacion",
        data: {
            IdPoliza: idPoliza,
            FechaInicio: fechaInicio,
            FechaFin: fechaFin,
            Mensaje: mensaje,
            VisibleCliente: cbxCliente
        },
        success: function (data) {
            $("#modalNotificaciones").modal("hide");
            location.reload();
        }
    });
}