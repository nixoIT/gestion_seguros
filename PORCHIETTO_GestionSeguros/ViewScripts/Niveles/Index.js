﻿$(function () {
    $("[name=EliminarNivel]").click(function () {
        var id = $(this).parent().parent().parent().attr("id");
        var idUsuario = $("#idUsuario").text();
        $.ajax({
            type: "GET",
            async: false,
            url: "/Usuarios/EliminarNivelUsuario",
            data: { IdUsuario: idUsuario, IdNivel: id },
            success: function (data) {
                $("#" + id).remove();
            }
        });
    });
});