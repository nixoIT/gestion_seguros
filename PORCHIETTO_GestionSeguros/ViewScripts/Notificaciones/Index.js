﻿var MarcarResueltos = [];

$.blockUI({
    message: 'Espere. Obteniendo Vencimientos...',
    css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#111',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .8,
        color: '#fff'
    },
    overlayCSS: { backgroundColor: '#FFF' }
});

$(function () {
    setTimeout($.unblockUI, 1500);

    $('#TablaExtendida').on('check.bs.table', function (e, row) {
        MarcarResueltos.push({ IdNotificacion: row.id, IdTipoNotificacion: row.idtipo });
    });

    $("[name=Tipo]").change(function () {
        var id = $("[name=Tipo]").val();
        if (id === '') {
            id = 0;
        }

        window.location.href = id;
        //$.ajax({
        //    type: "POST",
        //    async: false,
        //    url: "/Notificaciones/Informes",
        //    data: { Id: id },
        //    success: function (data) {
        //        $('#TablaExtendida').remove();
        //        $('#TablaExtendida').html(data);
        //        $('#TablaExtendida').bootstrapTable('refresh');
        //    }
        //});
    });
});

function ExportaXLS() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth() + 1; //hoy es 0!
    var yyyy = hoy.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    hoy = dd + '/' + mm + '/' + yyyy;
    $('#TablaExtendida').bootstrapTable('togglePagination');
    $('#TablaExtendida').tableExport({
        type: 'excel',
        tableName: 'Vencimiento Polizas',
        worksheetName: 'Vencimiento Polizas',
        fileName: 'Vencimientos_' + hoy,
        headerStyles: {
            fillColor: [52, 73, 94],
            textColor: 255,
            fontStyle: 'bold',
            halign: 'center'
        },
        tableWidth: 'auto'
    });
}

function MarcarNotificacionesResueltas() {
    if (MarcarResueltos.length > 0) {
        marked = JSON.stringify({ 'request': MarcarResueltos });
        $.ajax({
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            type: 'POST',
            url: "/Notificaciones/MarcarNotificacionesResueltas",
            data: marked,
            success: function () {
                location.reload();
            }
        });
    }
}