﻿function EliminarRequerimiento(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Requerimientos/EliminarRequerimiento",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};

function EliminarRequerimientoEmpresa(id, idEmpresa) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Requerimientos/EliminarRequerimientoEmpresa",
        data: { Id: id, IdEmpresa: idEmpresa },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};