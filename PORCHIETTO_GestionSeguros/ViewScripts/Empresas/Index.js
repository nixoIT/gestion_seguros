﻿function EliminarRamaEmpresa(id, idEmpresa) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Empresas/EliminarRamaEmpresa",
        data: { IdEmpresa: idEmpresa, IdRama: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};

function EliminarEmpresa(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Empresas/EliminarEmpresa",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};