﻿$(function () {
    $.ajax({
        type: "GET",
        url: "/Home/ObtenerNotificaciones",
        success: function (result) {
            if (result) {
                result.forEach(function (value, index) {
                    Notificar(value.Titulo, value.Mensaje, value.Aviso, value.URL);
                });
            }
        },
        error: function (req, status, error) {
            Notificar('', 'Error obteniendo vencimientos...', 'danger', '');
            //alert("Error");
        }
    });
});

function Notificar(titulo, mensaje, aviso, url) {
    if (mensaje !== '') {
        var title = titulo;
        if (title === '') {
            title = 'NOTIFICACIÓN GESTIÓN';
        }

        $.notify({
            title: '<strong>' + title + '</strong>',
            message: mensaje,
            url: url,
            target: '_self'
        }, {
                type: 'pastel-' + aviso,
                delay: 8000,
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "right"
                },
                mouse_over: 'pause',
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button><span data-notify="title">{1}</span><span data-notify="message">{2}</span><a href="{3}" target="{4}" data-notify="url"></a></div>'
            });
    }
}