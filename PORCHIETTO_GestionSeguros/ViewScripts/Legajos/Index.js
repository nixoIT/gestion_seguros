﻿function EliminarLegajo(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Legajos/EliminarLegajo",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};