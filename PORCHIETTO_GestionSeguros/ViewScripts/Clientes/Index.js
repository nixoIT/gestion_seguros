﻿
$.blockUI({
    message: 'Espere. Obteniendo Clientes...',
    css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#111',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .8,
        color: '#fff'
    },
    overlayCSS: { backgroundColor: '#FFF' }
});

$(function () {
    $("[name=EliminarCliente]").click(function () {
        var id = $(this).parent().parent().parent().attr("id");
        $.ajax({
            type: "GET",
            async: false,
            url: "/Clientes/EliminarCliente",
            data: { Id: id },
            success: function (data) {
                $("#" + id).remove();
            }
        });
    });

    setTimeout($.unblockUI, 1500);
});

function EliminarCliente(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Clientes/EliminarCliente",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};

$(function () {
    $("[name=IdProductor]").change(function () {
        var id = $("[name=IdProductor]").val();
        if (id === '') {
            id = 0;
        }
        window.location.href = "/Clientes/Index/" + id;

    });

});