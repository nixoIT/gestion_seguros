﻿$(function () {
    if ($("[name=EsCabezadeGrupo]").is(':checked')) {
        $("[name=IdGrupodeCobranza]").attr("readonly", "readonly");
    }

    $("[name=IdGrupodeCobranza]").change(function () {
        if ($(this).val() != "") {
            getDomicilioCobro($("[name=IdGrupodeCobranza]").val());
            //Deshabilito campos de domicilio de cobro
            $("[name=DomicilioCobroCalle]").attr("readonly", "readonly");
            $("[name=DomicilioCobroNumero]").attr("readonly", "readonly");
            $("[name=CodigoProvinciaCobro]").attr("readonly", "readonly");
            $("[name=CodigoLocalidadCobro]").attr("readonly", "readonly");
        }
        else {
            //Habilito campos de domicilio de cobro
            $("[name=DomicilioCobroCalle]").attr("readonly", false);
            $("[name=DomicilioCobroNumero]").attr("readonly", false);
            $("[name=CodigoProvinciaCobro]").attr("readonly", false);
            $("[name=CodigoLocalidadCobro]").attr("readonly", false);
        }

    });

    //Sino tiene Domicilio de cobro a declararse como Referente de pago, coloco el domicilio real.
    $("[name=EsCabezadeGrupo]").change(function () {
        if (this.checked) {
            $("[name=IdGrupodeCobranza]").val("");
            $("[name=IdGrupodeCobranza]").attr("readonly", "readonly");

            if ($("[name=DomicilioCobroCalle]").val() == "") {
                getLocalidades($("[name=CodigoProvincia]").val(), "cobro");
                $("[name=CodigoProvinciaCobro]").val($("[name=CodigoProvincia]").val());
                $("[name=DomicilioCobroCalle]").val($("[name=DomicilioCalle]").val());
                $("[name=DomicilioCobroNumero]").val($("[name=DomicilioNumero]").val());
                setTimeout(function () {
                    $("[name=CodigoLocalidadCobro]").val($("[name=CodigoLocalidad]").val());
                }, 300);
            }

            //Habilito campos de domicilio de cobro
            $("[name=DomicilioCobroCalle]").attr("readonly", false);
            $("[name=DomicilioCobroNumero]").attr("readonly", false);
            $("[name=CodigoProvinciaCobro]").attr("readonly", false);
            $("[name=CodigoLocalidadCobro]").attr("readonly", false);
        }
        else {
            $("[name=IdGrupodeCobranza]").val("");
            $("[name=IdGrupodeCobranza]").attr("readonly", false);
        }
    });

    $("[name=CodigoProvinciaCobro]").on('change', function () {
        if ($(this).val() != "") {
            $("[name=CodigoLocalidadCobro]").val("");
            getLocalidades($(this).val(), "cobro");
        }
        else {
            $("[name=CodigoLocalidadCobro]").html("<option value=''>&lt;Seleccione una Localidad&gt;</option>");
            $("[name=CodigoLocalidadCobro]").val("");
        }
    });

    $("[name=CodigoProvincia]").on('change', function () {
        if ($(this).val() != "") {
            $("[name=CodigoLocalidad]").val("");
            getLocalidades($(this).val(), "real");
        }
        else {
            $("[name=CodigoLocalidad]").html("<option value=''>&lt;Seleccione una Localidad&gt;</option>");
            $("[name=CodigoLocalidad]").val("");
        }
    });

    $('#fechanacimiento').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY',
        locale: 'es'
    });

    //Funciones AJAX
    function getLocalidades(codProv, origen) {
        var prov = codProv;
        $.ajax({
            type: "POST",
            url: "/Clientes/ObtenerLocalidades",
            data: { CodigoProvincia: codProv },
            success: function (json) {
                var list = JSON.parse(json);
                var options = "<option value=''>&lt;Seleccione una Localidad&gt;</option>";
                for (var i = 0; i < list.length; i++) {
                    options += "<option value='" + list[i].Codigo + "'>" + list[i].Nombre + "</option>";
                }

                if (origen == "cobro") {
                    $("[name=CodigoLocalidadCobro]").html(options);
                }
                else {
                    $("[name=CodigoLocalidad]").html(options);
                }
            },
        });
    };

    function getDomicilioCobro(IdReferente) {
        $.ajax({
            type: "POST",
            url: "/Clientes/ObtenerDomicilioReferente",
            data: { IdReferente: IdReferente },
            success: function (json) {
                var dato = JSON.parse(json);
                getLocalidades(dato.CodigoProvincia, "cobro");
                $("[name=DomicilioCobroCalle]").val(dato.DomicilioCalle);
                $("[name=DomicilioCobroNumero]").val(dato.DomicilioNumero);
                $("[name=CodigoProvinciaCobro]").val(dato.CodigoProvincia);
                setTimeout(function () {
                    $("[name=CodigoLocalidadCobro]").val(dato.CodigoLocalidad);
                }, 300);
            },
        });
    };
});