﻿function EliminarProductor(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Productores/EliminarProductor",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};