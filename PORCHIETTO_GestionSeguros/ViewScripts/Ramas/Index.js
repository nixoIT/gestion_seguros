﻿function EliminarRama(id) {
    $.ajax({
        type: "GET",
        async: false,
        url: "/Ramas/EliminarRama",
        data: { Id: id },
        success: function (data) {
            $("#" + id).remove();
        }
    });
};