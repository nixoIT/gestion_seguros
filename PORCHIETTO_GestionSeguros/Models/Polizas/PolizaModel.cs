﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Polizas
{

    public class PolizaModel
    {
        public List<AgregarPolizaModel> Lista { get; set; }
        
        [Display(Name = "Seleccionar Productor")]
        public int? IdProductor { get; set; }
    }

    public class PolizaListadoRequest
    {
        public int? IdProductor { get; set; }
    }

    public class AgregarPolizaModel
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        [Display(Name = "Cliente")]
        [Range(1, int.MaxValue)]
        public int IdCliente { get; set; }
        public string NombreCliente { get; set; }
        [Display(Name = "Empresa")]
        [Range(1, int.MaxValue)]
        public int IdEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        [Required]
        public string Cobertura { get; set; }
        public string NombreCobertura { get; set; }
        [Required]
        public int Suplemento { get; set; }
        [Required]
        public DateTime Vencimiento { get; set; }
        [Display(Name = "Productor")]
        [Range(1, int.MaxValue)]
        public int IdProductor { get; set; }
        public string NombreProductor { get; set; }
        [Display(Name = "Rama")]
        [Range(1, int.MaxValue)]
        public int IdRama { get; set; }
        public string NombreTipoPoliza { get; set; }
        public bool Importada { get; set; }
        public DateTime? Borrado { get; set; }
        public string Dominio { get; set; }
        [Display(Name = "Unidad/Nómina")]
        public string Unidad_Nomina { get; set; }
    }

    public class AgregarPolizaRequest
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public int IdCliente { get; set; }
        public int IdEmpresa { get; set; }
        public string Cobertura { get; set; }
        public int Suplemento { get; set; }
        public DateTime Vencimiento { get; set; }
        public int IdProductor { get; set; }
        public int IdRama { get; set; }
        public string Dominio { get; set; }
        public string Unidad_Nomina { get; set; }
    }

    public class ImportarPolizaModel
    {
        public string Numero { get; set; }
        public int IdCliente { get; set; }
        public int IdEmpresa { get; set; }
        public int Suplemento { get; set; }
        public DateTime Vencimiento { get; set; }
        public int IdProductor { get; set; }
        public int IdRama { get; set; }
        public string Cobertura { get; set; }
    }

    public class AgregarPolizaResponse
    {
        public int Id { get; set; }
    }

    public class ModificarPolizaModel
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        [Display(Name = "Cliente")]
        [Range(1, int.MaxValue)]
        public int IdCliente { get; set; }
        [Display(Name = "Empresa")]
        [Range(1, int.MaxValue)]
        public int IdEmpresa { get; set; }
        public string Cobertura { get; set; }
        public int Suplemento { get; set; }
        [Required]
        public DateTime Vencimiento { get; set; }
        [Display(Name = "Productor")]
        [Range(1, int.MaxValue)]
        public int IdProductor { get; set; }
        [Display(Name = "Rama")]
        [Range(1, int.MaxValue)]
        public int IdRama { get; set; }
        public string Dominio { get; set; }
        [Display(Name = "Unidad/Nómina")]
        public string Unidad_Nomina { get; set; }
    }

    public class ModificarPolizaRequest
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public int IdCliente { get; set; }
        public int IdEmpresa { get; set; }
        public string Cobertura { get; set; }
        public int Suplemento { get; set; }
        public DateTime Vencimiento { get; set; }
        public int IdProductor { get; set; }
        public int IdRama { get; set; }
        public string Dominio { get; set; }
        public string Unidad_Nomina { get; set; }
    }

    public class ModificarPolizaResponse
    {
        public int Id { get; set; }
    }

    public class NotificacionesPolizasModel
    {
        public int IdPoliza { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime? FechaFinal { get; set; }

        public string Mensaje { get; set; }

        public bool VisibleCliente { get; set; }

        public int IdUsuario { get; set; }
    }
}