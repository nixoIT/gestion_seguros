﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace PORCHIETTO_GestionSeguros.Models.Polizas
{
    public class CargarPoliza
    {
        [Display(Name = "Empresa")]
        [Range(1, int.MaxValue)]
        public int IdEmpresa { get; set; }

        [Display(Name = "Productor")]
        [Range(1, int.MaxValue)]
        public int IdProductor { get; set; }

        [Required(ErrorMessage = "Seleccione un archivo.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.xlsx)$", ErrorMessage = "Sólo se permiten archivos Excel .XLSX")]
        [Display(Name ="Cargar Plantilla")]
        public HttpPostedFileBase PostedFile { get; set; }
    }

    public enum TipoPlantilla
    {
        POLIZAS = 1,
        PRODUCTORES = 2,
        CLIENTES = 3
    }
}