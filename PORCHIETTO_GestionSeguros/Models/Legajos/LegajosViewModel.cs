﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Legajos
{
    public class LegajosViewModel
    {
        public List<AgregarLegajoModel> Lista { get; set; }
    }

    public class AgregarLegajoModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [Display(Name = "Ubicación Física")]
        public string Ubicacion { get; set; }
        [Display(Name = "Ubicación")]
        public string Path { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class ModificarLegajoModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [Display(Name = "Ubicación Física")]
        public string Ubicacion { get; set; }
        [Display(Name = "Ubicación")]
        public string Path { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class AgregarLegajoRequest
    {
        public string Nombre { get; set; }
        public string Ubicacion { get; set; }
        public string Path { get; set; }
    }

    public class AgregarLegajoResponse
    {

    }

    public class ModificarLegajoRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Ubicacion { get; set; }
        public string Path { get; set; }
    }

    public class ModificarLegajoResponse
    {

    }
}