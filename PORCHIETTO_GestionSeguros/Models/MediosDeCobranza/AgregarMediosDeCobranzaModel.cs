﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.MediosDeCobranza
{
    public class AgregarMediosDeCobranzaModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre")]
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class AgregarMediosDeCobranzaRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class AgregarMediosDeCobranzaResponse
    {
        public int Id { get; set; }
    }
}