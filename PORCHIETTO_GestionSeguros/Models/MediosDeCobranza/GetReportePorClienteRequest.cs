﻿namespace PORCHIETTO_GestionSeguros.Models.MediosDeCobranza
{
    public class GetReportePorClienteRequest
    {
        public int? IdMedioDePago { get; set; }
        public int? IdProductor { get; set; }
    }
}