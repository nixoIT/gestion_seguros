﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.MediosDeCobranza
{
    public class ModificarMediosDeCobranzaModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre")]
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class ModificarMediosDeCobranzaRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class ModificarMediosDeCobranzaResponse
    {
        public int Id { get; set; }
    }
}