﻿using PORCHIETTO_GestionSeguros.Models.Clientes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.MediosDeCobranza
{
    public class ReporteClientesModel
    {
        [Display(Name = "Medio de Pago")]
        public int? IdMedio { get; set; }

        [Display(Name = "Productor")]
        public int? IdProductor { get; set; }
        public List<ReporteCliente> Lista { get; set; }
    }

    public class ReporteCliente
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public bool ReferentePago { get; set; }
        public List<string> AsociadosAReferentePago { get; set; }
        public string NombreMediodeCobranza { get; set; }
        public string Poliza { get; set; }
    }
}