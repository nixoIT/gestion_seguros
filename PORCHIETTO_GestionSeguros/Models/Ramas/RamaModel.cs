﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PORCHIETTO_GestionSeguros.Models.Ramas
{
    public class RamaModel
    {
        public List<AgregarRamaModel> Lista { get; set; }
    }

    public class AgregarRamaModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre")]
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
        public int Codigo { get; set; }
        public int Id_Empresa { get; set; }
    }

    public class AgregarRamaRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
        public int Codigo { get; set; }
        public int Id_Empresa { get; set; }
    }

    public class AgregarRamaResponse
    {
        public int Id { get; set; }
    }

    public class ModificarRamaModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre")]
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
        public int Codigo { get; set; }
        public int Id_Empresa { get; set; }
    }

    public class ModificarRamaRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
        public int Codigo { get; set; }
        public int Id_Empresa { get; set; }
    }

    public class ModificarRamaResponse
    {
        public int Id { get; set; }
    }
}