﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Siniestros
{
    public class SiniestroModel
    {
        public List<AgregarSiniestroModel> Lista { get; set; }

        [Display(Name = "Seleccionar Productor")]
        public int? IdProductor { get; set; }
    }
    public class SiniestroListadoRequest
    {
        public int? IdProductor { get; set; }
    }
    public class AgregarSiniestroModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Cliente")]
        public int? IdCliente { get; set; }
        public string NombreCliente { get; set; }
        [Display(Name = "Fecha del Siniestro")]
        public DateTime? FechaSiniestro { get; set; }
        [Required]
        [Display(Name = "Rama de Siniestro")]
        public int IdRama { get; set; }
        public string Rama { get; set; }
        [Required]
        [Display(Name = "Empresa")]
        public int IdEmpresa { get; set; }
        [Display(Name = "Apellido y Nombre del Tercero")]
        public string ApellidoNombreTercero { get; set; }
        [Display(Name = "Telefono del Tercero")]
        public string TelefonoTercero { get; set; }
        [Display(Name = "Compañia del Tercero")]
        public int? IdCompañiaTercero { get; set; }
        [Display(Name = "Rama de Siniestro Tercero")]
        public int? IdRamaTercero { get; set; }
        public string RamaTercero { get; set; }
        [Display(Name = "Fecha del Presupuesto")]
        public DateTime? FechaPresupuesto { get; set; }
        public string Observaciones { get; set; }
        public int? Carpeta { get; set; }
        [Display(Name = "El Tercero Presenta Reclamo?")]
        public bool PresentaReclamoTercero { get; set; }
        public bool Terminado { get; set; }
        public DateTime? FechaFin { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class AgregarSiniestroRequest
    {
        public int Id { get; set; }
        public int? IdCliente { get; set; }
        public DateTime? FechaSiniestro { get; set; }
        public int IdRama { get; set; }
        public int IdEmpresa { get; set; }
        public string ApellidoNombreTercero { get; set; }
        public string TelefonoTercero { get; set; }
        public int? IdCompañiaTercero { get; set; }
        public int? IdRamaTercero { get; set; }
        public string RamaTercero { get; set; }
        public DateTime? FechaPresupuesto { get; set; }
        public string Observaciones { get; set; }
        public int? Carpeta { get; set; }
        public bool PresentaReclamoTercero { get; set; }
        public bool Terminado { get; set; }
        public DateTime? FechaFin { get; set; }
    }

    public class AgregarSiniestroResponse
    {
        public int Id { get; set; }
    }

    public class ModificarSiniestroRequest
    {
        public int Id { get; set; }
        public int? IdCliente { get; set; }
        public DateTime? FechaSiniestro { get; set; }
        public int IdRama { get; set; }
        public int IdEmpresa { get; set; }
        public string ApellidoNombreTercero { get; set; }
        public string TelefonoTercero { get; set; }
        public int? IdCompañiaTercero { get; set; }
        public int? IdRamaTercero { get; set; }
        public string RamaTercero { get; set; }
        public DateTime? FechaPresupuesto { get; set; }
        public string Observaciones { get; set; }
        public int? Carpeta { get; set; }
        public bool PresentaReclamoTercero { get; set; }
        public bool Terminado { get; set; }
        public DateTime? FechaFin { get; set; }
    }

    public class ModificarSiniestroResponse
    {
        public int Id { get; set; }
    }

    public class RequerimientosSiniestroModel
    {
        public int IdSiniestro { get; set; }
        public string DescripRama { get; set; }
        public string DescripEmpresa { get; set; }
        public List<RequerimientoSiniestroEmpresa> ListaReqEmpresa { get; set; }
        public string DescripRamaTercero { get; set; }
        public string DescripEmpresaTercero { get; set; }
        public List<RequerimientoSiniestroEmpresa> ListaReqEmpresaTercero { get; set; }
    }

    public class RequerimientoSiniestroEmpresa
    {
        public int Id { get; set; }
        public int IdSiniestro { get; set; }
        public int IdRequerimientoEmpresa { get; set; }
        public string DescripRequerimiento { get; set; }
        public bool Valor { get; set; }
        public bool Principal { get; set; }
    }

    public class RequerimientosSiniestroRequest
    {
        public List<RequerimientoSiniestroEmpresa> Lista { get; set; }
    }

    public class RequerimientosSiniestroResponse
    {
        public int Id { get; set; }
    }

    public class NotificacionesSiniestrosModel
    {
        public int IdSiniestro { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime? FechaFinal { get; set; }

        public string Mensaje { get; set; }

        public bool VisibleCliente { get; set; }

        public int IdUsuario { get; set; }
    }
}