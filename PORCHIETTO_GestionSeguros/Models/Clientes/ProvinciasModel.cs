﻿namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ProvinciasModel
    {
        public int Id { get; set; }
        public int Codigo { get; set; }
        public string Nombre { get; set; }
    }
}