﻿namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ObtenerDomicilioCobroResponse
    {
        public string DocmicilioCalle { get; set; }

        public string DomicilioNumero { get; set; }

        public int? CodigoLocalidad { get; set; }

        public int? CodigoProvincia { get; set; }
    }
}