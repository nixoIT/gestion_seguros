﻿using PORCHIETTO_GestionSeguros.Models.Polizas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class AgregarClienteModel
    {
        public int Id { get; set; }

        [Display(Name = "Apellido/Razón Social")]
        [Required(ErrorMessage = "Se requiere Apellido")]
        public string Apellido { get; set; }

        public string Nombre { get; set; }

        public string NombreCompleto { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-mail")]
        public string email { get; set; }

        [Display(Name = "Teléfono")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "El teléfono debe ser de 11 dígitos")]
        public string TelefonoFijo { get; set; }

        //[Required(ErrorMessage = "Se requiere Celular")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "El celular debe ser ingresado sin 0 y sin 15")]
        [Display(Name = "Celular")]
        public string TelefonoMovil { get; set; }

        [Display(Name = "Domicilio")]
        [Required]
        public string DomicilioCalle { get; set; }

        [Display(Name = "N°")]
        [Required]
        public string DomicilioNumero { get; set; }

        [Display(Name = "Domicilio de Cobro")]
        [Required]
        public string DomicilioCobroCalle { get; set; }

        [Display(Name = "N°")]
        [Required]
        public string DomicilioCobroNumero { get; set; }

        public string Carpeta { get; set; }

        [Display(Name = "Medio de pago")]
        public int IdMediodeCobranza { get; set; }

        public string NombreMediodeCobranza { get; set; }

        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }

        public DateTime? Borrado { get; set; }

        [Display(Name = "DNI")]
        [StringLength(8, MinimumLength = 7, ErrorMessage = "El DNI debe ser entre 1000000 y 99999999")]
        public string DNI { get; set; }

        [Display(Name = "Fecha Nacimiento")]
        public DateTime? FechaNacimiento { get; set; }

        public string Observaciones { get; set; }

        [StringLength(11, MinimumLength = 11, ErrorMessage = "El CUIT debe ser de 11 caracteres (sin guiones)")]
        public string CUIT { get; set; }

        [Display(Name = "Localidad")]
        [Required]
        public int CodigoLocalidad { get; set; }

        [Display(Name = "Provincia")]
        public int CodigoProvincia { get; set; }

        [Display(Name = "Localidad de Cobro")]
        public int? CodigoLocalidadCobro { get; set; }

        [Display(Name = "Provincia de Cobro")]
        public int? CodigoProvinciaCobro { get; set; }

        [Display(Name = "Es Referente de pago")]
        public bool EsCabezadeGrupo { get; set; }

        [Display(Name = "Referente de pago")]
        public int? IdGrupodeCobranza { get; set; }
        public int? IdUsuario { get; set; }
        public string Productor { get; set; }
    }

    public class AgregarClienteRequest
    {
        public int Id { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string TelefonoFijo { get; set; }
        public string TelefonoMovil { get; set; }
        public string DomicilioCalle { get; set; }
        public string DomicilioNumero { get; set; }
        public int CodigoLocalidad { get; set; }
        public int CodigoProvincia { get; set; }
        public int IdMediodeCobranza { get; set; }
        public string DomicilioCobroCalle { get; set; }
        public string DomicilioCobroNumero { get; set; }
        public int? CodigoLocalidadCobro { get; set; }
        public int? CodigoProvinciaCobro { get; set; }
        public string Carpeta { get; set; }
        public DateTime Fecha { get; set; }
        public string DNI { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Observaciones { get; set; }
        public string CUIT { get; set; }
        public int IdGrupodeCobranza { get; set; }
        public bool EsCabezadeGrupo { get; set; }
        public int? IdProductor { get; set; }
    }

    public class AgregarClienteResponse
    {
        public int Id { get; set; }
    }

    public class ClientesListado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string TelefonoAlternativo { get; set; }
        public string Domicilio { get; set; }
        public string DomicilioCobro { get; set; }
		public int? IdUsuario { get; set; }
    }

    public class ClientesListadoModel
    {
        public List<ClientesListado> Lista { get; set; }

        [Display(Name = "Seleccionar Productor")]
        public int? IdProductor { get; set; }
    }

    public class ClientesListadoRequest
    {
        public int? IdProductor { get; set; }
    }
    public class DetalleClienteModel
    {
        public AgregarClienteModel Cliente { get; set; }

        public List<AgregarPolizaModel> Polizas { get; set; }

        public List<string> Referidos { get; set; }

		public AgregarUsuarioClienteModel Usuario { get; set; }
	}

	public class AgregarUsuarioClienteModel
	{
		public int Id { get; set; }

		[Required(ErrorMessage = "Se requiere Apellido")]
		public string Apellido { get; set; }

		[Required(ErrorMessage = "Se requiere Nombre")]
		public string Nombre { get; set; }

		[Required(ErrorMessage = "Se requiere de un email para registrar el usuario")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required(ErrorMessage = "Se requiere Usuario")]
		public string Usuario { get; set; }

		[Required(ErrorMessage = "Se requiere Contraseña")]
		[DataType(DataType.Password)]
		[DisplayName("Contraseña")]
		public string Pass { get; set; }

		[Required(ErrorMessage = "Se requiere Confirmación de Contraseña")]
		[DataType(DataType.Password)]
		[Compare("Pass")]
		[DisplayName("Confirmar Contraseña")]
		public string ConfirmarPass { get; set; }

		public int IdCliente { get; set; }
	}

	public class AgregarUsuarioClienteRequest
	{
		public int Id { get; set; }
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		public DateTime FechaAlta { get; set; }
		public string Email { get; set; }
		public string Usuario { get; set; }
		public string Pass { get; set; }
		public int IdCliente { get; set; }
	}

	public class AgregarUsuarioClienteResponse
	{
		public int Id { get; set; }
	}
}