﻿namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ObtenerLocalidadesRequest
    {
        public int? Codigo { get; set; }
        public int? CodigoProvincia { get; set; }
        public string CodigoPostal { get; set; }
        public string Nombre { get; set; }
    }
}