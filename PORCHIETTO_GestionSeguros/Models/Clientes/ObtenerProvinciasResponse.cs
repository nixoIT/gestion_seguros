﻿using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ObtenerProvinciasResponse
    {
        public List<ProvinciasModel> Provincias { get; set; }
    }
}