﻿namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class LocalidadesModel
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string CodigoPostal { get; set; }
        public int CodigoProvincia { get; set; }
        public string NombreProvincia { get; set; }
    }
}