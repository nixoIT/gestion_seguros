﻿namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ObtenerDomicilioReferenteResponse
    {
        public string DomicilioCalle { get; set; }
        public string DomicilioNumero { get; set; }
        public int? CodigoLocalidad { get; set; }
        public int? CodigoProvincia { get; set; }
    }
}