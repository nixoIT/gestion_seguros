﻿using System;

namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ModificarClienteRequest
    {
        public int Id { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string TelefonoFijo { get; set; }
        public string TelefonoMovil { get; set; }
        public string DomicilioCalle { get; set; }
        public string DomicilioNumero { get; set; }
        public int CodigoLocalidad { get; set; }
        public int CodigoProvincia { get; set; }
        public int IdMediodeCobranza { get; set; }
        public string DomicilioCobroCalle { get; set; }
        public string DomicilioCobroNumero { get; set; }
        public int? CodigoLocalidadCobro { get; set; }
        public int? CodigoProvinciaCobro { get; set; }
        public string Carpeta { get; set; }
        public DateTime Fecha { get; set; }
        public string DNI { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Observaciones { get; set; }
        public string CUIT { get; set; }
        public int IdGrupodeCobranza { get; set; }
        public bool EsCabezadeGrupo { get; set; }
    }

    public class ModificarClienteResponse
    {
        public int Id { get; set; }
    }
}