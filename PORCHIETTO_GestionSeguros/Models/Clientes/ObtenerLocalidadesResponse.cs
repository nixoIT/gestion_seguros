﻿using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ObtenerLocalidadesResponse
    {
        public List<LocalidadesModel> Localidades { get; set; }
    }
}