﻿using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ObtenerGruposdeCobranzaResponse
    {
        public List<AgregarClienteModel> Lista { get; set; }
    }
}