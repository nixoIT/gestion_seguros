﻿using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class ObtenerProvinciasRequest
    {
        public int? Codigo { get; set; }
    }
}