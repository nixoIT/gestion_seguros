﻿using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Models.Clientes
{
    public class DatosDeContactoClienteModel
    {
        public string Cliente { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
    }
}