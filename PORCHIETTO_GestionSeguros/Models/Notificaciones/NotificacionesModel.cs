﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PORCHIETTO_GestionSeguros.Models.Notificaciones
{
    public class NotificacionesModel
    {
        public class ObtenerNotificacionesRequest
        {
            public DateTime Fecha { get; set; }
            public int IdTipo { get; set; }
            public TipoNotificaciones Tipo { get; set; }
            public int IdProductor { get; set; }
            public int IdUsuario { get; set; }
        }

        public class ObtenerNotificacionesResponse
        {
            public List<ObtenerNotificacionesModel> Lista { get; set; }
        }

        public class ObtenerNotificacionesModel
        {
            public string Titulo { get; set; }
            public string Mensaje { get; set; }
            public int Cantidad { get; set; }
            TipoNotificaciones Tipo { get; set; }
            public string URL { get; set; }
        }
    }

    public class InformeViewModel
    {
        public TipoNotificaciones Tipo { get; set; }
        public List<Informe> Lista { get; set; }
    }

    public class Informe
    {
        public int Id { get; set; }
        public int IdTipo { get; set; }
        public TipoNotificaciones Tipo { get; set; }
        public string Numero { get; set; }
        public string Cliente { get; set; }
        public DateTime Vencimiento { get; set; }
        public string Mensaje { get; set; }
        public int IdProductor { get; set; }
        public int IdUsuario { get; set; }
    }

    public class ObtenerInformeRequest
    {
        public DateTime Fecha { get; set; }
        public TipoNotificaciones Tipo { get; set; }
        public int IdUsuario { get; set; }
        public int IdProductor { get; set; }
    }

    public class NotificacionesSiniestros
    {
        public int Id { get; set; }
        public int IdSiniestro { get; set; }
        public string Mensaje { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public DateTime? Borrado { get; set; }
        public bool VisibleCliente { get; set; }
        public int IdUsuario { get; set; }
        public string Usuario { get; set; }
    }

    public class InformeNotificacionesSiniestros
    {
        public string Cliente { get; set; }
        public int IdSiniestro { get; set; }
        public List<NotificacionesSiniestros> Lista { get; set; }
    }

    public class NotificacionesPolizas
    {
        public int Id { get; set; }
        public int IdPoliza { get; set; }
        public string Mensaje { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public DateTime? Borrado { get; set; }
        public bool VisibleCliente { get; set; }
        public int IdUsuario { get; set; }
        public string Usuario { get; set; }
    }

    public class InformeNotificacionesPolizas
    {
        public string Cliente { get; set; }
        public int IdPoliza { get; set; }
        public List<NotificacionesPolizas> Lista { get; set; }
    }

    public enum TipoNotificaciones
    {
        [Description("Todas")] Todas,
        [Description("Vencimiento de Pólizas")] Vencimientos_Pólizas,
        [Description("Vencimiento de Siniestros")] Vencimientos_Siniestros,
        [Description("Notificaciones de Siniestros")] Notificaciones_Siniestros,
        [Description("Notificaciones de Pólizas")] Notificaciones_Pólizas
    }
}