﻿using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Models.Notificaciones
{
    public class MarcarResueltos
    {
        public int IdNotificacion { get; set; }

        public int IdTipoNotificacion { get; set; }
    }

    public class MarcarResueltosResponse
    {

    }

    public class MarcarResueltosRequest
    {
        public List<MarcarResueltos> Lista { get; set; }
    }
}