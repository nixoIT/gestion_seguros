﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Productores
{
	public class ProductoresViewModel
	{
		public List<AgregarProductorModel> Lista { get; set; }
	}

	public class AgregarProductorModel
	{
		public int Id { get; set; }
		[Required(ErrorMessage = "Se requiere Nombre o Razón Social")]
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		[Display(Name = "CUIT")]
		[StringLength(11, MinimumLength = 11, ErrorMessage = "El CUIT debe ser de 11 dígitos")]
		public string CUIT { get; set; }
		public string Web { get; set; }
		public string Domicilio { get; set; }
		[EmailAddress(ErrorMessage = "Email inválido")]
		public string Email { get; set; }
		[Display(Name = "Teléfono")]
		[StringLength(14, MinimumLength = 11, ErrorMessage = "El teléfono debe ser de al menos 11 dígitos")]
		public string Telefono { get; set; }
		[Display(Name = "Teléfono Alternativo")]
		[StringLength(14, MinimumLength = 11, ErrorMessage = "El teléfono debe ser de al menos 11 dígitos")]
		public string TelefonoAlternativo { get; set; }
		[Display(Name = "Actividad Principal")]
		public string ActividadPrincipal { get; set; }
		[Display(Name = "Actividad Secundaria")]
		public string ActividadSecundaria { get; set; }
		public DateTime? Borrado { get; set; }
		public int? IdUsuario { get; set; }
	}

	public class ModificarProductorModel
	{
		public int Id { get; set; }
		[Required(ErrorMessage = "Se requiere Nombre o Razón Social")]
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		[Display(Name = "CUIT")]
		[StringLength(11, MinimumLength = 11, ErrorMessage = "El CUIT debe ser de 11 dígitos")]
		public string CUIT { get; set; }
		public string Web { get; set; }
		public string Domicilio { get; set; }
		[EmailAddress(ErrorMessage = "Email inválido")]
		public string Email { get; set; }
		[Display(Name = "Teléfono")]
		[StringLength(14, MinimumLength = 11, ErrorMessage = "El teléfono debe ser de al menos 11 dígitos")]
		public string Telefono { get; set; }
		[Display(Name = "Teléfono Alternativo")]
		[StringLength(14, MinimumLength = 11, ErrorMessage = "El teléfono debe ser de al menos 11 dígitos")]
		public string TelefonoAlternativo { get; set; }
		[Display(Name = "Actividad Principal")]
		public string ActividadPrincipal { get; set; }
		[Display(Name = "Actividad Secundaria")]
		public string ActividadSecundaria { get; set; }
		public DateTime? Borrado { get; set; }
		public int? IdUsuario { get; set; }
	}

	public class AgregarProductorRequest
	{
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		public string CUIT { get; set; }
		public string Web { get; set; }
		public string Domicilio { get; set; }
		public string Email { get; set; }
		public string Telefono { get; set; }
		public string TelefonoAlternativo { get; set; }
		public string ActividadPrincipal { get; set; }
		public string ActividadSecundaria { get; set; }
	}
	public class AgregarProductorResponse
	{
		public int Id { get; set; }
	}

	public class ModificarProductorRequest
	{
		public int Id { get; set; }
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		public string CUIT { get; set; }
		public string Web { get; set; }
		public string Domicilio { get; set; }
		public string Email { get; set; }
		public string Telefono { get; set; }
		public string TelefonoAlternativo { get; set; }
		public string ActividadPrincipal { get; set; }
		public string ActividadSecundaria { get; set; }
	}

	public class ModificarProductorResponse
	{
		public int Id { get; set; }
	}

	public class DetalleProductorModel
	{
		public ModificarProductorModel Productor { get; set; }

		public AgregarUsuarioProductorModel Usuario { get; set; }
	}

	public class AgregarUsuarioProductorModel
	{
		public int Id { get; set; }

		[Required(ErrorMessage = "Se requiere Apellido o Tipo de Razón Social")]
		public string Apellido { get; set; }

		[Required(ErrorMessage = "Se requiere Nombre o Razón Social")]
		public string Nombre { get; set; }

		[Required(ErrorMessage = "Se requiere de un email para registrar el usuario")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required(ErrorMessage = "Se requiere Usuario")]
		public string Usuario { get; set; }

		[Required(ErrorMessage = "Se requiere Contraseña")]
		[DataType(DataType.Password)]
		[DisplayName("Contraseña")]
		public string Pass { get; set; }

		[Required(ErrorMessage = "Se requiere Confirmación de Contraseña")]
		[DataType(DataType.Password)]
		[Compare("Pass")]
		[DisplayName("Confirmar Contraseña")]
		public string ConfirmarPass { get; set; }

		public int IdProductor { get; set; }
	}

	public class AgregarUsuarioProductorRequest
	{
		public int Id { get; set; }
		public string Nombre { get; set; }
		public string Apellido { get; set; }
		public DateTime FechaAlta { get; set; }
		public string Email { get; set; }
		public string Usuario { get; set; }
		public string Pass { get; set; }
		public int IdProductor { get; set; }
	}

	public class AgregarUsuarioProductorResponse
	{
		public int Id { get; set; }
	}
}