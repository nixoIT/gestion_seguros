﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Empresas
{
    public class EmpresasViewModel
    {
        public List<AgregarEmpresaViewModel> Lista { get; set; }
    }

    public class AgregarEmpresaViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre o Razón Social")]
        public string Nombre { get; set; }
        [Display(Name = "CUIT")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "El CUIT debe ser de 11 dígitos")]
        public string CUIT { get; set; }
        public string Web { get; set; }
        public string Domicilio { get; set; }
        [Display(Name = "Teléfono")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "El teléfono debe ser de 11 dígitos")]
        public string Telefono { get; set; }
        [Display(Name = "Actividad Principal")]
        public string ActividadPrincipal { get; set; }
        [Display(Name = "Actividad Secundaria")]
        public string ActividadSecundaria { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class ModificarEmpresaViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Apellido")]
        public string Nombre { get; set; }
        [Display(Name = "CUIT")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "El CUIT debe ser de 11 dígitos")]
        public string CUIT { get; set; }
        public string Web { get; set; }
        public string Domicilio { get; set; }
        [Display(Name = "Teléfono")]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "El teléfono debe ser de 11 dígitos")]
        public string Telefono { get; set; }
        [Display(Name = "Actividad Principal")]
        public string ActividadPrincipal { get; set; }
        [Display(Name = "Actividad Secundaria")]
        public string ActividadSecundaria { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class AgregarEmpresaRequest
    {
        public string Nombre { get; set; }
        public string CUIT { get; set; }
        public string Web { get; set; }
        public string Domicilio { get; set; }
        public string Telefono { get; set; }
        public string ActividadPrincipal { get; set; }
        public string ActividadSecundaria { get; set; }
    }
    public class AgregarEmpresaResponse
    {

    }

    public class ModificarEmpresaRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string CUIT { get; set; }
        public string Web { get; set; }
        public string Domicilio { get; set; }
        public string Telefono { get; set; }
        public string ActividadPrincipal { get; set; }
        public string ActividadSecundaria { get; set; }
    }
    public class ModificarEmpresaResponse
    {

    }

}