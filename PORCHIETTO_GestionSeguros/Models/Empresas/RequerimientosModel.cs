﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PORCHIETTO_GestionSeguros.Models.Empresas
{
    public class RequerimientosModel
    {
        public List<AgregarRequerimientoModel> Lista { get; set; }
    }

    public class AgregarRequerimientoModel
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class AgregarRequerimientoRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class AgregarRequerimientoResponse
    {
        public int Id { get; set; }
    }

    public class ModificarRequerimientoModel
    {
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class ModificarRequerimientoRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime? Borrado { get; set; }
    }

    public class ModificarRequerimientoResponse
    {
        public int Id { get; set; }
    }

    #region REQUERIMIENTOS-EMPRESA
    public class ListarRequerimientosEmpresaModel
    {
        public List<RequerimientoEmpresaModel> Lista { get; set; }
    }

    public class RequerimientoEmpresaModel
    {
        public int Id { get; set; }
        public string Requerimiento { get; set; }
        public int Id_Rama { get; set; }
        public string Rama { get; set; }
    }

    public class AgregarRequerimientoEmpresaModel
    {
        public int Id { get; set; }
        [Required]
        public int Id_Requerimiento { get; set; }
        [Required]
        public int Id_Empresa { get; set; }
        [Required]
        public int Id_Rama { get; set; }
    }

    public class AgregarRequerimientoEmpresaRequest
    {
        public int Id { get; set; }
        public int Id_Requerimiento { get; set; }
        public int Id_Empresa { get; set; }
        public int Id_Rama { get; set; }
    }

    public class AgregarRequerimientoEmpresaResponse
    {
        public int Id { get; set; }
    }
    #endregion
}
