﻿using PORCHIETTO_GestionSeguros.Models.Usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Login
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Pass { get; set; }
        public List<NivelModel> Niveles { get; set; }
    }

    public class ObtenerUsuarioRequest
    {
        public string Usuario { get; set; }
    }

    public class LoginResponse
    {
        public bool Aceptado { get; set; }
        public List<NivelModel> Niveles { get; set; }
        public string NombreUsuario { get; set; }
        public int IdUsuario { get; set; }
		public bool CambiarPass { get; set; }
	}

    public class LoginRequest
    {
        public string Usuario { get; set; }
        public string Pass { get; set; }
    }

	public class LostPassRequest
	{
		[DataType(DataType.EmailAddress)]
		[Display(Name = "E-mail")]
		public string Email { get; set; }
	}
}