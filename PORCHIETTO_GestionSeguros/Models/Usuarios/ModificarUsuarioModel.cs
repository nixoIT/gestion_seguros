﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PORCHIETTO_GestionSeguros.Models.Usuarios
{
    public class ModificarUsuarioModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Apellido")]
        public string Apellido { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre")]
        public string Nombre { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Telefono { get; set; }
    }

    public class ModificarUsuarioRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
    }

    public class ModificarUsuarioResponse
    {
        public int Id { get; set; }
    }
}