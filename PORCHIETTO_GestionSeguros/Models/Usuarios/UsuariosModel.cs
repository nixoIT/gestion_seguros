﻿using System;
using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Models.Usuarios
{
    public class UsuariosModel
    {
        public List<AgregarUsuarioModel> Lista { get; set; }
    }

    public class NivelModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime Borrado { get; set; }
    }

    public class NivelesUsuariosModel
    {
        public List<NivelModel> Lista { get; set; }
    }

    public class AgregarNivelUsuario
    {
        public int IdUsuario { get; set; }
        public int IdNivel { get; set; }
    }
}