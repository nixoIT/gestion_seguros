﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PORCHIETTO_GestionSeguros.Models.Usuarios
{
    public class ModificarPassModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Se requiere Contraseña Actual")]
        [DataType(DataType.Password)]
        [DisplayName("Contraseña Actual")]
        public string PassAntigua { get; set; }

        [Required(ErrorMessage = "Se requiere Contraseña Nueva")]
        [DataType(DataType.Password)]
        [DisplayName("Contraseña Nueva")]

        public string Pass { get; set; }

        [Required(ErrorMessage = "Se requiere Confirmación de Contraseña Nueva")]
        [DataType(DataType.Password)]
        [Compare("Pass")]
        [DisplayName("Confirmar Contraseña Nueva")]
        public string ConfirmarPass { get; set; }
    }

    public class ModificarPassRequest
    {
        public int Id { get; set; }
        public string Pass { get; set; }
    }

    public class ModificarPassResponse
    {
        public int Id { get; set; }
    }
}