﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PORCHIETTO_GestionSeguros.Models.Usuarios
{
    public class AgregarUsuarioModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Se requiere Apellido")]
        public string Apellido { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Se requiere Usuario")]
        public string Usuario { get; set; }
        [Required(ErrorMessage = "Se requiere Contraseña")]
        [DataType(DataType.Password)]
        [DisplayName("Contraseña")]
        public string Pass { get; set; }
        [Required(ErrorMessage = "Se requiere Confirmación de Contraseña")]
        [DataType(DataType.Password)]
        [Compare("Pass")]
        [DisplayName("Confirmar Contraseña")]
        public string ConfirmarPass { get; set; }
        public List<string> Niveles { get; set; }
    }

    public class AgregarUsuarioRequest
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaAlta { get; set; }
        public string Usuario { get; set; }
        public string Pass { get; set; }
        public List<string> Niveles { get; set; }
    }

    public class AgregarUsuarioResponse
    {
        public int Id { get; set; }
    }
}