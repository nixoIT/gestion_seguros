﻿using Newtonsoft.Json;
using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Clientes;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PORCHIETTO_GestionSeguros.Controllers
{
	public class ClientesController : BaseController
	{
		#region Properties
		private IClientesService servicioClientes;
		private IPolizasService servicioPolizas;
		private IProductoresService servicioProductores;

		public IClientesService GetServicioClientes()
		{
			return servicioClientes;
		}

		public void SetServicioClientes(IClientesService value)
		{
			servicioClientes = value;
		}

		public IPolizasService GetServicioPolizas()
		{
			return servicioPolizas;
		}

		public void SetServicioPolizas(IPolizasService value)
		{
			servicioPolizas = value;
		}

		public IProductoresService GetServicioProductores()
		{
			return servicioProductores;
		}

		public void SetServicioProductores(IProductoresService value)
		{
			servicioProductores = value;
		}
		#endregion Properties

		#region Constructor
		public ClientesController(IClientesService ServicioClientes, IPolizasService ServicioPolizas, IProductoresService ServicioProductores)
		{
			this.SetServicioClientes(ServicioClientes);
			this.SetServicioPolizas(ServicioPolizas);
			this.SetServicioProductores(ServicioProductores);
		}

		#endregion Constructor

		// GET: Usuarios
		public ActionResult Index(int? id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString() || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
			{
				var request = new ClientesListadoRequest { };

				if ((int)Session["idproductor"] == 0)
				{
					ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
					request.IdProductor = id.HasValue ? id : 0;
				}
				else
				{
					request.IdProductor = (int)Session["idproductor"];
				}
				var model = GetServicioClientes().ObtenerClientesListado(request);
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult AgregarCliente()
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString() || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
			{
				var requestProv = new ObtenerProvinciasRequest() { };
				ViewBag.Provincias = new SelectList(GetServicioClientes().ObtenerProvincias(requestProv).Provincias, "Codigo", "Nombre");
				ViewBag.Medios = new SelectList(GetServicioClientes().ObtenerMediosDePago().Lista, "Id", "Nombre");
				ViewBag.Grupos = new SelectList(GetServicioClientes().ObtenerGruposCobranza().Lista, "Id", "Nombre");

				return View();
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult AgregarCliente(AgregarClienteModel model)
		{
			if (string.IsNullOrEmpty(model.DNI) && string.IsNullOrEmpty(model.CUIT))
				if (string.IsNullOrEmpty(model.Nombre))
				{
					ModelState.AddModelError("", "Debe ingresar CUIT");
				}
				else
				{
					ModelState.AddModelError("", "Debe ingresar DNI o CUIT");
				}


			if (ModelState.IsValid)
			{
				AgregarClienteRequest _Cliente = new AgregarClienteRequest
				{
					Id = model.Id,
					Apellido = model.Apellido,
					Nombre = model.Nombre,
					Email = model.email,
					TelefonoFijo = model.TelefonoFijo,
					TelefonoMovil = model.TelefonoMovil,
					DomicilioCalle = model.DomicilioCalle,
					DomicilioNumero = model.DomicilioNumero,
					CodigoLocalidad = model.CodigoLocalidad,
					CodigoProvincia = model.CodigoProvincia,
					IdMediodeCobranza = model.IdMediodeCobranza,
					IdGrupodeCobranza = model.IdGrupodeCobranza ?? 0,
					Fecha = DateTime.Now,
					DomicilioCobroCalle = model.DomicilioCobroCalle,
					DomicilioCobroNumero = model.DomicilioCobroNumero,
					CodigoLocalidadCobro = model.CodigoLocalidadCobro,
					CodigoProvinciaCobro = model.CodigoProvinciaCobro,
					Carpeta = model.Carpeta,
					CUIT = model.CUIT,
					DNI = model.DNI,
					FechaNacimiento = model.FechaNacimiento,
					Observaciones = model.Observaciones,
					EsCabezadeGrupo = model.EsCabezadeGrupo
				};

				if ((int)Session["idproductor"] != 0)
					_Cliente.IdProductor = (int)Session["idproductor"];

				GetServicioClientes().AgregarCliente(_Cliente);

				return RedirectToAction("Index");
			}
			else
			{
				var requestProv = new ObtenerProvinciasRequest() { };
				ViewBag.Provincias = new SelectList(GetServicioClientes().ObtenerProvincias(requestProv).Provincias, "Codigo", "Nombre");
				ViewBag.Medios = new SelectList(GetServicioClientes().ObtenerMediosDePago().Lista, "Id", "Nombre");
				ViewBag.Grupos = new SelectList(GetServicioClientes().ObtenerGruposCobranza().Lista, "Id", "Nombre");
				return View(model);
			}
		}

		public ActionResult ModificarCliente(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString() || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
			{
				var requestProv = new ObtenerProvinciasRequest() { };
				ViewBag.Provincias = new SelectList(GetServicioClientes().ObtenerProvincias(requestProv).Provincias, "Codigo", "Nombre");
				ViewBag.Medios = new SelectList(GetServicioClientes().ObtenerMediosDePago().Lista, "Id", "Nombre");
				ViewBag.Grupos = new SelectList(GetServicioClientes().ObtenerGruposCobranza().Lista, "Id", "Nombre");
				var model = GetServicioClientes().ObtenerClienteAEditar(Id);
				ViewBag.Localidades = new SelectList(GetServicioClientes().ObtenerLocalidades(new ObtenerLocalidadesRequest { CodigoProvincia = model.CodigoProvincia }).Localidades, "Codigo", "Nombre");
				ViewBag.LocalidadesCobro = new SelectList(GetServicioClientes().ObtenerLocalidades(new ObtenerLocalidadesRequest { CodigoProvincia = model.CodigoProvinciaCobro }).Localidades, "Codigo", "Nombre");
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult ModificarCliente(AgregarClienteModel model)
		{
			if (string.IsNullOrEmpty(model.Nombre))
			{
				if (string.IsNullOrEmpty(model.CUIT))
				{
					ModelState.AddModelError("", "Debe ingresar CUIT");
					return View(model);
				}
			}
			else
			{
				if (string.IsNullOrEmpty(model.DNI))
				{
					if (string.IsNullOrEmpty(model.CUIT))
					{
						ModelState.AddModelError("", "Debe ingresar DNI o CUIT");
						return View(model);
					}
				}
			}

			if (ModelState.IsValid)
			{
				ModificarClienteRequest Cliente = new ModificarClienteRequest
				{
					Id = model.Id,
					Nombre = model.Nombre,
					Apellido = model.Apellido,
					CUIT = model.CUIT,
					DNI = model.DNI,
					FechaNacimiento = model.FechaNacimiento,
					Carpeta = model.Carpeta,
					DomicilioCalle = model.DomicilioCalle,
					DomicilioNumero = model.DomicilioNumero,
					CodigoLocalidad = model.CodigoLocalidad,
					CodigoProvincia = model.CodigoProvincia,
					DomicilioCobroCalle = model.DomicilioCobroCalle,
					DomicilioCobroNumero = model.DomicilioCobroNumero,
					CodigoLocalidadCobro = model.CodigoLocalidadCobro,
					CodigoProvinciaCobro = model.CodigoProvinciaCobro,
					Email = model.email,
					Fecha = model.Fecha,
					IdMediodeCobranza = model.IdMediodeCobranza,
					IdGrupodeCobranza = model.IdGrupodeCobranza ?? 0,
					EsCabezadeGrupo = model.EsCabezadeGrupo,
					TelefonoFijo = model.TelefonoFijo,
					TelefonoMovil = model.TelefonoMovil,
					Observaciones = model.Observaciones
				};

				if (model.FechaNacimiento.HasValue)
					Cliente.FechaNacimiento = model.FechaNacimiento.Value;

				GetServicioClientes().ModificarCliente(Cliente);

				return RedirectToAction("Index", "Clientes");

			}
			else
			{
				//notificacion.Mensaje = "Atención! Faltan datos o están incorrectos.";
				//notificacion.Tipo = "warning";
				//ViewBag.Notificar = notificacion;
				var requestProv = new ObtenerProvinciasRequest() { };
				ViewBag.Provincias = new SelectList(GetServicioClientes().ObtenerProvincias(requestProv).Provincias, "Codigo", "Nombre");
				ViewBag.Medios = new SelectList(GetServicioClientes().ObtenerMediosDePago().Lista, "Id", "Nombre");
				ViewBag.Grupos = new SelectList(GetServicioClientes().ObtenerGruposCobranza().Lista, "Id", "Nombre");
				ViewBag.Localidades = new SelectList(GetServicioClientes().ObtenerLocalidades(new ObtenerLocalidadesRequest { CodigoProvincia = model.CodigoProvincia }).Localidades, "Codigo", "Nombre");
				ViewBag.LocalidadesCobro = new SelectList(GetServicioClientes().ObtenerLocalidades(new ObtenerLocalidadesRequest { CodigoProvincia = model.CodigoProvinciaCobro }).Localidades, "Codigo", "Nombre");
				ViewBag.fechanac = String.Format("{0:yyyy-MM-dd}", model.FechaNacimiento);

				return View(model);
			}

		}

		public ActionResult EliminarCliente(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString() || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
			{
				GetServicioClientes().EliminarCliente(Id);

				//Genero la notificación
				//Notificaciones notificacion = new Notificaciones
				//{
				//    Mensaje = "Usuario eliminado correctamente.",
				//    Tipo = "success"
				//};
				//ViewBag.Notificar = notificacion;
				//Genero la notificación

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult DetalleCliente(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
			|| x.Nombre == Niveles.Empleado.ToString()
			|| x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
			{
				var model = new DetalleClienteModel
				{
					Cliente = GetServicioClientes().ObtenerCliente(Id),
					Polizas = GetServicioPolizas().ObtenerPolizasDeCliente(Id),
					Referidos = GetServicioClientes().ListadoReferidos(Id),
					Usuario = GetServicioClientes().ObtenerUsuarioCliente(Id)
				};
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult Obtenerlocalidades(int? CodigoProvincia)
		{
			var json = JsonConvert.SerializeObject(GetServicioClientes().ObtenerLocalidades(new ObtenerLocalidadesRequest { CodigoProvincia = CodigoProvincia }).Localidades);

			return Json(json, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult ObtenerDomicilioReferente(int IdReferente)
		{
			var json = JsonConvert.SerializeObject(GetServicioClientes().ObtenerDomicilioReferente(IdReferente));
			return Json(json, JsonRequestBehavior.AllowGet);
		}

		public ActionResult DatosDeContactoCliente(int IdCliente)
		{
			var cliente = (GetServicioClientes().ObtenerCliente(IdCliente));
			var detalle = new DatosDeContactoClienteModel
			{
				Cliente = cliente.Apellido?.ToUpper().ToString() + ", " + cliente.Nombre?.ToString() ?? "",
				Telefono = cliente.TelefonoFijo ?? "",
				Celular = cliente.TelefonoMovil ?? "",
				Email = cliente.email ?? ""
			};

			return Json(detalle, JsonRequestBehavior.AllowGet);
		}

		public ActionResult AgregarUsuarioCliente(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				var Cliente = GetServicioClientes().ObtenerCliente(Id);
				AgregarUsuarioClienteModel model = new AgregarUsuarioClienteModel()
				{
					Nombre = Cliente.Nombre,
					Apellido = Cliente.Apellido,
					IdCliente = Cliente.Id,
					Email = Cliente.email
				};
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult AgregarUsuarioCliente(AgregarUsuarioClienteModel model)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				if (ModelState.IsValid)
				{
					var existe = GetServicioClientes().ExisteUsuario(model.Usuario);
					if (existe)
					{
						ModelState.AddModelError("Usuario", "El usuario ya existe");
						return View(model);
					}

					AgregarUsuarioClienteRequest Usuario = new AgregarUsuarioClienteRequest
					{
						Nombre = model.Nombre,
						Apellido = model.Apellido,
						Usuario = model.Usuario,
						Pass = Seguridad.GetHashSha256(model.Pass),
						FechaAlta = DateTime.Today,
						IdCliente = model.IdCliente,
						Email = model.Email
					};
					var response = GetServicioClientes().AgregarUsuarioCliente(Usuario);

					if (response.Id > 0)
					{
						return RedirectToAction("Index", "Clientes");
					}
					else
					{
						return View(model);
					}

				}
				else
				{
					return View(model);
				}
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult EliminarUsuarioCliente(int idUsuario, int idCliente)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var response = GetServicioClientes().BorrarUsuarioCliente(idUsuario, idCliente);

				return RedirectToAction("DetalleCliente/" + idCliente.ToString(), "Clientes");
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult BlanquearPassCliente(int idUsuario, int idCliente)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var response = GetServicioClientes().BlanquearPassUsuarioCliente(idUsuario);

				return RedirectToAction("DetalleCliente/" + idCliente.ToString(), "Clientes");
			}
			else
				return RedirectToAction("Index", "Home");
		}
	}
}