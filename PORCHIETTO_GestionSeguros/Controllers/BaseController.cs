﻿using PORCHIETTO_GestionSeguros.Infrastructure.IoC;
using PORCHIETTO_GestionSeguros.Filters;
using PORCHIETTO_GestionSeguros.Servicios;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    [CustomAuthorization]
    public class BaseController : Controller
    {
        public ILoginService ServicioLogin { get; set; }

        public BaseController()
        {
            this.ServicioLogin = IoC.Instance.Resolve<ILoginService>();
        }
    }
}