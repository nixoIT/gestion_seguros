﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using PORCHIETTO_GestionSeguros.Models.Siniestros;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    public class SiniestrosController : BaseController
    {
        #region Properties
        private ISiniestrosService servicioSiniestros;
        private IClientesService servicioClientes;
        private IEmpresasService servicioEmpresas;
        private IProductoresService servicioProductores;
        private IRamasService servicioRamas;

        public ISiniestrosService GetServicioSiniestros()
        {
            return servicioSiniestros;
        }

        public void SetServicioSiniestros(ISiniestrosService value)
        {
            servicioSiniestros = value;
        }

        public IClientesService GetServicioClientes()
        {
            return servicioClientes;
        }

        public void SetServicioClientes(IClientesService value)
        {
            servicioClientes = value;
        }

        public IEmpresasService GetServicioEmpresas()
        {
            return servicioEmpresas;
        }

        public void SetServicioEmpresas(IEmpresasService value)
        {
            servicioEmpresas = value;
        }

        public IRamasService GetServicioRamas()
        {
            return servicioRamas;
        }

        public void SetServicioRamas(IRamasService value)
        {
            servicioRamas = value;
        }
        public IProductoresService GetServicioProductores()
        {
            return servicioProductores;
        }

        public void SetServicioProductores(IProductoresService value)
        {
            servicioProductores = value;
        }

        #endregion Properties

        #region Constructor
        public SiniestrosController(ISiniestrosService ServicioSiniestros,
            IClientesService ServicioClientes,
            IEmpresasService ServicioEmpresas,
            IRamasService ServicioRamas,
            IProductoresService ServicioProductores)
        {
            this.SetServicioSiniestros(ServicioSiniestros);
            this.SetServicioClientes(ServicioClientes);
            this.SetServicioEmpresas(ServicioEmpresas);
            this.SetServicioRamas(ServicioRamas);
            this.SetServicioProductores(ServicioProductores);
        }

        #endregion Constructor

        #region Siniestros

        // GET: Siniestros
        public ActionResult Index(int? id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() 
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                ViewBag.Ramas = GetServicioRamas().ObtenerRama().Lista;

                var request = new SiniestroListadoRequest { };

                if ((int)Session["idproductor"] == 0)
                {
                    ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                    request.IdProductor = id.HasValue ? id : 0;
                }
                else
                {
                    request.IdProductor = (int)Session["idproductor"];
                }

                var model = GetServicioSiniestros().ObtenerSiniestros(request);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult AgregarSiniestro()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                ViewBag.Clientes = new SelectList(GetServicioClientes().ObtenerClientes().Lista, "Id", "NombreCompleto");
                ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");

                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult AgregarSiniestro(AgregarSiniestroModel model)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                if (ModelState.IsValid)
                {
                    AgregarSiniestroRequest siniestro = new AgregarSiniestroRequest
                    {
                        Id = model.Id,
                        IdCliente = model.IdCliente,
                        FechaSiniestro = model.FechaSiniestro,
                        IdRama = model.IdRama,
                        IdEmpresa = model.IdEmpresa,
                        ApellidoNombreTercero = model.ApellidoNombreTercero,
                        TelefonoTercero = model.TelefonoTercero,
                        IdCompañiaTercero = model.IdCompañiaTercero,
                        IdRamaTercero = model.IdRamaTercero,
                        Observaciones = model.Observaciones,
                        Carpeta = model.Carpeta,
                        PresentaReclamoTercero = model.PresentaReclamoTercero
                    };

                    var idsin = GetServicioSiniestros().AgregarSiniestro(siniestro);

                    return RedirectToAction("RequerimientoDeSiniestro", new { id = idsin.Id });
                }
                else
                {
                    ViewBag.Clientes = new SelectList(GetServicioClientes().ObtenerClientes().Lista, "Id", "NombreCompleto");
                    ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");

                    return View(model);
                }

            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult ModificarSiniestro(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                ViewBag.Clientes = new SelectList(GetServicioClientes().ObtenerClientes().Lista, "Id", "NombreCompleto");
                ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
                var model = GetServicioSiniestros().ObtenerSiniestros(Id);
                ViewBag.Ramas = new SelectList(GetServicioRamas().ObtenerRamasEmpresa(model.IdEmpresa).Lista, "Id", "Nombre");
                if (model.IdCompañiaTercero != null)
                {
                    ViewBag.RamasTercero = new SelectList(GetServicioRamas().ObtenerRamasEmpresa(model.IdCompañiaTercero ?? 0).Lista, "Id", "Nombre");
                }
                return View(model);

            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult ModificarSiniestro(AgregarSiniestroModel model)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                if (ModelState.IsValid)
                {
                    ModificarSiniestroRequest siniestro = new ModificarSiniestroRequest
                    {
                        Id = model.Id,
                        IdCliente = model.IdCliente,
                        FechaSiniestro = model.FechaSiniestro,
                        IdRama = model.IdRama,
                        IdEmpresa = model.IdEmpresa,
                        ApellidoNombreTercero = model.ApellidoNombreTercero,
                        TelefonoTercero = model.TelefonoTercero,
                        IdCompañiaTercero = model.IdCompañiaTercero,
                        IdRamaTercero = model.IdRamaTercero,
                        Observaciones = model.Observaciones,
                        Carpeta = model.Carpeta,
                        PresentaReclamoTercero = model.PresentaReclamoTercero
                    };

                    GetServicioSiniestros().ModificarSiniestro(siniestro);

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Clientes = new SelectList(GetServicioClientes().ObtenerClientes().Lista, "Id", "NombreCompleto");
                    ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");

                    return View(model);
                }

            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult EliminarSiniestro(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                GetServicioSiniestros().EliminarSiniestro(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult FinalizarSiniestro(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                GetServicioSiniestros().FinalizarSiniestro(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult Historico(int? id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() 
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                ViewBag.Ramas = GetServicioRamas().ObtenerRama().Lista;
                var request = new SiniestroListadoRequest { };

                if ((int)Session["idproductor"] == 0)
                {
                    ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                    request.IdProductor = id.HasValue ? id : 0;
                }
                else
                {
                    request.IdProductor = (int)Session["idproductor"];
                }
                var model = GetServicioSiniestros().ObtenerHistoricoSiniestros(request);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult RequerimientoDeSiniestro(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioSiniestros().ObtenerRequerimientosSiniestro(Id);

                return View(model);

            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult RequerimientoDeSiniestro(RequerimientosSiniestroModel model)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                if (ModelState.IsValid)
                {
                    var Reqsiniestro = new List<RequerimientoSiniestroEmpresa>();

                    var Lista = model.ListaReqEmpresa.Select(x => new RequerimientoSiniestroEmpresa
                    {
                        Id = x.Id,
                        IdSiniestro = x.IdSiniestro,
                        IdRequerimientoEmpresa = x.IdRequerimientoEmpresa,
                        DescripRequerimiento = x.DescripRequerimiento,
                        Valor = x.Valor,
                        Principal = x.Principal,
                    }).ToList();

                    Reqsiniestro.AddRange(Lista);

                    if (model.ListaReqEmpresaTercero != null)
                    {
                        var Lista3ro = model.ListaReqEmpresaTercero.Select(x => new RequerimientoSiniestroEmpresa
                        {
                            Id = x.Id,
                            IdSiniestro = x.IdSiniestro,
                            IdRequerimientoEmpresa = x.IdRequerimientoEmpresa,
                            DescripRequerimiento = x.DescripRequerimiento,
                            Valor = x.Valor,
                            Principal = x.Principal,
                        }).ToList();

                        Reqsiniestro.AddRange(Lista3ro);
                    }

                    GetServicioSiniestros().AgregarRequerimientosSiniestro(Reqsiniestro);

                    return RedirectToAction("Index");
                }
                else
                {
                    var modelo = GetServicioSiniestros().ObtenerRequerimientosSiniestro(model.IdSiniestro);

                    return View(modelo);
                }
            }
            else
                return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Siniestros Cliente
        public ActionResult MisSiniestros()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Cliente.ToString() || x.Nombre == Niveles.Administrador.ToString()).FirstOrDefault() != null)
            {
                ViewBag.Ramas = GetServicioRamas().ObtenerRama().Lista;
                var model = GetServicioSiniestros().ObtenerMisSiniestros((int)Session["idcliente"]);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult VerSiniestro(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Cliente.ToString() || x.Nombre == Niveles.Administrador.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioSiniestros().ObtenerSiniestros(Id);
                if (model.IdCliente == (int)Session["idcliente"])
                {
                    ViewBag.Cliente = GetServicioClientes().ObtenerCliente(model.IdCliente ?? 0).NombreCompleto;
                    ViewBag.Empresa = GetServicioEmpresas().ObtenerEmpresa(model.IdEmpresa).Nombre;
                    ViewBag.Rama = GetServicioRamas().ObtenerRamasEmpresa(model.IdEmpresa).Lista.Where(x => x.Id == model.IdRama).FirstOrDefault().Nombre;
                    ViewBag.RamaTercero = model.IdCompañiaTercero != null ? GetServicioRamas().ObtenerRamasEmpresa(model.IdCompañiaTercero ?? 0).Lista.Where(x => x.Id == model.IdRamaTercero).FirstOrDefault().Nombre : "";
                    ViewBag.EmpresaTercero = model.IdCompañiaTercero != null ? GetServicioEmpresas().ObtenerEmpresa(model.IdCompañiaTercero ?? 0).Nombre : "";
					ViewBag.Notificaciones = GetServicioSiniestros().ObtenerNotificacionesSiniestrosCliente(model.Id).Lista;
					return View(model);
                }
                else
                    return RedirectToAction("Index", "Home");
            }
            else
                return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Notificaciones Siniestros

        public ActionResult Notificaciones(int Id)
        {
            var model = GetServicioSiniestros().ObtenerInformeNotificacionesSiniestros(Id);
            return View(model);
        }

        public ActionResult EliminarNotificacionSiniestro(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                GetServicioSiniestros().EliminarNotificacionSiniestro(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public JsonResult AgregarNotificacion(int IdSiniestro, DateTime FechaInicio, DateTime FechaFin, string Mensaje, bool VisibleCliente)
        {
            var nuevaNotificacion = new NotificacionesSiniestrosModel
            {
                IdSiniestro = IdSiniestro,
                FechaInicio = FechaInicio,
                FechaFinal = FechaFin,
                Mensaje = Mensaje,
                VisibleCliente = VisibleCliente,
                IdUsuario = (int)Session["idusuario"]
            };

            GetServicioSiniestros().AgregarNotificaciones(nuevaNotificacion);

            return Json(true, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult AgregarNotificacionCliente(int IdSiniestro, string Mensaje)
        {
            var nuevaNotificacion = new NotificacionesSiniestrosModel
            {
                IdSiniestro = IdSiniestro,
                FechaInicio = DateTime.Today,
                FechaFinal = null,
                Mensaje = Mensaje,
                VisibleCliente = true,
                IdUsuario = (int)Session["idusuario"]
            };

            GetServicioSiniestros().AgregarNotificaciones(nuevaNotificacion);

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}