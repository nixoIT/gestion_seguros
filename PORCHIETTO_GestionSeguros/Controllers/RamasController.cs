﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Ramas;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    public class RamasController : BaseController
    {
        #region Properties
        private IRamasService servicioRamas;

        public IRamasService GetServicioRamas()
        {
            return servicioRamas;
        }

        public void SetServicioRamas(IRamasService value)
        {
            servicioRamas = value;
        }

        private IEmpresasService servicioEmpresas;

        public IEmpresasService GetServicioEmpresas()
        {
            return servicioEmpresas;
        }

        public void SetServicioEmpresas(IEmpresasService value)
        {
            servicioEmpresas = value;
        }

        #endregion Properties

        #region Constructor
        public RamasController(IRamasService ServicioRamas, IEmpresasService ServicioEmpresas)
        {
            this.SetServicioRamas(ServicioRamas);
            this.SetServicioEmpresas(ServicioEmpresas);
        }
        #endregion Constructor

        // GET: Ramas
        public ActionResult Index()
        {
            var model = GetServicioRamas().ObtenerRama();
            ViewBag.Empresas = GetServicioEmpresas().ObtenerEmpresas().Lista;
            return View(model);
        }

        public ActionResult AgregarRama(int? Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                if (Id != null)
                {
                    ViewBag.Empresa = GetServicioEmpresas().ObtenerEmpresa(Id ?? 1);
                }
                else
                {
                    ViewBag.ListaEmpresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
                }
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult AgregarRama(AgregarRamaModel model)
        {
            if (ModelState.IsValid)
            {
                AgregarRamaRequest rama = new AgregarRamaRequest
                {
                    Nombre = model.Nombre,
                    Codigo = model.Codigo,
                    Id_Empresa = model.Id_Empresa
                };
                var response = GetServicioRamas().AgregarRama(rama);

                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult ModificarRama(int Id)
        {
            var model = GetServicioRamas().ObtenerRama(Id);
            ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
            return View(model);
        }

        [HttpPost]
        public ActionResult ModificarRama(ModificarRamaModel model)
        {
            if (ModelState.IsValid)
            {
                ModificarRamaRequest Medio = new ModificarRamaRequest
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                    Codigo = model.Codigo,
                    Id_Empresa = model.Id_Empresa
                };
                var response = GetServicioRamas().ModificarRama(Medio);

                return RedirectToAction("Index");

            }
            else
            {
                //notificacion.Mensaje = "Atención! Faltan datos o están incorrectos.";
                //notificacion.Tipo = "warning";
                //ViewBag.Notificar = notificacion;

                return View(model);
            }

        }

        public ActionResult EliminarRama(int Id)
        {
            GetServicioRamas().EliminarRama(Id);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ObtenerRamasEmpresa(int IdEmpresa)
        {
            var ramas = GetServicioRamas().ObtenerRamasEmpresa(IdEmpresa).Lista;

            var lista = new List<ListItem> { };

            foreach (var item in ramas)
            {
                lista.Add(new ListItem { Text = item.Nombre, Value = item.Id.ToString() });
            }

            return Json(lista, JsonRequestBehavior.AllowGet);
            //return lista;
        }
    }
}