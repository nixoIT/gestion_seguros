﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Legajos;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
	public class LegajosController : BaseController
	{
		#region Properties
		private ILegajosService servicioLegajos;


		public ILegajosService GetServicioLegajos()
		{
			return servicioLegajos;
		}

		public void SetServicioLegajos(ILegajosService value)
		{
			servicioLegajos = value;
		}
		#endregion Properties

		#region Constructor
		public LegajosController(ILegajosService ServicioLegajos)
		{

			this.SetServicioLegajos(ServicioLegajos);
		}

		#endregion Constructor
		// GET: Legajos
		public ActionResult Index()
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
			|| x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioLegajos().ObtenerLegajos();
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult AgregarLegajo()
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
			|| x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				return View();
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult AgregarLegajo(AgregarLegajoModel model)
		{
			if (ModelState.IsValid)
			{
				var request = new AgregarLegajoRequest
				{
					Nombre = model.Nombre,
					Ubicacion = model.Ubicacion,
					Path = model.Path
				};

				GetServicioLegajos().AgregarLegajo(request);

				return RedirectToAction("Index");
			}
			else
			{
				return View(model);
			}
		}

		public ActionResult ModificarLegajo(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
			|| x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioLegajos().ObtenerLegajo(Id);
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult ModificarLegajo(ModificarLegajoModel model)
		{
			if (ModelState.IsValid)
			{
				var request = new ModificarLegajoRequest
				{
					Id = model.Id,
					Nombre = model.Nombre,
					Ubicacion = model.Ubicacion,
					Path = model.Path
				};

				GetServicioLegajos().ModificarLegajo(request);

				return RedirectToAction("Index");
			}
			else
			{
				return View(model);
			}
		}

		public ActionResult EliminarLegajo(int Id)
		{
			GetServicioLegajos().EliminarLegajo(Id);

			return Json(true, JsonRequestBehavior.AllowGet);
		}
	}
}