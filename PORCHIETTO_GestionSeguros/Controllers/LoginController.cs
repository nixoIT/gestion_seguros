﻿using NIXO_Framework;
using NIXO_Framework.Services;
using PORCHIETTO_GestionSeguros.Models.Login;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System.Linq;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
	public class LoginController : BaseController
	{
		#region Properties
		private ILoginService servicioLogin;
		private INotificacionesService servicioNotificaciones;

		public ILoginService GetServicioLogin()
		{
			return servicioLogin;
		}

		public void SetServicioLogin(ILoginService value)
		{
			servicioLogin = value;
		}

		public INotificacionesService GetServicioNotificaciones()
		{
			return servicioNotificaciones;
		}

		public void SetServicioNotificaciones(INotificacionesService value)
		{
			servicioNotificaciones = value;
		}
		#endregion Properties

		#region Constructor
		public LoginController(ILoginService ServicioLogin, INotificacionesService ServicioNotificaciones)
		{
			this.SetServicioLogin(ServicioLogin);
			this.SetServicioNotificaciones(ServicioNotificaciones);
		}
		#endregion Constructor

		// GET: Login
		[AllowAnonymous]
		public ActionResult Index(string returnUrl)
		{
			ViewBag.Error = "";
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}

		[AllowAnonymous]
		public ActionResult Loguearse(string inputUsuario, string inputPass, string returnUrl)
		{
			var respuesta = GetServicioLogin().LoguearUsuario(new LoginRequest { Usuario = inputUsuario, Pass = inputPass });

			if (respuesta.Aceptado)
			{
				Session["niveles"] = respuesta.Niveles;
				Session["username"] = respuesta.NombreUsuario;
				Session["idusuario"] = respuesta.IdUsuario;
				Session["idcliente"] = 0;
				Session["idproductor"] = 0;

				var idModificarPass = respuesta.IdUsuario.ToString();

				if (respuesta.Niveles.Where(x => x.Nombre == Niveles.Cliente.ToString()).FirstOrDefault() != null
					&& respuesta.Niveles.Where(x => x.Nombre == Niveles.Administrador.ToString()).FirstOrDefault() == null
					&& respuesta.Niveles.Where(x => x.Nombre == Niveles.SysAdmin.ToString()).FirstOrDefault() == null
					&& respuesta.Niveles.Where(x => x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() == null)
				{
					Session["idcliente"] = GetServicioLogin().ObtenerIdCliente(respuesta.IdUsuario);
					idModificarPass = Session["idcliente"].ToString();
				}
				else if (respuesta.Niveles.Where(x => x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null
					&& respuesta.Niveles.Where(x => x.Nombre == Niveles.Administrador.ToString()).FirstOrDefault() == null
					&& respuesta.Niveles.Where(x => x.Nombre == Niveles.SysAdmin.ToString()).FirstOrDefault() == null
					&& respuesta.Niveles.Where(x => x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() == null)
				{
					Session["idproductor"] = GetServicioLogin().ObtenerIdProductor(respuesta.IdUsuario);
					idModificarPass = Session["idproductor"].ToString();
				}

				if (respuesta.CambiarPass)
				{
					return RedirectToAction("ModificarPass/" + idModificarPass, "Usuarios");
				}

				if (!string.IsNullOrEmpty(returnUrl))
				{
					return Redirect(returnUrl);
				}
				else
				{
					return RedirectToAction("Index", "Home");
				}

			}
			else
			{
				ViewBag.Error = "Usuario o Contraseña incorrecta";
				return View("Index");
			}
		}

		public ActionResult LogOut()
		{
			Session["username"] = null;
			Session["niveles"] = null;

			return RedirectToAction("Index", "Home");
		}

		[AllowAnonymous]
		public ActionResult LostPass()
		{
			return View();
		}

		[AllowAnonymous]
		[HttpPost]
		public ActionResult LostPass(LostPassRequest model)
		{
			if (ModelState.IsValid)
			{
				var clienteExistente = GetServicioLogin().ExisteCorreo(model.Email);

				if (clienteExistente != null)
				{
					string asunto = "[Sistema de Gestion] - Restablecer contraseña";
					string correo = string.Format("<h4>Solicitud para restablecer contraseña</h4>" +
						"<table><tr><th>Cliente:</th><th>{0}</th></tr>" +
						"<tr><th>Correo:</th><th>{1}</th></tr>" +
						"<tr><th>Telefono:</th><th>{2}</th></tr>" +
						"<tr><th>Celular:</th><th>{3}</th></tr>" +
						"<tr><th>Id Usuario:</th><th>{4}</th></tr></table>", 
						clienteExistente.NombreCompleto,
						clienteExistente.email,
						clienteExistente.TelefonoFijo,
						clienteExistente.TelefonoMovil,
						clienteExistente.IdUsuario);
					EmailManager.SendEmailRecovery(asunto, correo);
					//Mandar el contacto al correo de porchietto y que le diga que esta persona esta solicitando blanquear la contraseña
					//Devolver un mensaje de que un Administrador se pondra en contacto por correo para recuperar la contraseña
					return View();
				}
				else
				{
					ModelState.AddModelError("Email", "El email ingresado no corresponde a ningun cliente");
					return View(model);
				}
			}
			else
			{
				return View(model);
			}
		}
	}
}