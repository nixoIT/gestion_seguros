﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Polizas;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    public class PolizasController : BaseController
    {
        #region Properties
        private IPolizasService servicioPolizas;
        private IRamasService servicioRamas;
        private IClientesService servicioClientes;
        private IEmpresasService servicioEmpresas;
        private ILegajosService servicioLegajos;
        private IProductoresService servicioProductores;

        public IPolizasService GetServicioPolizas()
        {
            return servicioPolizas;
        }

        public void SetServicioPolizas(IPolizasService value)
        {
            servicioPolizas = value;
        }

        public IRamasService GetServicioRamas()
        {
            return servicioRamas;
        }

        public void SetServicioRamas(IRamasService value)
        {
            servicioRamas = value;
        }

        public IClientesService GetServicioClientes()
        {
            return servicioClientes;
        }

        public void SetServicioClientes(IClientesService value)
        {
            servicioClientes = value;
        }

        public IEmpresasService GetServicioEmpresas()
        {
            return servicioEmpresas;
        }

        public void SetServicioEmpresas(IEmpresasService value)
        {
            servicioEmpresas = value;
        }

        public ILegajosService GetServicioLegajos()
        {
            return servicioLegajos;
        }

        public void SetServicioLegajos(ILegajosService value)
        {
            servicioLegajos = value;
        }

        public IProductoresService GetServicioProductores()
        {
            return servicioProductores;
        }

        public void SetServicioProductores(IProductoresService value)
        {
            servicioProductores = value;
        }
        #endregion Properties

        #region Constructor
        public PolizasController(IPolizasService ServicioPolizas, IClientesService ServicioClientes,
            IEmpresasService ServicioEmpresas, ILegajosService ServicioLegajos,
            IProductoresService ServicioProductores, IRamasService ServicioRamas)
        {
            this.SetServicioPolizas(ServicioPolizas);
            this.SetServicioClientes(ServicioClientes);
            this.SetServicioEmpresas(ServicioEmpresas);
            this.SetServicioLegajos(ServicioLegajos);
            this.SetServicioProductores(ServicioProductores);
            this.SetServicioRamas(ServicioRamas);
        }

        #endregion Constructor

        // GET: Polizas
        #region Polizas
        public ActionResult Index(int? id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() 
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                var request = new PolizaListadoRequest { };

                if ((int)Session["idproductor"] == 0)
                {
                    ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                    request.IdProductor = id.HasValue ? id : 0;
                }
                else
                {
                    request.IdProductor = (int)Session["idproductor"];
                }
                var model = GetServicioPolizas().ObtenerPolizas(request);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult AgregarPoliza()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() 
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                ViewBag.Clientes = new SelectList(GetServicioClientes().ObtenerClientes().Lista, "Id", "NombreCompleto");
                ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
                ViewBag.Legajos = new SelectList(GetServicioLegajos().ObtenerLegajos().Lista, "Id", "Ubicacion");
                ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                //ViewBag.Ramas = new SelectList(GetServicioRamas().ObtenerRama().Lista, "Id", "Nombre");

                AgregarPolizaModel model = new AgregarPolizaModel { };
                if ((int)Session["idproductor"] != 0)
                    model.IdProductor = (int)Session["idproductor"];

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult AgregarPoliza(AgregarPolizaModel model)
        {
            if (ModelState.IsValid)
            {
                AgregarPolizaRequest poliza = new AgregarPolizaRequest
                {
                    Id = model.Id,
                    Numero = model.Numero,
                    IdCliente = model.IdCliente,
                    Cobertura = model.Cobertura,
                    IdEmpresa = model.IdEmpresa,
                    IdProductor = model.IdProductor,
                    IdRama = model.IdRama,
                    Suplemento = model.Suplemento,
                    Vencimiento = model.Vencimiento,
                    Dominio = model.Dominio,
                    Unidad_Nomina = model.Unidad_Nomina
                };

                GetServicioPolizas().AgregarPoliza(poliza);

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Clientes = new SelectList(GetServicioClientes().ObtenerClientes().Lista, "Id", "NombreCompleto");
                ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
                ViewBag.Legajos = new SelectList(GetServicioLegajos().ObtenerLegajos().Lista, "Id", "Ubicacion");
                ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                //ViewBag.Ramas = new SelectList(GetServicioRamas().ObtenerRama().Lista, "Id", "Nombre");

                model.IdEmpresa = 0;
                model.IdRama = 0;
                return View(model);
            }
        }

        public ActionResult ModificarPoliza(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() 
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                ViewBag.Clientes = new SelectList(GetServicioClientes().ObtenerClientes().Lista, "Id", "NombreCompleto");
                ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
                ViewBag.Legajos = new SelectList(GetServicioLegajos().ObtenerLegajos().Lista, "Id", "Ubicacion");
                ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                ViewBag.Ramas = new SelectList(GetServicioRamas().ObtenerRama().Lista, "Id", "Nombre");
                var model = GetServicioPolizas().ObtenerPoliza(Id);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult ModificarPoliza(ModificarPolizaModel model)
        {
            if (ModelState.IsValid)
            {
                ModificarPolizaRequest poliza = new ModificarPolizaRequest
                {
                    Id = model.Id,
                    Numero = model.Numero,
                    IdCliente = model.IdCliente,
                    Cobertura = model.Cobertura,
                    IdEmpresa = model.IdEmpresa,
                    IdProductor = model.IdProductor,
                    IdRama = model.IdRama,
                    Suplemento = model.Suplemento,
                    Vencimiento = model.Vencimiento,
                    Dominio = model.Dominio,
                    Unidad_Nomina = model.Unidad_Nomina
                };
                GetServicioPolizas().ModificarPoliza(poliza);

                return RedirectToAction("Index", "Polizas");

            }
            else
            {
                //notificacion.Mensaje = "Atención! Faltan datos o están incorrectos.";
                //notificacion.Tipo = "warning";
                //ViewBag.Notificar = notificacion;

                return View(model);
            }

        }

        public ActionResult EliminarPoliza(int Id)
        {
            GetServicioPolizas().EliminarPoliza(Id);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PolizasBorradas(int? id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                var request = new PolizaListadoRequest { };

                if ((int)Session["idproductor"] == 0)
                {
                    ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                    request.IdProductor = id.HasValue ? id : 0;
                }
                else
                {
                    request.IdProductor = (int)Session["idproductor"];
                }

                var model = GetServicioPolizas().ObtenerPolizasBorradas(request);

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult PolizasVencidas(int? id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                var request = new PolizaListadoRequest { };

                if ((int)Session["idproductor"] == 0)
                {
                    ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
                    request.IdProductor = id.HasValue ? id : 0;
                }
                else
                {
                    request.IdProductor = (int)Session["idproductor"];
                }
                
                var model = GetServicioPolizas().ObtenerPolizasVencidas(request);

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
            
        }
        #endregion

        #region Polizas Cliente
        public ActionResult MisPolizas()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Cliente.ToString() || x.Nombre == Niveles.Administrador.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioPolizas().ObtenerMisPolizas((int)Session["idcliente"]);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult VerPoliza(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Cliente.ToString() || x.Nombre == Niveles.Administrador.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioPolizas().ObtenerPoliza(Id);
                if (model.IdCliente == (int)Session["idcliente"])
                {
                    ViewBag.Cliente = GetServicioClientes().ObtenerCliente(model.IdCliente).NombreCompleto;
                    ViewBag.Empresa = GetServicioEmpresas().ObtenerEmpresa(model.IdEmpresa).Nombre;
                    ViewBag.Productor = GetServicioProductores().ObtenerProductor(model.IdProductor).Nombre;
                    ViewBag.Rama = GetServicioRamas().ObtenerRama(model.IdRama).Nombre;
					ViewBag.Notificaciones = GetServicioPolizas().ObtenerNotificacionesPolizasCliente(model.Id).Lista;
					return View(model);
                }
                else
                    return RedirectToAction("Index", "Home");
            }
            else
                return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Cargar Pólizas
        public ActionResult CargarPoliza()
        {
            ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
            ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");

            return View();
        }

        [HttpPost]
        public ActionResult CargarPoliza(HttpPostedFileBase PostedFile, int IdEmpresa, int IdProductor)
        {
            var Polizas = new List<ImportarPolizaModel> { };
            string path = Server.MapPath("~/Uploads/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (PostedFile != null)
            {
                string fileName = Path.GetFileName(PostedFile.FileName);
                PostedFile.SaveAs(path + fileName);
                var validado = GetServicioPolizas().ValidarArchivo(TipoPlantilla.POLIZAS, path + fileName, IdEmpresa, out Polizas);

                if (validado)
                {
                    Polizas.ForEach(x => x.IdEmpresa = IdEmpresa);
                    Polizas.ForEach(x => x.IdProductor = IdProductor);

                    if (GetServicioPolizas().ImportarPlantillaPoliza(Polizas))
                    {
                        ViewBag.Mensaje = "Importación exitosa.";
                    }
                    else
                    {
                        ViewBag.Mensaje = "La importación no se pudo completar.";
                    }
                }
                else
                {
                    ViewBag.Mensaje = "Formato de plantilla no válido.";
                }
            }
            else
            {
                ViewBag.Mensaje = "No se pudo obtener el archivo.";
            }

            ViewBag.Empresas = new SelectList(GetServicioEmpresas().ObtenerEmpresas().Lista, "Id", "Nombre");
            ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
            return View();
        }
        #endregion

        #region Notificaciones Pólizas

        public ActionResult Notificaciones(int Id)
        {
            var model = GetServicioPolizas().ObtenerInformeNotificacionesPolizas(Id);
            return View(model);
        }

        public ActionResult EliminarNotificacionPoliza(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                GetServicioPolizas().EliminarNotificacionPolizas(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public JsonResult AgregarNotificacion(int IdPoliza, DateTime FechaInicio, DateTime FechaFin, string Mensaje, bool VisibleCliente)
        {
            var nuevaNotificacion = new NotificacionesPolizasModel
            {
                IdPoliza = IdPoliza,
                FechaInicio = FechaInicio,
                FechaFinal = FechaFin,
                Mensaje = Mensaje,
                VisibleCliente = VisibleCliente,
                IdUsuario = (int)Session["idusuario"]
            };

            GetServicioPolizas().AgregarNotificaciones(nuevaNotificacion);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}