﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Productores;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
	public class ProductoresController : BaseController
	{
		#region Properties
		private IProductoresService servicioProductores;


		public IProductoresService GetServicioProductores()
		{
			return servicioProductores;
		}

		public void SetServicioProductores(IProductoresService value)
		{
			servicioProductores = value;
		}
		#endregion Properties

		#region Constructor
		public ProductoresController(IProductoresService ServicioProductores)
		{

			this.SetServicioProductores(ServicioProductores);
		}

		#endregion Constructor
		// GET: Productores
		public ActionResult Index()
		{
			var model = GetServicioProductores().ObtenerProductores();
			return View(model);
		}

		public ActionResult AgregarProductor()
		{
			return View();
		}

		[HttpPost]
		public ActionResult AgregarProductor(AgregarProductorModel model)
		{
			if (ModelState.IsValid)
			{
				var request = new AgregarProductorRequest
				{
					Nombre = model.Nombre,
					CUIT = model.CUIT,
					Domicilio = model.Domicilio,
					Email = model.Email,
					Telefono = model.Telefono,
					TelefonoAlternativo = model.TelefonoAlternativo,
					Web = model.Web,
					ActividadPrincipal = model.ActividadPrincipal,
					ActividadSecundaria = model.ActividadSecundaria
				};

				GetServicioProductores().AgregarProductor(request);

				return RedirectToAction("Index");
			}
			else
			{
				return View();
			}
		}

		public ActionResult ModificarProductor(int Id)
		{
			var model = GetServicioProductores().ObtenerProductor(Id);
			return View(model);
		}

		[HttpPost]
		public ActionResult ModificarProductor(ModificarProductorModel model)
		{
			if (ModelState.IsValid)
			{
				var request = new ModificarProductorRequest
				{
					Id = model.Id,
					Nombre = model.Nombre,
					CUIT = model.CUIT,
					Domicilio = model.Domicilio,
					Email = model.Email,
					Telefono = model.Telefono,
					TelefonoAlternativo = model.TelefonoAlternativo,
					Web = model.Web,
					ActividadPrincipal = model.ActividadPrincipal,
					ActividadSecundaria = model.ActividadSecundaria
				};

				GetServicioProductores().ModificarProductor(request);
			}

			return RedirectToAction("Index");
		}

		public ActionResult EliminarProductor(int Id)
		{
			GetServicioProductores().EliminarProductor(Id);

			return Json(true, JsonRequestBehavior.AllowGet);
		}

		public ActionResult DetalleProductor(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				var model = new DetalleProductorModel
				{
					Productor = GetServicioProductores().ObtenerProductor(Id),
					Usuario = GetServicioProductores().ObtenerUsuarioCliente(Id)
				};
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult AgregarUsuarioProductor(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				var productor = GetServicioProductores().ObtenerProductor(Id);
				AgregarUsuarioProductorModel model = new AgregarUsuarioProductorModel()
				{
					Nombre = productor.Nombre,
					Apellido = productor.Apellido,
					IdProductor = productor.Id,
					Email = productor.Email
				};
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult AgregarUsuarioProductor(AgregarUsuarioProductorModel model)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				if (ModelState.IsValid)
				{
					var existe = GetServicioProductores().ExisteUsuario(model.Usuario);
					if (existe)
					{
						ModelState.AddModelError("Usuario", "El usuario ya existe");
						return View(model);
					}

					AgregarUsuarioProductorRequest Usuario = new AgregarUsuarioProductorRequest
					{
						Nombre = model.Nombre,
						Apellido = model.Apellido,
						Usuario = model.Usuario,
						Pass = Seguridad.GetHashSha256(model.Pass),
						FechaAlta = DateTime.Today,
						IdProductor = model.IdProductor,
						Email = model.Email
					};
					var response = GetServicioProductores().AgregarUsuarioProductor(Usuario);

					if (response.Id > 0)
					{
						return RedirectToAction("Index", "Productores");
					}
					else
					{
						ModelState.AddModelError("Usuario", "Error al intentar grabar el usuario");
						return View(model);
					}

				}
				else
				{
					return View(model);
				}
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult EliminarUsuarioProductor(int idUsuario, int idProductor)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var response = GetServicioProductores().BorrarUsuarioProductor(idUsuario, idProductor);

				return RedirectToAction("DetalleProductor/" + idProductor.ToString(), "Productores");
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult BlanquearPassProductor(int idUsuario, int idProductor)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var response = GetServicioProductores().BlanquearPassUsuarioProductor(idUsuario);

				return RedirectToAction("DetalleProductor/" + idProductor.ToString(), "Productores");
			}
			else
				return RedirectToAction("Index", "Home");
		}
	}
}