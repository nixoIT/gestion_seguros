﻿using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Web.Mvc;
using NIXO_Framework;
using System.Collections.Generic;
using System.Linq;

namespace PORCHIETTO_GestionSeguros.Controllers
{
	public class UsuariosController : BaseController
	{
		#region Properties
		private IUsuariosService servicioUsuarios;
		private IClientesService servicioClientes;
		public IUsuariosService GetServicioUsuarios()
		{
			return servicioUsuarios;
		}

		public void SetServicioUsuarios(IUsuariosService value)
		{
			servicioUsuarios = value;
		}

		public IClientesService GetServicioClientes()
		{
			return servicioClientes;
		}

		public void SetServicioClientes(IClientesService value)
		{
			servicioClientes = value;
		}
		#endregion Properties

		#region Constructor
		public UsuariosController(IUsuariosService ServicioUsuarios, IClientesService ServicioClientes)
		{
			this.SetServicioUsuarios(ServicioUsuarios);
			this.SetServicioClientes(ServicioClientes);
		}

		#endregion Constructor

		// GET: Usuarios
		public ActionResult Index()
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioUsuarios().ObtenerUsuarios();

				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult AgregarUsuario()
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				return View();
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult AgregarUsuario(AgregarUsuarioModel model)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				if (ModelState.IsValid)
				{
					AgregarUsuarioRequest Usuario = new AgregarUsuarioRequest
					{
						Nombre = model.Nombre,
						Apellido = model.Apellido,
						Usuario = model.Usuario,
						Pass = Seguridad.GetHashSha256(model.Pass),
						FechaAlta = DateTime.Today
					};
					var response = GetServicioUsuarios().AgregarUsuario(Usuario);

					if (response.Id > 0)
					{
						var modelo = new UsuariosModel();
						modelo = GetServicioUsuarios().ObtenerUsuarios();

						return View("Index", modelo);
					}
					else
					{
						return View(model);
					}
				}
				else
				{
					return View(model);
				}
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult ModificarUsuario(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioUsuarios().ObtenerUsuarioAEditar(Id);

				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult ModificarUsuario(ModificarUsuarioModel model)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				if (ModelState.IsValid)
				{
					ModificarUsuarioRequest Persona = new ModificarUsuarioRequest
					{
						Id = model.Id,
						Nombre = model.Nombre,
						Apellido = model.Apellido,
						Telefono = model.Telefono,
						Email = model.Email
					};
					var response = GetServicioUsuarios().ModificarUsuario(Persona);
					return RedirectToAction("Index", "Usuarios");

				}
				else
				{
					return View(model);
				}
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult EliminarUsuario(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				GetServicioUsuarios().EliminarUsuario(Id);

				//Genero la notificación
				//Notificaciones notificacion = new Notificaciones
				//{
				//    Mensaje = "Usuario eliminado correctamente.",
				//    Tipo = "success"
				//};
				//ViewBag.Notificar = notificacion;
				//Genero la notificación

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult DetalleUsuario(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioUsuarios().ObtenerUsuario(Id);

				//Genero la notificación
				//Notificaciones notificacion = new Notificaciones
				//{
				//    Mensaje = ""
				//};
				//ViewBag.Notificar = notificacion;
				//Genero la notificación 

				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult ModificarPass(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioUsuarios().ObtenerUsuario(Id);

				return View(new ModificarPassModel { Id = model.Id });
			}
			else if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Cliente.ToString()).FirstOrDefault() != null && (int)Session["idcliente"] == Id)
			{
				var model = GetServicioUsuarios().ObtenerUsuarioCliente(Id);

				return View(new ModificarPassModel { Id = model.Id });
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult ModificarPass(ModificarPassModel model)
		{
			// Administrador
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				if (ModelState.IsValid)
				{
					var passanterior = GetServicioUsuarios().ObtenerPass(model.Id);

					if (passanterior == Seguridad.GetHashSha256(model.PassAntigua))
					{
						if (model.PassAntigua != model.Pass)
						{
							ModificarPassRequest Password = new ModificarPassRequest
							{
								Id = model.Id,
								Pass = Seguridad.GetHashSha256(model.Pass)
							};
							var response = GetServicioUsuarios().ModificarPass(Password);

							if (response.Id > 0)
							{
								return RedirectToAction("Index", "Usuarios");
							}
							else
							{
								return View(model);
							}
						}
						else
						{
							return View(model);
						}
					}
					else
					{
						return View(model);
					}
				}
				else
				{
					return View(model);
				}
			}
			// Cliente
			else if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Cliente.ToString()).FirstOrDefault() != null
				|| model.Id == (int)Session["idcliente"])
			{
				if (ModelState.IsValid)
				{
					var passanterior = GetServicioUsuarios().ObtenerPassCliente(model.Id);

					if (passanterior == Seguridad.GetHashSha256(model.PassAntigua))
					{
						if (model.PassAntigua != model.Pass)
						{
							ModificarPassRequest Password = new ModificarPassRequest
							{
								Id = model.Id,
								Pass = Seguridad.GetHashSha256(model.Pass)
							};
							var response = GetServicioUsuarios().ModificarPassCliente(Password);

							if (response.Id > 0)
							{
								return RedirectToAction("Index", "Home");
							}
							else
							{
								return View(model);
							}
						}
						else
						{
							return View(model);
						}
					}
					else
					{
						return View(model);
					}
				}
				else
				{
					return View(model);
				}
			}
			// Productor
			else if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Productor.ToString()).FirstOrDefault() != null
				|| model.Id == (int)Session["idproductor"])
			{

				if (ModelState.IsValid)
				{
					var passanterior = GetServicioUsuarios().ObtenerPassProductor(model.Id);

					if (passanterior == Seguridad.GetHashSha256(model.PassAntigua))
					{
						if (model.PassAntigua != model.Pass)
						{
							ModificarPassRequest Password = new ModificarPassRequest
							{
								Id = model.Id,
								Pass = Seguridad.GetHashSha256(model.Pass)
							};
							var response = GetServicioUsuarios().ModificarPassProductor(Password);

							if (response.Id > 0)
							{
								return RedirectToAction("Index", "Home");
							}
							else
							{
								return View(model);
							}
						}
						else
						{
							return View(model);
						}
					}
					else
					{
						return View(model);
					}
				}
				else
				{
					return View(model);
				}
			}
			else
				return RedirectToAction("LogOut", "Login");
		}

		public ActionResult BlanquearPass(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				GetServicioClientes().BlanquearPassUsuarioCliente(Id);

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult ListarNiveles(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioUsuarios().ObtenerNivelesUsuario(Id);
				var user = GetServicioUsuarios().ObtenerUsuario(Id);
				ViewBag.Usuario = user.Usuario.ToString();
				ViewBag.UsuarioId = user.Id;
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpGet]
		public ActionResult AgregarNivel(int id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				ViewBag.Niveles = new SelectList(GetServicioUsuarios().ObtenerNiveles().Lista, "Id", "Nombre");
				var user = GetServicioUsuarios().ObtenerUsuario(id);
				ViewBag.Usuario = user.Usuario;
				TempData["IdUsuario"] = id.ToString();
				return View();
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult AgregarNivel(string IdNivel)
		{
			var IdUsuario = TempData["IdUsuario"].ToString();
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				GetServicioUsuarios().AgregarNivelUsuario(int.Parse(IdUsuario), int.Parse(IdNivel));

				return RedirectToAction("ListarNiveles", new { Id = int.Parse(IdUsuario) });
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult EliminarNivelUsuario(int IdUsuario, int IdNivel)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString()).FirstOrDefault() != null)
			{
				GetServicioUsuarios().EliminarNivelUsuario(IdUsuario, IdNivel);

				//Genero la notificación
				//Notificaciones notificacion = new Notificaciones
				//{
				//    Mensaje = "Usuario eliminado correctamente.",
				//    Tipo = "success"
				//};
				//ViewBag.Notificar = notificacion;
				//Genero la notificación

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			else
				return RedirectToAction("Index", "Home");
		}
	}
}