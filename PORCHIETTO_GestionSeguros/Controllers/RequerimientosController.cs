﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Empresas;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    public class RequerimientosController : BaseController
    {
        #region Properties
        private IEmpresasService servicioEmpresas;

        public IEmpresasService GetServicioEmpresas()
        {
            return servicioEmpresas;
        }

        public void SetServicioEmpresas(IEmpresasService value)
        {
            servicioEmpresas = value;
        }

        private IRamasService servicioRamas;

        public IRamasService GetServicioRamas()
        {
            return servicioRamas;
        }

        public void SetServicioRamas(IRamasService value)
        {
            servicioRamas = value;
        }

        #endregion Properties

        #region Constructor
        public RequerimientosController(IEmpresasService ServicioEmpresas, IRamasService ServicioRamas)
        {
            this.SetServicioEmpresas(ServicioEmpresas);
            this.SetServicioRamas(ServicioRamas);
        }
        #endregion Constructor

        // GET: Requerimientos
        public ActionResult Index()
        {
            var model = GetServicioEmpresas().ObtenerRequerimientos();
            //ViewBag.Empresas = GetServicioEmpresas().ObtenerEmpresas().Lista;
            return View(model);
        }

        /// <summary>
        /// Agregar un nuevo Requerimiento
        /// </summary>
        /// <returns></returns>
        public ActionResult AgregarRequerimiento()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult AgregarRequerimiento(AgregarRequerimientoModel model)
        {
            if (ModelState.IsValid)
            {
                AgregarRequerimientoRequest Requerimiento = new AgregarRequerimientoRequest
                {
                    Nombre = model.Nombre,
                };
                var response = GetServicioEmpresas().AgregarRequerimiento(Requerimiento);

                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        /// <summary>
        /// Modifica el requerimiento
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult ModificarRequerimiento(int Id)
        {
            var model = GetServicioEmpresas().ObtenerRequerimiento(Id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ModificarRequerimiento(AgregarRequerimientoRequest model)
        {
            if (ModelState.IsValid)
            {
                ModificarRequerimientoRequest Medio = new ModificarRequerimientoRequest
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                };
                var response = GetServicioEmpresas().ModificarRequerimiento(Medio);

                return RedirectToAction("Index");

            }
            else
            {
                //notificacion.Mensaje = "Atención! Faltan datos o están incorrectos.";
                //notificacion.Tipo = "warning";
                //ViewBag.Notificar = notificacion;

                return View(model);
            }

        }

        /// <summary>
        /// Elimina el requerimiento seleccionado
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult EliminarRequerimiento(int Id)
        {
            GetServicioEmpresas().EliminarRequerimiento(Id);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #region REQUERIMIENTOS-EMPRESA
        public ActionResult ObtenerRequerimientosEmpresa(int IdEmpresa)
        {
            var Requerimientos = GetServicioEmpresas().ObtenerRequerimientosEmpresa(IdEmpresa).Lista;

            var lista = new List<ListItem> { };

            foreach (var item in Requerimientos)
            {
                lista.Add(new ListItem { Text = item.Nombre, Value = item.Id.ToString() });
            }

            return Json(lista, JsonRequestBehavior.AllowGet);
            //return lista;
        }

        /// <summary>
        /// Agrega un requerimiento de una lista a una empresa
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult AgregarRequerimientoEmpresa(int? Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                if (Id == null)
                    return RedirectToAction("Index", "Empresas");

                ViewBag.Empresa = GetServicioEmpresas().ObtenerEmpresa(Id ?? 1);
                ViewBag.ListaRamas = new SelectList(GetServicioRamas().ObtenerRamasEmpresa(Id ?? 1).Lista, "Id", "Nombre");
                ViewBag.ListaReq = new SelectList(GetServicioEmpresas().ObtenerRequerimientos().Lista, "Id", "Nombre");
                
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult AgregarRequerimientoEmpresa(AgregarRequerimientoEmpresaModel model)
        {
            if (ModelState.IsValid)
            {
                AgregarRequerimientoEmpresaRequest RequerimientoEmpresa = new AgregarRequerimientoEmpresaRequest
                {
                    Id_Empresa = model.Id_Empresa,
                    Id_Rama = model.Id_Rama,
                    Id_Requerimiento = model.Id_Requerimiento
                };
                var response = GetServicioEmpresas().AgregarRequerimientoEmpresa(RequerimientoEmpresa);

                return RedirectToAction("ListarRequerimientosEmpresa", "Empresas", new { Id = model.Id_Empresa});
            }
            else
            {
                return View(model);
            }
        }

        /// <summary>
        /// Elimina el requerimiento seleccionado de una empresa
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult EliminarRequerimientoEmpresa(int Id, int IdEmpresa)
        {
            GetServicioEmpresas().EliminarRequerimientoEmpresa(Id, IdEmpresa);

            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}