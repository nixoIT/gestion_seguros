﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.MediosDeCobranza;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using Rotativa;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
	public class MediosDeCobranzasController : BaseController
	{
		#region Properties
		private IMediosDeCobranzaService servicioMediosDeCobranzas;
		private IClientesService servicioClientes;
		private IProductoresService servicioProductores;
		public IMediosDeCobranzaService GetServicioMediosDeCobranzas()
		{
			return servicioMediosDeCobranzas;
		}
		public IClientesService GetServicioClientes()
		{
			return servicioClientes;
		}

		public void SetServicioMediosDeCobranzas(IMediosDeCobranzaService value)
		{
			servicioMediosDeCobranzas = value;
		}
		public void SetServicioClientes(IClientesService value)
		{
			servicioClientes = value;
		}
		public IProductoresService GetServicioProductores()
		{
			return servicioProductores;
		}
		public void SetServicioProductores(IProductoresService value)
		{
			servicioProductores = value;
		}
		#endregion Properties

		#region Constructor
		public MediosDeCobranzasController(IMediosDeCobranzaService ServicioMediosDeCobranzas, IClientesService ServicioClientes, IProductoresService ServicioProductores)
		{
			this.SetServicioMediosDeCobranzas(ServicioMediosDeCobranzas);
			this.SetServicioClientes(ServicioClientes);
			this.SetServicioProductores(ServicioProductores);
		}

		#endregion Constructor

		// GET: Usuarios
		public ActionResult Index()
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
			|| x.Nombre == Niveles.Empleado.ToString()
			|| x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioMediosDeCobranzas().ObtenerMediosDeCobranza();

				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult AgregarMediosDeCobranza()
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				return View();
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult AgregarMediosDeCobranza(AgregarMediosDeCobranzaModel model)
		{
			if (ModelState.IsValid)
			{
				AgregarMediosDeCobranzaRequest Medios = new AgregarMediosDeCobranzaRequest
				{
					Nombre = model.Nombre
				};
				var response = GetServicioMediosDeCobranzas().AgregarMediosDeCobranza(Medios);

				if (response.Id > 0)
				{
					var modelo = new MediosDeCobranzaModel();
					modelo = GetServicioMediosDeCobranzas().ObtenerMediosDeCobranza();

					return View("Index", modelo);
				}
				else
				{
					return View(model);
				}
			}
			else
			{
				return View(model);
			}
		}

		public ActionResult ModificarMediosDeCobranza(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				var model = GetServicioMediosDeCobranzas().ObtenerMediosDeCobranzaAEditar(Id);
				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		public ActionResult ModificarMediosDeCobranza(ModificarMediosDeCobranzaModel model)
		{
			if (ModelState.IsValid)
			{
				ModificarMediosDeCobranzaRequest Medio = new ModificarMediosDeCobranzaRequest
				{
					Id = model.Id,
					Nombre = model.Nombre,
				};
				var response = GetServicioMediosDeCobranzas().ModificarMediosDeCobranza(Medio);

				//if (response.Id > 0)
				//{
				//    notificacion.Mensaje = "Usuario guardado correctamente.";
				//    notificacion.Tipo = "success";
				//}
				//else
				//{
				//    notificacion.Mensaje = "Error! No se pudo guardar el usuario.";
				//    notificacion.Tipo = "danger";
				//}

				//ViewBag.Notificar = notificacion;

				return RedirectToAction("Index", "Usuarios");

			}
			else
			{
				//notificacion.Mensaje = "Atención! Faltan datos o están incorrectos.";
				//notificacion.Tipo = "warning";
				//ViewBag.Notificar = notificacion;

				return View(model);
			}

		}

		public ActionResult EliminarMediosDeCobranza(int Id)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
			{
				GetServicioMediosDeCobranzas().EliminarMediosDeCobranza(Id);
				//Genero la notificación
				//Notificaciones notificacion = new Notificaciones
				//{
				//    Mensaje = "Usuario eliminado correctamente.",
				//    Tipo = "success"
				//};
				//ViewBag.Notificar = notificacion;
				//Genero la notificación

				return Json(true, JsonRequestBehavior.AllowGet);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		public ActionResult Reportes(int? IdMedio, int? IdProductor)
		{
			if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString()
			|| x.Nombre == Niveles.Empleado.ToString()
			|| x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
			{
				ViewBag.Medios = new SelectList(GetServicioClientes().ObtenerMediosDePago().Lista, "Id", "Nombre");
				var request = new GetReportePorClienteRequest
				{
					IdMedioDePago = IdMedio.HasValue ? IdMedio : 0,
					IdProductor = (int)Session["idproductor"]
				};

				if ((int)Session["idproductor"] == 0)
				{
					ViewBag.Productores = new SelectList(GetServicioProductores().ObtenerProductores().Lista, "Id", "Nombre");
					request.IdProductor = IdProductor.HasValue ? IdProductor : 0;
				}
				else
				{
					request.IdProductor = (int)Session["idproductor"];
				}

				var model = GetServicioMediosDeCobranzas().GetReportesPorCliente(request);

				return View(model);
			}
			else
				return RedirectToAction("Index", "Home");
		}

		//[HttpPost]
		//public ActionResult Reportes(int Id)
		//{
		//    ViewBag.Medios = new SelectList(GetServicioClientes().ObtenerMediosDePago().Lista, "Id", "Nombre");
		//    var request = new GetReportePorClienteRequest
		//    {
		//        IdMedioDePago = Id
		//    };
		//    var model = GetServicioMediosDeCobranzas().GetReportesPorCliente(request);

		//    return PartialView("_Clientes", model);
		//}

		[AllowAnonymous]
		public ActionResult GetReporte(int IdMedio, int IdProductor)
		{
			if (IdMedio == 0)
				ViewBag.Cobrador = "<Todos>";
			else
				ViewBag.Cobrador = GetServicioMediosDeCobranzas().ObtenerMediosDeCobranza(IdMedio).Nombre.ToString();

			if (IdProductor == 0)
				ViewBag.Productor = "<Todos>";
			else
			{
				var productor = GetServicioProductores().ObtenerProductor(IdProductor);
				ViewBag.Productor = productor.Nombre.ToString() + " " + productor.Apellido?.ToString();
			}

			var request = new GetReportePorClienteRequest
			{
				IdMedioDePago = IdMedio,
				IdProductor = IdProductor
			};
			var reporte = GetServicioMediosDeCobranzas().GetReportesPorCliente(request);
			return View(reporte);
		}

		[AllowAnonymous]
		public ActionResult Pdf(int idMedio, int idProductor)
		{
			//Guarda directamente el PDF
			//return new Rotativa.ActionAsPdf("GetReporte/" + id.ToString()) { FileName = "Presupuesto_"+ id.ToString() + ".pdf" };
			//Previsualiza el PDF
			return new Rotativa.ActionAsPdf("GetReporte/" + idMedio.ToString() + "/" + idProductor.ToString());
			//return new ActionAsPdf("GetReporte", new { Id = id.ToString() });
		}
	}
}