﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    public class NotificacionesController : BaseController
    {
        #region Properties
        private INotificacionesService servicioNotificaciones;

        public INotificacionesService GetServicioNotificaciones()
        {
            return servicioNotificaciones;
        }

        public void SetServicioNotificaciones(INotificacionesService value)
        {
            servicioNotificaciones = value;
        }
        #endregion Properties

        #region Constructor
        public NotificacionesController(INotificacionesService ServicioNotificaciones)
        {
            this.SetServicioNotificaciones(ServicioNotificaciones);
        }

        #endregion Constructor

        // GET: Notificaciones
        public ActionResult Index()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() 
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult Informe(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() 
            || x.Nombre == Niveles.Empleado.ToString()
            || x.Nombre == Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                var dias = int.Parse(ConfigurationManager.AppSettings["DiasVencimiento"].ToString());
                var notificaciones = GetServicioNotificaciones().ObtenerInformeNotificaciones(new ObtenerInformeRequest
                {
                    Fecha = DateTime.Today.AddDays(dias),
                    Tipo = (TipoNotificaciones)Id,
                    IdUsuario = (int)Session["idproductor"] != 0 ? (int)Session["idusuario"] : 0,
                    IdProductor = (int)Session["idproductor"] != 0 ? (int)Session["idproductor"] : 0
                }).Lista.ToList();

                var model = new InformeViewModel
                {
                    Lista = notificaciones,
                    Tipo = (TipoNotificaciones)Id
                };

                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult MarcarNotificacionesResueltas(List<MarcarResueltos> request)
        {
            GetServicioNotificaciones().MarcarResueltos(new MarcarResueltosRequest { Lista = request.ToList() });
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}