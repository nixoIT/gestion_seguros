﻿using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using static PORCHIETTO_GestionSeguros.Models.Notificaciones.NotificacionesModel;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    public class HomeController : BaseController
    {
        #region Properties
        private INotificacionesService servicioNotificaciones;

        public INotificacionesService GetServicioNotificaciones()
        {
            return servicioNotificaciones;
        }

        public void SetServicioNotificaciones(INotificacionesService value)
        {
            servicioNotificaciones = value;
        }
        #endregion Properties

        #region Constructor
        public HomeController(INotificacionesService ServicioNotificaciones)
        {
            this.SetServicioNotificaciones(ServicioNotificaciones);
        }
        #endregion Constructor

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ObtenerNotificaciones()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == NIXO_Framework.Niveles.Administrador.ToString() 
            || x.Nombre == NIXO_Framework.Niveles.Empleado.ToString()
            || x.Nombre == NIXO_Framework.Niveles.Productor.ToString()).FirstOrDefault() != null)
            {
                var dias = int.Parse(ConfigurationManager.AppSettings["DiasVencimiento"].ToString());
                var notificaciones = GetServicioNotificaciones().ObtenerNotificaciones(new ObtenerNotificacionesRequest
                {
                    Fecha = DateTime.Today.AddDays(dias),
                    Tipo = TipoNotificaciones.Todas,
                    IdUsuario = (int)Session["idproductor"] != 0 ? (int)Session["idusuario"] : 0,
                    IdProductor = (int)Session["idproductor"] != 0 ? (int)Session["idproductor"] : 0
                }).Lista.ToList();

                return Json(notificaciones, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }
    }
}