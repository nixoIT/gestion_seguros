﻿using NIXO_Framework;
using PORCHIETTO_GestionSeguros.Models.Empresas;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Servicios;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Controllers
{
    public class EmpresasController : BaseController
    {
        #region Properties
        private IEmpresasService servicioEmpresas;

        public IEmpresasService GetServicioEmpresas()
        {
            return servicioEmpresas;
        }

        public void SetServicioEmpresas(IEmpresasService value)
        {
            servicioEmpresas = value;
        }

        private IRamasService servicioRamas;

        public IRamasService GetServicioRamas()
        {
            return servicioRamas;
        }

        public void SetServicioRamas(IRamasService value)
        {
            servicioRamas = value;
        }
        #endregion Properties

        #region Constructor
        public EmpresasController(IEmpresasService ServicioEmpresas, IRamasService ServicioRamas)
        {
            this.SetServicioEmpresas(ServicioEmpresas);
            this.SetServicioRamas(ServicioRamas);
        }

        #endregion Constructor
        // GET: Empresas
        public ActionResult Index()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioEmpresas().ObtenerEmpresas();
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult AgregarEmpresa()
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult AgregarEmpresa(AgregarEmpresaViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new AgregarEmpresaRequest
                {
                    Nombre = model.Nombre,
                    CUIT = model.CUIT,
                    Domicilio = model.Domicilio,
                    Telefono = model.Telefono,
                    Web = model.Web,
                    ActividadPrincipal = model.ActividadPrincipal,
                    ActividadSecundaria = model.ActividadSecundaria
                };

                GetServicioEmpresas().AgregarEmpresa(request);

                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public ActionResult ModificarEmpresa(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioEmpresas().ObtenerEmpresa(Id);
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult ModificarEmpresa(ModificarEmpresaViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new ModificarEmpresaRequest
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                    CUIT = model.CUIT,
                    Domicilio = model.Domicilio,
                    Telefono = model.Telefono,
                    Web = model.Web,
                    ActividadPrincipal = model.ActividadPrincipal,
                    ActividadSecundaria = model.ActividadSecundaria
                };

                GetServicioEmpresas().ModificarEmpresa(request);
            }

            return RedirectToAction("Index");
        }

        public ActionResult EliminarEmpresa(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                GetServicioEmpresas().EliminarEmpresa(Id);  
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        // RAMAS
        public ActionResult ListarRamasEmpresa(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioRamas().ObtenerRamasEmpresa(Id);
                var empresa = GetServicioEmpresas().ObtenerEmpresa(Id);
                ViewBag.Empresa = empresa.Nombre;
                ViewBag.EmpresaId = empresa.Id;
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        // REQUERIMIENTOS

        public ActionResult ListarRequerimientosEmpresa(int Id)
        {
            if (((List<NivelModel>)Session["niveles"]).Where(x => x.Nombre == Niveles.Administrador.ToString() || x.Nombre == Niveles.Empleado.ToString()).FirstOrDefault() != null)
            {
                var model = GetServicioEmpresas().ListarRequerimientosEmpresa(Id);
                var empresa = GetServicioEmpresas().ObtenerEmpresa(Id);
                ViewBag.Empresa = empresa.Nombre;
                ViewBag.EmpresaId = empresa.Id;
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }
    }
}