﻿namespace PORCHIETTO_GestionSeguros.Infrastructure.IoC
{
    using Castle.Windsor;
    using System;
    using System.Globalization;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;


    public class WindsorControllerFactory : DefaultControllerFactory
    {
        private readonly IWindsorContainer container;

        public WindsorControllerFactory(IWindsorContainer container)
        {
            if (container == null)
                throw new ArgumentNullException("container");
            this.container = container;
        }

        public override void ReleaseController(IController controller)
        {
            base.ReleaseController(controller);
            this.container.Release((object)controller);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == (Type)null)
            {
                throw new HttpException(404, string.Format((IFormatProvider)CultureInfo.CurrentUICulture, "No se encuentra el controller para {0}.", new object[1]
        {
          (object) requestContext.HttpContext.Request.Path
        }));
            }
            else
            {
                if (typeof(IController).IsAssignableFrom(controllerType))
                    return this.container.Resolve(controllerType) as IController;
                throw new ArgumentException(string.Format((IFormatProvider)CultureInfo.CurrentUICulture, "El tipo {0} no implementa IController.", new object[1]
        {
          (object) controllerType
        }), "controllerType");
            }
        }
    }
}