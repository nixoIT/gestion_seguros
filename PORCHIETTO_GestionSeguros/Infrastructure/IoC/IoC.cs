﻿namespace PORCHIETTO_GestionSeguros.Infrastructure.IoC
{
    using Castle.Facilities.TypedFactory;
    using Castle.Windsor;
    using Castle.Windsor.Installer;

    /// <summary>
    /// Maneja la configuración del container de inversión de control.
    /// Esta clase sólo debería ser accedida desde puntos claves de la infraestructura,
    /// idealmente sólo una vez al inicio de la aplicación.
    /// </summary>
    public static class IoC
    {
        private static volatile IWindsorContainer instance;

        private static object syncRoot = new object();

        /// <summary>
        /// Permite acceso a la instancia del container creada para la aplicación.
        /// Se crea e inicializa en el primer acceso.
        /// </summary>
        public static IWindsorContainer Instance
        {
            get
            {
                InitializeInstance();
                return instance;
            }
        }

        /// <summary>
        /// Inicializa la instancia del container.
        /// </summary>
        public static void InitializeInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                    {
                        instance = SetupContainer();
                    }
                }
            }
        }

        /// <summary>
        /// Configuración del contenedor de IOC.
        /// </summary>
        private static IWindsorContainer SetupContainer()
        {
            var container = new WindsorContainer();

            // instalar las facilities necesarias
            container.AddFacility<TypedFactoryFacility>();

            // buscar e instalar todos los IWindsorInstallers de este assembly
            container.Install(FromAssembly.This());

            return container;
        }
    }
}