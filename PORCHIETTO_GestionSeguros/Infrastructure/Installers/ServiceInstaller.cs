﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PORCHIETTO_GestionSeguros.Servicios;

namespace PORCHIETTO_GestionSeguros.Infrastructure
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For(typeof(ILoginService)).ImplementedBy(typeof(LoginService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IClientesService)).ImplementedBy(typeof(ClientesService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IUsuariosService)).ImplementedBy(typeof(UsuariosService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IMediosDeCobranzaService)).ImplementedBy(typeof(MediosDeCobranzaService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IPolizasService)).ImplementedBy(typeof(PolizasService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IEmpresasService)).ImplementedBy(typeof(EmpresasService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(ILegajosService)).ImplementedBy(typeof(LegajosService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IProductoresService)).ImplementedBy(typeof(ProductoresService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(IRamasService)).ImplementedBy(typeof(RamasService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(INotificacionesService)).ImplementedBy(typeof(NotificacionesService)).LifestylePerWebRequest());
            container.Register(Component.For(typeof(ISiniestrosService)).ImplementedBy(typeof(SiniestrosService)).LifestylePerWebRequest());
        }
    }
}