﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PORCHIETTO_GestionSeguros.Controllers;
using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros.Infrastructure
{
    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes
                    .FromAssemblyContaining<BaseController>()
                    .BasedOn(typeof(IController))
                    .Configure(
                        component =>
                            component.LifestyleTransient()
                        )
                    );
        }
    }
}