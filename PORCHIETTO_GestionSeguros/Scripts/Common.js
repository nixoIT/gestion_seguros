﻿$(function () {
    $('#TablaExtendida').bootstrapTable({
        classes: 'table-no-bordered',
        idField: 'Id',
        search: true,
        pagination: true,
        pageSize: 6,
        pageList: [6, 12, 25],
        searchAlign: 'left',
        clickToSelect: true,
        showToggle: false,       //Muestra botón de cambio de vista
        filterControl: true,     //Muestra filtros
        filterShowClear: false  //Muestra botón reseteo de filtros
    });
});