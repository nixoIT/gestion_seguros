﻿using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using static PORCHIETTO_GestionSeguros.Models.Notificaciones.NotificacionesModel;

namespace PORCHIETTO_GestionSeguros.Servicios
{
	public class NotificacionesService : INotificacionesService
	{
		#region Notificaciones
		public ObtenerNotificacionesResponse ObtenerNotificaciones(ObtenerNotificacionesRequest request)
		{
			var lista = new List<ObtenerNotificacionesModel> { };
			using (var datos = new npsEntities())
			{
				if (request.Tipo == TipoNotificaciones.Vencimientos_Pólizas || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.Polizas : datos.Polizas.Where(x => x.IdProductor == request.IdProductor);
					var tmpPolizas = db.Where(x => x.Borrado == null && x.Vencimiento <= request.Fecha && x.Notificar).Select(x => x.Id).ToList().Count();

					if (tmpPolizas > 0)
					{
						var notificaciones = new ObtenerNotificacionesModel
						{
							Cantidad = tmpPolizas,
							Titulo = "PÓLIZAS POR VENCER",
							Mensaje = $"Hay {tmpPolizas} pólizas por vencer a la fecha {request.Fecha.Date.ToString(@"dd\/MM\/yyyy")}.",
							URL = $"../Notificaciones/Informe/{(int)TipoNotificaciones.Vencimientos_Pólizas}"

						};

						lista.Add(notificaciones);
					}
				}

				if (request.Tipo == TipoNotificaciones.Vencimientos_Siniestros || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.Siniestros : datos.Siniestros.Where(x => x.IdProductor == request.IdProductor);
					var tmpSiniestros = db.Where(x => x.Borrado == null && x.FechaFin <= request.Fecha && x.Notificar).Select(x => x.id).ToList().Count();

					if (tmpSiniestros > 0)
					{
						var notificaciones = new ObtenerNotificacionesModel
						{
							Cantidad = tmpSiniestros,
							Titulo = "SINIESTROS POR VENCER",
							Mensaje = $"Hay {tmpSiniestros} siniestros por vencer a la fecha {request.Fecha.Date.ToString(@"dd\/MM\/yyyy")}.",
							URL = $"../Notificaciones/Informe/{(int)TipoNotificaciones.Vencimientos_Siniestros}"
						};

						lista.Add(notificaciones);
					}
				}

				if (request.Tipo == TipoNotificaciones.Notificaciones_Siniestros || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.NotificacionesSiniestros : datos.NotificacionesSiniestros.Where(x => x.IdUsuario == request.IdUsuario);
					var tmpSiniestrosPersonalizados = db.Where(x => x.Borrado == null && x.FechaInicio <= DateTime.Today && (x.FechaFin == null || x.FechaFin >= DateTime.Today)).Select(x => x.id).ToList().Count();

					if (tmpSiniestrosPersonalizados > 0)
					{
						var notificaciones = new ObtenerNotificacionesModel
						{
							Cantidad = tmpSiniestrosPersonalizados,
							Titulo = "NOTIFICACIONES DE SINIESTROS",
							Mensaje = $"Hay {tmpSiniestrosPersonalizados} notificaciones sobre siniestros a la fecha {request.Fecha.Date.ToString(@"dd\/MM\/yyyy")}.",
							URL = $"../Notificaciones/Informe/{(int)TipoNotificaciones.Notificaciones_Siniestros}"
						};

						lista.Add(notificaciones);
					}
				}

				if (request.Tipo == TipoNotificaciones.Notificaciones_Pólizas || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.NotificacionesPolizas : datos.NotificacionesPolizas.Where(x => x.IdUsuario == request.IdUsuario);
					var tmpPolizasPersonalizadas = db.Where(x => x.Borrado == null && x.FechaInicio <= DateTime.Today && (x.FechaFin == null || x.FechaFin >= DateTime.Today)).Select(x => x.Id).ToList().Count();

					if (tmpPolizasPersonalizadas > 0)
					{
						var notificaciones = new ObtenerNotificacionesModel
						{
							Cantidad = tmpPolizasPersonalizadas,
							Titulo = "NOTIFICACIONES DE PÓLIZAS",
							Mensaje = $"Hay {tmpPolizasPersonalizadas} notificaciones sobre pólizas a la fecha {request.Fecha.Date.ToString(@"dd\/MM\/yyyy")}.",
							URL = $"../Notificaciones/Informe/{(int)TipoNotificaciones.Notificaciones_Pólizas}"
						};

						lista.Add(notificaciones);
					}
				}
			}

			return new ObtenerNotificacionesResponse { Lista = lista };
		}

		public InformeViewModel ObtenerInformeNotificaciones(ObtenerInformeRequest request)
		{
			var lista = new List<Informe> { };
			using (var datos = new npsEntities())
			{
				if (request.Tipo == TipoNotificaciones.Vencimientos_Pólizas || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.Polizas : datos.Polizas.Where(x => x.IdProductor == request.IdProductor);

					var informe = db.Where(x => x.Borrado == null && x.Vencimiento <= request.Fecha && x.Notificar).Select(x => new Informe
					{
						Id = x.Id,
						Cliente = string.IsNullOrEmpty(x.Clientes.Nombre) ? x.Clientes.Apellido : x.Clientes.Apellido + ", " + x.Clientes.Nombre,
						Numero = x.Numero,
						IdTipo = (int)TipoNotificaciones.Vencimientos_Pólizas,
						Tipo = TipoNotificaciones.Vencimientos_Pólizas,
						Vencimiento = x.Vencimiento,
						IdProductor = x.IdProductor

					}).OrderBy(x => x.Vencimiento).ToList();

					lista.AddRange(informe);
				}

				if (request.Tipo == TipoNotificaciones.Vencimientos_Siniestros || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.Siniestros : datos.Siniestros.Where(x => x.IdProductor == request.IdProductor);

					var informe = db.Where(x => x.Borrado == null && x.FechaFin <= request.Fecha && x.Notificar).Select(x => new Informe
					{
						Id = x.id,
						Cliente = string.IsNullOrEmpty(x.Clientes.Nombre) ? x.Clientes.Apellido : x.Clientes.Apellido + ", " + x.Clientes.Nombre,
						Numero = x.id.ToString(),
						IdTipo = (int)TipoNotificaciones.Vencimientos_Siniestros,
						Tipo = TipoNotificaciones.Vencimientos_Siniestros,
						Vencimiento = x.FechaFin.Value,
						IdProductor = x.IdProductor ?? 0

					}).OrderBy(x => x.Vencimiento).ToList();

					lista.AddRange(informe);
				}

				if (request.Tipo == TipoNotificaciones.Notificaciones_Siniestros || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.NotificacionesSiniestros : datos.NotificacionesSiniestros.Where(x => x.IdUsuario == request.IdUsuario);

					var informe = db.Where(x => x.Borrado == null && x.FechaInicio <= DateTime.Today && (x.FechaFin == null || x.FechaFin >= DateTime.Today)).Select(x => new Informe
					{
						Id = x.id,
						Cliente = string.IsNullOrEmpty(x.Siniestros.Clientes.Nombre) ? x.Siniestros.Clientes.Apellido : x.Siniestros.Clientes.Apellido + ", " + x.Siniestros.Clientes.Nombre,
						Numero = x.IdSiniestro.ToString(),
						IdTipo = (int)TipoNotificaciones.Notificaciones_Siniestros,
						Tipo = TipoNotificaciones.Notificaciones_Siniestros,
						Mensaje = x.Mensaje,
						Vencimiento = x.FechaFin.Value,
						IdUsuario = x.IdUsuario ?? 0

					}).OrderBy(x => x.Vencimiento).ToList();

					lista.AddRange(informe);
				}

				if (request.Tipo == TipoNotificaciones.Notificaciones_Pólizas || request.Tipo == TipoNotificaciones.Todas)
				{
					var db = request.IdProductor == 0 ? datos.NotificacionesPolizas : datos.NotificacionesPolizas.Where(x => x.IdUsuario == request.IdUsuario);

					var informe = db.Where(x => x.Borrado == null && x.FechaInicio <= DateTime.Today && (x.FechaFin == null || x.FechaFin >= DateTime.Today)).Select(x => new Informe
					{
						Id = x.Id,
						Cliente = string.IsNullOrEmpty(x.Polizas.Clientes.Nombre) ? x.Polizas.Clientes.Apellido : x.Polizas.Clientes.Apellido + ", " + x.Polizas.Clientes.Nombre,
						Numero = x.IdPoliza.ToString(),
						IdTipo = (int)TipoNotificaciones.Notificaciones_Pólizas,
						Tipo = TipoNotificaciones.Notificaciones_Pólizas,
						Mensaje = x.Mensaje,
						Vencimiento = x.FechaFin.Value,
						IdUsuario = x.IdUsuario ?? 0

					}).OrderBy(x => x.Vencimiento).ToList();

					lista.AddRange(informe);
				}
			}

			return new InformeViewModel { Lista = lista };
		}

		public MarcarResueltosResponse MarcarResueltos(MarcarResueltosRequest request)
		{
			using (var datos = new npsEntities())
			{
				foreach (var notificacion in request.Lista)
				{
					if (notificacion.IdTipoNotificacion == (int)TipoNotificaciones.Vencimientos_Pólizas)
					{
						var informe = datos.Polizas.Where(x => x.Borrado == null && x.Id == notificacion.IdNotificacion).FirstOrDefault();

						if (informe != null)
						{
							informe.Notificar = false;
						}
					}
					else if (notificacion.IdTipoNotificacion == (int)TipoNotificaciones.Vencimientos_Siniestros)
					{
						var informe = datos.Siniestros.Where(x => x.Borrado == null && x.id == notificacion.IdNotificacion).FirstOrDefault();

						if (informe != null)
						{
							informe.Notificar = false;
						}
					}
					else if (notificacion.IdTipoNotificacion == (int)TipoNotificaciones.Notificaciones_Siniestros)
					{
						var informe = datos.NotificacionesSiniestros.Where(x => x.Borrado == null && x.id == notificacion.IdNotificacion).FirstOrDefault();

						if (informe != null)
						{
							informe.Borrado = DateTime.Now;
						}
					}

					datos.SaveChanges();
				}
			}

			return new MarcarResueltosResponse();
		}
		#endregion
	}
}