﻿using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Legajos;
using System;
using System.Linq;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public class LegajosService : ILegajosService
    {
        public ModificarLegajoModel ObtenerLegajo(int Id)
        {
            using (var datos = new npsEntities())
            {
                return datos.Legajos.Where(x => x.Id == Id && x.Borrado == null).Select(x => new ModificarLegajoModel
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Ubicacion = x.Ubicacion,
                    Path = x.Path
                }).FirstOrDefault();
            }
        }

        public LegajosViewModel ObtenerLegajos()
        {
            using (var datos = new Data.npsEntities())
            {
                var _legajos = datos.Legajos.Where(x => x.Borrado == null).Select(x => new AgregarLegajoModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Ubicacion = x.Ubicacion,
                    Path = x.Path
                }).ToList();

                return new LegajosViewModel() { Lista = _legajos };
            }
        }

        public AgregarLegajoResponse AgregarLegajo(AgregarLegajoRequest request)
        {
            using (var datos = new npsEntities())
            {
                var _Legajo = new Legajos
                {
                    Nombre = request.Nombre,
                    Ubicacion = request.Ubicacion,
                    Path = request.Path
                };

                datos.Legajos.Add(_Legajo);
                datos.SaveChanges();
            }

            return new AgregarLegajoResponse();
        }

        public ModificarLegajoResponse ModificarLegajo(ModificarLegajoRequest request)
        {
            using (var datos = new npsEntities())
            {
                var _Legajo = datos.Legajos.Where(x => x.Id == request.Id && x.Borrado == null).FirstOrDefault();

                if(_Legajo != null)
                {
                    _Legajo.Nombre = request.Nombre;
                    _Legajo.Ubicacion = request.Ubicacion;
                    _Legajo.Path = request.Path;

                    datos.SaveChanges();
                }
            }

            return new ModificarLegajoResponse();
        }

        public void EliminarLegajo(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var _Legajo = datos.Legajos.Where(x => x.Id == Id).First();

                _Legajo.Borrado = DateTime.Today;

                datos.SaveChanges();
            }
        }
    }
}