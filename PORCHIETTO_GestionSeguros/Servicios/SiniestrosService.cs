﻿using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using PORCHIETTO_GestionSeguros.Models.Siniestros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PORCHIETTO_GestionSeguros.Servicios
{
	public class SiniestrosService : ISiniestrosService
	{
		#region Siniestros
		public SiniestroModel ObtenerSiniestros(SiniestroListadoRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var db = request.IdProductor == 0 ? datos.Siniestros : datos.Siniestros.Where(x => x.IdProductor == request.IdProductor);
				var siniestros = db.Where(x => x.Terminado != true && x.Borrado == null).Select(x => new AgregarSiniestroModel()
				{
					Id = x.id,
					IdCliente = x.IdCliente,
					NombreCliente = (string.IsNullOrEmpty(x.Clientes.Nombre.Trim()) ? x.Clientes.Apellido : x.Clientes.Apellido + ", " + x.Clientes.Nombre),
					FechaSiniestro = x.FechaSiniestro,
					IdRama = x.IdRama,
					Rama = x.Ramas.Nombre,
					IdEmpresa = x.IdEmpresa,
					ApellidoNombreTercero = x.ApellidoNombreTercero,
					TelefonoTercero = x.TelefonoTercero,
					IdCompañiaTercero = x.IdCompañiaTercero,
					IdRamaTercero = x.IdRamaTercero,
					FechaPresupuesto = x.FechaPresupuesto,
					Observaciones = x.Observaciones,
					Carpeta = x.Carpeta,
					PresentaReclamoTercero = x.PresentaReclamoTercero,
					Terminado = x.Terminado,
					FechaFin = x.FechaFin
				});

				return new SiniestroModel() { Lista = siniestros.ToList(), IdProductor = request.IdProductor };
			}
		}

		public AgregarSiniestroModel ObtenerSiniestros(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				return datos.Siniestros.Where(x => x.Terminado != true && x.id == Id).Select(x => new AgregarSiniestroModel()
				{
					Id = x.id,
					IdCliente = x.IdCliente,
					FechaSiniestro = x.FechaSiniestro,
					IdRama = x.IdRama,
					IdEmpresa = x.IdEmpresa,
					ApellidoNombreTercero = x.ApellidoNombreTercero,
					TelefonoTercero = x.TelefonoTercero,
					IdCompañiaTercero = x.IdCompañiaTercero,
					IdRamaTercero = x.IdRamaTercero,
					FechaPresupuesto = x.FechaPresupuesto,
					Observaciones = x.Observaciones,
					Carpeta = x.Carpeta,
					PresentaReclamoTercero = x.PresentaReclamoTercero,
					Terminado = x.Terminado,
					FechaFin = x.FechaFin
				}).FirstOrDefault();
			}
		}

		public SiniestroModel ObtenerHistoricoSiniestros(SiniestroListadoRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var db = request.IdProductor == 0 ? datos.Siniestros : datos.Siniestros.Where(x => x.IdProductor == request.IdProductor);
				var siniestros = db.Where(x => x.Terminado == true || x.Borrado != null).Select(x => new AgregarSiniestroModel()
				{
					Id = x.id,
					IdCliente = x.IdCliente,
					NombreCliente = (string.IsNullOrEmpty(x.Clientes.Nombre.Trim()) ? x.Clientes.Apellido : x.Clientes.Apellido + ", " + x.Clientes.Nombre),
					FechaSiniestro = x.FechaSiniestro,
					IdRama = x.IdRama,
					Rama = x.Ramas.Nombre,
					IdEmpresa = x.IdEmpresa,
					ApellidoNombreTercero = x.ApellidoNombreTercero,
					TelefonoTercero = x.TelefonoTercero,
					IdCompañiaTercero = x.IdCompañiaTercero,
					IdRamaTercero = x.IdRamaTercero,
					FechaPresupuesto = x.FechaPresupuesto,
					Observaciones = x.Observaciones,
					Carpeta = x.Carpeta,
					PresentaReclamoTercero = x.PresentaReclamoTercero,
					Terminado = x.Terminado,
					FechaFin = x.FechaFin,
					Borrado = x.Borrado
				});

				return new SiniestroModel() { Lista = siniestros.ToList() };
			}
		}

		public AgregarSiniestroResponse AgregarSiniestro(AgregarSiniestroRequest request)
		{
			using (var datos = new npsEntities())
			{
				var _siniestro = datos.Siniestros.Where(x => x.id == request.Id).FirstOrDefault();

				if (_siniestro != null)
				{
					return new AgregarSiniestroResponse { Id = 0 };
				}
				else
				{
					Siniestros siniestro = new Siniestros
					{
						IdCliente = request.IdCliente,
						FechaSiniestro = request.FechaSiniestro,
						IdRama = request.IdRama,
						IdEmpresa = request.IdEmpresa,
						ApellidoNombreTercero = request.ApellidoNombreTercero,
						TelefonoTercero = request.TelefonoTercero,
						IdCompañiaTercero = request.IdCompañiaTercero,
						IdRamaTercero = request.IdRamaTercero,
						FechaPresupuesto = request.FechaPresupuesto,
						Observaciones = request.Observaciones,
						Carpeta = request.Carpeta,
						PresentaReclamoTercero = request.PresentaReclamoTercero,
						Terminado = request.Terminado,
						FechaFin = request.FechaFin
					};

					datos.Siniestros.Add(siniestro);
					datos.SaveChanges();

					return new AgregarSiniestroResponse { Id = siniestro.id };
				}
			}
		}

		public ModificarSiniestroResponse ModificarSiniestro(ModificarSiniestroRequest request)
		{
			using (var datos = new npsEntities())
			{
				var _siniestro = datos.Siniestros.Where(x => x.id == request.Id).FirstOrDefault();

				if (_siniestro != null)
				{
					_siniestro.IdCliente = request.IdCliente;
					_siniestro.FechaSiniestro = request.FechaSiniestro;
					_siniestro.IdRama = request.IdRama;
					_siniestro.IdEmpresa = request.IdEmpresa;
					_siniestro.ApellidoNombreTercero = request.ApellidoNombreTercero;
					_siniestro.TelefonoTercero = request.TelefonoTercero;
					_siniestro.IdCompañiaTercero = request.IdCompañiaTercero;
					_siniestro.IdRamaTercero = request.IdRamaTercero;
					_siniestro.FechaPresupuesto = request.FechaPresupuesto;
					_siniestro.Observaciones = request.Observaciones;
					_siniestro.Carpeta = request.Carpeta;
					_siniestro.PresentaReclamoTercero = request.PresentaReclamoTercero;
					_siniestro.Terminado = request.Terminado;
					_siniestro.FechaFin = request.FechaFin;
					datos.SaveChanges();
					return new ModificarSiniestroResponse { Id = _siniestro.id };
				}
				else
				{
					return new ModificarSiniestroResponse { Id = 0 };
				}
			}
		}

		public void EliminarSiniestro(int Id)
		{
			using (var datos = new npsEntities())
			{
				var siniestro = datos.Siniestros.Where(x => x.id == Id).First();

				siniestro.Borrado = DateTime.Today;

				datos.SaveChanges();
			}
		}

		public void FinalizarSiniestro(int Id)
		{
			using (var datos = new npsEntities())
			{
				var siniestro = datos.Siniestros.Where(x => x.id == Id).First();

				siniestro.FechaFin = DateTime.Today;
				siniestro.Terminado = true;

				datos.SaveChanges();
			}
		}
		#endregion

		#region Siniestros Cliente
		public SiniestroModel ObtenerMisSiniestros(int IdCliente)
		{
			using (var datos = new Data.npsEntities())
			{
				var siniestros = datos.Siniestros.Where(x => x.Terminado != true && x.Borrado == null && x.IdCliente == IdCliente).Select(x => new AgregarSiniestroModel()
				{
					Id = x.id,
					IdCliente = x.IdCliente,
					NombreCliente = (string.IsNullOrEmpty(x.Clientes.Nombre.Trim()) ? x.Clientes.Apellido : x.Clientes.Apellido + ", " + x.Clientes.Nombre),
					FechaSiniestro = x.FechaSiniestro,
					IdRama = x.IdRama,
					Rama = x.Ramas.Nombre,
					IdEmpresa = x.IdEmpresa,
					ApellidoNombreTercero = x.ApellidoNombreTercero,
					TelefonoTercero = x.TelefonoTercero,
					IdCompañiaTercero = x.IdCompañiaTercero,
					IdRamaTercero = x.IdRamaTercero,
					FechaPresupuesto = x.FechaPresupuesto,
					Observaciones = x.Observaciones,
					Carpeta = x.Carpeta,
					PresentaReclamoTercero = x.PresentaReclamoTercero,
					Terminado = x.Terminado,
					FechaFin = x.FechaFin
				});

				return new SiniestroModel() { Lista = siniestros.ToList() };
			}
		}
		#endregion

		#region Requerimientos Siniestros
		public RequerimientosSiniestroModel ObtenerRequerimientosSiniestro(int Id)
		{
			using (var datos = new npsEntities())
			{
				var Respuesta = new RequerimientosSiniestroModel();

				var siniestro = datos.Siniestros.Where(x => x.Terminado != true && x.id == Id).FirstOrDefault();

				if (siniestro != null)
				{
					Respuesta.IdSiniestro = siniestro.id;
					Respuesta.DescripRama = siniestro.Ramas.Nombre;
					Respuesta.DescripEmpresa = siniestro.Empresas.Nombre;
					Respuesta.DescripEmpresaTercero = siniestro.EmpresaTercero != null ? siniestro.EmpresaTercero.Nombre : "";

					var ListaReqEmpresa = datos.RequerimientosEmpresas.Where(x => x.Borrado == null && x.Id_Empresa == siniestro.IdEmpresa && x.Id_Rama == siniestro.IdRama).Select(y => new RequerimientoSiniestroEmpresa()
					{
						Id = y.Id,
						IdSiniestro = siniestro.id,
						IdRequerimientoEmpresa = y.Requerimientos.Id,
						DescripRequerimiento = y.Requerimientos.Requerimiento,
						Principal = true
					}).ToList();

					foreach (var Req in ListaReqEmpresa)
					{
						var _req = datos.SiniestrosRequerimientosEmpresas.Where(x => x.IdSiniestro == Respuesta.IdSiniestro && x.IdRequerimientoEmpresa == Req.IdRequerimientoEmpresa && x.Principal == Req.Principal).FirstOrDefault();
						if (_req != null)
						{
							Req.Valor = _req.Valor;
						}
					}

					Respuesta.ListaReqEmpresa = ListaReqEmpresa;

					if (siniestro.IdCompañiaTercero != null)
					{
						var ListaReqEmpresaTercero = datos.RequerimientosEmpresas.Where(x => x.Borrado == null && x.Id_Empresa == siniestro.IdCompañiaTercero && x.Id_Rama == siniestro.IdRamaTercero).Select(y => new RequerimientoSiniestroEmpresa()
						{
							Id = y.Id,
							IdSiniestro = siniestro.id,
							IdRequerimientoEmpresa = y.Requerimientos.Id,
							DescripRequerimiento = y.Requerimientos.Requerimiento,
							Principal = false
						}).ToList();

						foreach (var Req in ListaReqEmpresaTercero)
						{
							var _req = datos.SiniestrosRequerimientosEmpresas.Where(x => x.IdSiniestro == Respuesta.IdSiniestro && x.IdRequerimientoEmpresa == Req.IdRequerimientoEmpresa && x.Principal == Req.Principal).FirstOrDefault();
							if (_req != null)
							{
								Req.Valor = _req.Valor;
							}
						}

						Respuesta.ListaReqEmpresaTercero = ListaReqEmpresaTercero;
					}
					else
					{
						Respuesta.ListaReqEmpresaTercero = new List<RequerimientoSiniestroEmpresa>();
					}
				}

				return Respuesta;
			}
		}

		public void AgregarRequerimientosSiniestro(List<RequerimientoSiniestroEmpresa> request)
		{
			using (var datos = new npsEntities())
			{
				foreach (var item in request)
				{
					var _ReqSiniestro = datos.SiniestrosRequerimientosEmpresas.Where(x => x.IdSiniestro == item.IdSiniestro && x.IdRequerimientoEmpresa == item.IdRequerimientoEmpresa && x.Principal == item.Principal).FirstOrDefault();
					if (_ReqSiniestro != null)
					{
						_ReqSiniestro.Valor = item.Valor;
					}
					else
					{
						SiniestrosRequerimientosEmpresas ReqSiniestro = new SiniestrosRequerimientosEmpresas
						{
							IdSiniestro = item.IdSiniestro,
							IdRequerimientoEmpresa = item.IdRequerimientoEmpresa,
							Valor = item.Valor,
							Principal = item.Principal
						};
						datos.SiniestrosRequerimientosEmpresas.Add(ReqSiniestro);
					}
					datos.SaveChanges();
				}
			}
		}
		#endregion

		#region Notificaciones Siniestros

		public void AgregarNotificaciones(NotificacionesSiniestrosModel request)
		{
			using (var datos = new npsEntities())
			{
				Data.NotificacionesSiniestros notificacion = new Data.NotificacionesSiniestros
				{
					IdSiniestro = request.IdSiniestro,
					FechaInicio = request.FechaInicio,
					FechaFin = request.FechaFinal,
					Mensaje = request.Mensaje,
					VisibleCliente = request.VisibleCliente,
					IdUsuario = request.IdUsuario,
				};

				datos.NotificacionesSiniestros.Add(notificacion);
				datos.SaveChanges();
			}
		}

		public InformeNotificacionesSiniestros ObtenerInformeNotificacionesSiniestros(int IdSiniestro)
		{
			using (var datos = new npsEntities())
			{
				return new InformeNotificacionesSiniestros
				{
					IdSiniestro = IdSiniestro,
					Cliente = datos.Siniestros.Where(x => x.id == IdSiniestro).Select(y => y.Clientes.Nombre + " " + y.Clientes.Apellido).FirstOrDefault(),
					Lista = datos.NotificacionesSiniestros.Where(x => x.IdSiniestro == IdSiniestro && x.Borrado == null).Select(y => new Models.Notificaciones.NotificacionesSiniestros
					{
						Id = y.id,
						IdSiniestro = y.IdSiniestro,
						Mensaje = y.Mensaje,
						FechaInicio = y.FechaInicio,
						FechaFin = y.FechaFin,
						VisibleCliente = y.VisibleCliente,
						IdUsuario = y.IdUsuario ?? 0,
						Usuario = y.Usuarios.Usuario
					}).ToList()
				};
			}
		}

		public InformeNotificacionesSiniestros ObtenerNotificacionesSiniestrosCliente(int IdSiniestro)
		{
			using (var datos = new npsEntities())
			{
				return new InformeNotificacionesSiniestros
				{
					IdSiniestro = IdSiniestro,
					Cliente = datos.Siniestros.Where(x => x.id == IdSiniestro).Select(y => y.Clientes.Nombre + " " + y.Clientes.Apellido).FirstOrDefault(),
					Lista = datos.NotificacionesSiniestros.Where(x => x.IdSiniestro == IdSiniestro && x.Borrado == null && x.VisibleCliente == true).Select(y => new Models.Notificaciones.NotificacionesSiniestros
					{
						Id = y.id,
						IdSiniestro = y.IdSiniestro,
						Mensaje = y.Mensaje,
						FechaInicio = y.FechaInicio,
						FechaFin = y.FechaFin,
						VisibleCliente = y.VisibleCliente,
						IdUsuario = y.IdUsuario ?? 0,
						Usuario = y.Usuarios.Usuario
					}).ToList()
				};
			}
		}

		public void EliminarNotificacionSiniestro(int Id)
		{
			using (var datos = new npsEntities())
			{
				var siniestro = datos.NotificacionesSiniestros.Where(x => x.id == Id).First();

				siniestro.Borrado = DateTime.Today;

				datos.SaveChanges();
			}
		}

		#endregion
	}
}