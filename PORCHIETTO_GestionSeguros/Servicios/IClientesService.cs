﻿using PORCHIETTO_GestionSeguros.Models.Clientes;
using PORCHIETTO_GestionSeguros.Models.MediosDeCobranza;
using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface IClientesService : IServicioBase
    {
        ClientesModel ObtenerClientes();

        ClientesListadoModel ObtenerClientesListado(ClientesListadoRequest request);

        AgregarClienteModel ObtenerCliente(int Id);

        AgregarClienteModel ObtenerClienteAEditar(int Id);

        AgregarClienteResponse AgregarCliente(AgregarClienteRequest request);

        void EliminarCliente(int Id);

        ModificarClienteResponse ModificarCliente(ModificarClienteRequest request);

        MediosDeCobranzaModel ObtenerMediosDePago();

        ObtenerLocalidadesResponse ObtenerLocalidades(ObtenerLocalidadesRequest request);

        ObtenerProvinciasResponse ObtenerProvincias(ObtenerProvinciasRequest request);

        ObtenerDomicilioReferenteResponse ObtenerDomicilioReferente(int Id);

        ObtenerGruposdeCobranzaResponse ObtenerGruposCobranza();

        List<string> ListadoReferidos(int IdCliente);

		AgregarUsuarioClienteResponse AgregarUsuarioCliente(AgregarUsuarioClienteRequest request);

		AgregarUsuarioClienteModel ObtenerUsuarioCliente(int Id);

		bool BorrarUsuarioCliente(int IdUsuario, int IdCliente);
		bool BlanquearPassUsuarioCliente(int IdUsuario);
		bool ExisteUsuario(string name);
	}
}