﻿using System;
using System.Linq;
using PORCHIETTO_GestionSeguros.Models.MediosDeCobranza;
using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Clientes;
using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public class MediosDeCobranzaService : IMediosDeCobranzaService
    {
        public MediosDeCobranzaModel ObtenerMediosDeCobranza()
        {
            using (var datos = new Data.npsEntities())
            {
                var MediosDeCobranza = datos.MediosdeCobranza.Where(x => x.Borrado == null).Select(x => new AgregarMediosDeCobranzaModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                });

                return new MediosDeCobranzaModel() { Lista = MediosDeCobranza.ToList() };
            }
        }

        public AgregarMediosDeCobranzaModel ObtenerMediosDeCobranza(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                return datos.MediosdeCobranza.Where(x => x.Id == Id && x.Borrado == null).Select(x => new AgregarMediosDeCobranzaModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                }).FirstOrDefault();
            }
        }

        public ModificarMediosDeCobranzaModel ObtenerMediosDeCobranzaAEditar(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                return datos.MediosdeCobranza.Where(x => x.Id == Id && x.Borrado == null).Select(x => new ModificarMediosDeCobranzaModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                }).FirstOrDefault();
            }
        }

        public AgregarMediosDeCobranzaResponse AgregarMediosDeCobranza(AgregarMediosDeCobranzaRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var _medio = datos.MediosdeCobranza.Where(x => x.Id == request.Id).FirstOrDefault();

                if (_medio != null)
                {
                    // Retorno 0 porque la persona ya existe como usuario
                    return new AgregarMediosDeCobranzaResponse { Id = 0 };
                }
                else
                {
                    MediosdeCobranza medio = new MediosdeCobranza
                    {
                        Nombre = request.Nombre,
                        Borrado = null
                    };

                    datos.MediosdeCobranza.Add(medio);
                    datos.SaveChanges();

                    return new AgregarMediosDeCobranzaResponse { Id = medio.Id };
                }
            }
        }

        public void EliminarMediosDeCobranza(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var usuario = datos.MediosdeCobranza.Where(x => x.Id == Id).First();

                usuario.Borrado = DateTime.Today;

                datos.SaveChanges();
            }
        }

        public ModificarMediosDeCobranzaResponse ModificarMediosDeCobranza(ModificarMediosDeCobranzaRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var persona = datos.MediosdeCobranza.Where(x => x.Id == request.Id).FirstOrDefault();

                if (persona != null)
                {
                    persona.Nombre = request.Nombre;

                    datos.SaveChanges();
                    return new ModificarMediosDeCobranzaResponse { Id = persona.Id };
                }
                else
                {
                    // Retorna 0 porque no existe, es muy probable que 
                    // no se de el caso pero puede pasar
                    return new ModificarMediosDeCobranzaResponse { Id = 0 };
                }
            }
        }

        public ReporteClientesModel GetReportesPorCliente(GetReportePorClienteRequest request)
        {
            using (var datos = new npsEntities())
            {
                var db = request.IdProductor == 0 ? datos.Clientes : datos.Clientes.Where(x => x.IdProductor == request.IdProductor);

                // Obtengo los clientes no dados de baja
                var clientes = db.Where(x => x.Borrado == null).Select(x => new AgregarClienteModel()
                {
                    Apellido = x.Apellido,
                    Carpeta = x.Carpeta,
                    DomicilioCalle = x.DomicilioCalle,
                    DomicilioNumero = x.DomicilioNumero,
                    CodigoLocalidad = x.CodigoLocalidad,
                    DomicilioCobroCalle = x.DomicilioCobroCalle,
                    DomicilioCobroNumero = x.DomicilioCobroNumero,
                    email = x.Email,
                    Fecha = x.FechaAlta,
                    Id = x.Id,
                    IdMediodeCobranza = x.IdMediosdeCobranza.Value,
                    NombreMediodeCobranza = x.MediosdeCobranza.Nombre,
                    Nombre = x.Nombre,
                    TelefonoFijo = x.TelefonoFijo,
                    TelefonoMovil = x.TelefonoMovil,
                    IdGrupodeCobranza = x.IdGrupodeCobranza,
                    EsCabezadeGrupo = x.EsCabezadeGrupo ?? false
                }).OrderBy(x => x.Apellido).ThenByDescending(x => x.Apellido).ToList();

                // Filtro los clientes por medio de cobranza
                if (request.IdMedioDePago != 0)
                {
                    clientes = clientes.Where(x => x.IdMediodeCobranza == request.IdMedioDePago).ToList();
                }

                var clientes_referidos = clientes.Where(x => x.IdGrupodeCobranza != 0 && x.EsCabezadeGrupo == false).ToList();
                clientes.RemoveAll(x => clientes_referidos.Any(y => y.Id == x.Id));

                var listaReporte = new List<ReporteCliente> { };
                foreach (var item in clientes)
                {
                    var clientereporte = new ReporteCliente()
                    {
                        Id = item.Id,
                        Nombre = (item.Apellido ?? "") + " " + (item.Nombre ?? ""),
                        Direccion = item.DomicilioCobroCalle + " " + item.DomicilioCobroNumero,
                        Telefono = item.TelefonoFijo,
                        Celular = item.TelefonoMovil,
                        ReferentePago = item.EsCabezadeGrupo,
                        NombreMediodeCobranza = item.NombreMediodeCobranza,
                    };

                    //if (clientereporte.Nombre == null)
                    //{
                    //    clientereporte.Nombre = item.Nombre + " " + item.Apellido;
                    //}

                    if (item.CUIT != null)
                    {
                        clientereporte.Nombre = item.NombreCompleto;
                    }

                    if (item.EsCabezadeGrupo == true)
                    {
                        // Create a list.
                        List<string> ListaAsociados = new List<string>();

                        // Busco los clientes referidos al referente de pago y agrego los nombres a la lista
                        var referidos = datos.Clientes.Where(x => x.Borrado == null && x.IdGrupodeCobranza == item.Id && x.Id != item.Id).OrderBy(x => x.Apellido).ThenByDescending(x => x.Apellido).ToList();
                        foreach (var itemref in referidos)
                        {
                            var asociado = (itemref.Apellido ?? "") + " " + (itemref.Nombre ?? "");
                            var asociadoPolizas = ObtenerListadoNumerosDePolizasCliente(itemref.Id);

                            // Si el asociado posee polizas activas lo agrego
                            if (asociadoPolizas.Length > 0)
                            {
                                ListaAsociados.Add(asociado + " ( Polizas: " + asociadoPolizas + " )");
                            }
                        }

                        if (ListaAsociados.Count == 0)
                        {
                            ListaAsociados.Add("No se encontraron asociados");
                        }

                        clientereporte.AsociadosAReferentePago = ListaAsociados;
                    }

                    //Busco nº poliza
                    clientereporte.Poliza = ObtenerListadoNumerosDePolizasCliente(item.Id);

                    //Si el cliente posee polizas activas lo agrego
                    if (clientereporte.Poliza.Length > 0)
                    {
                        listaReporte.Add(clientereporte);
                    }
                }

                return new ReporteClientesModel { Lista = listaReporte, IdMedio = request.IdMedioDePago, IdProductor = request.IdProductor };
            }
        }

        public string ObtenerListadoNumerosDePolizasCliente(int IdCliente)
        {
            using (var datos = new npsEntities())
            {
                var rto = string.Empty;
                var polizas = datos.Polizas.Where(x => x.IdCliente == IdCliente && x.Borrado == null && x.Vencimiento > DateTime.Today).ToList();
                foreach (var _p in polizas)
                {
                    if (rto == string.Empty)
                        rto = _p.Numero.ToString();
                    else
                        rto += " - " + _p.Numero.ToString();
                }
                return rto;
            }

        }
    }
}