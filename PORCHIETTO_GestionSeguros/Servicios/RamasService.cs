﻿using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Ramas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public class RamasService: IRamasService
    {
        public RamaModel ObtenerRama()
        {
            using (var datos = new Data.npsEntities())
            {
                var ramas = datos.Ramas.Where(x => x.Borrado == null).Select(x => new AgregarRamaModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Codigo = x.Codigo,
                    Id_Empresa = x.Id_Empresa
                });

                return new RamaModel() { Lista = ramas.ToList() };
            }
        }

        public AgregarRamaModel ObtenerRama(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                return datos.Ramas.Where(x => x.Id == Id && x.Borrado == null).Select(x => new AgregarRamaModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Codigo = x.Codigo,
                    Id_Empresa = x.Id_Empresa
                }).FirstOrDefault();
            }
        }

        public AgregarRamaResponse AgregarRama(AgregarRamaRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var _medio = datos.Ramas.Where(x => x.Id == request.Id).FirstOrDefault();

                if (_medio != null)
                {
                    // Retorno 0 porque la persona ya existe como usuario
                    return new AgregarRamaResponse { Id = 0 };
                }
                else
                {
                    Ramas medio = new Ramas
                    {
                        Nombre = request.Nombre,
                        Borrado = null,
                        Codigo = request.Codigo,
                        Id_Empresa = request.Id_Empresa
                    };

                    datos.Ramas.Add(medio);
                    datos.SaveChanges();

                    return new AgregarRamaResponse { Id = medio.Id };
                }
            }
        }

        public ModificarRamaResponse ModificarRama(ModificarRamaRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var rama = datos.Ramas.Where(x => x.Id == request.Id).FirstOrDefault();

                if (rama != null)
                {
                    rama.Nombre = request.Nombre;
                    rama.Codigo = request.Codigo;
                    rama.Id_Empresa = request.Id_Empresa;
                    datos.SaveChanges();
                    return new ModificarRamaResponse { Id = rama.Id };
                }
                else
                {
                    // Retorna 0 porque no existe, es muy probable que 
                    // no se de el caso pero puede pasar
                    return new ModificarRamaResponse { Id = 0 };
                }
            }
        }

        public void EliminarRama(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var rama = datos.Ramas.Where(x => x.Id == Id).First();

                rama.Borrado = DateTime.Today;

                datos.SaveChanges();
            }
        }

        public RamaModel ObtenerRamasEmpresa(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var ramas_empresa = datos.Ramas.Where(x => x.Borrado == null && x.Id_Empresa == Id).Select(x => new AgregarRamaModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Codigo = x.Codigo,
                    Id_Empresa = x.Id_Empresa
                });

                return new RamaModel() { Lista = ramas_empresa.ToList() };
            }
        }
    }
}