﻿using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Productores;
using System;
using System.Linq;
using Niveles = NIXO_Framework.Niveles;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public class ProductoresService : IProductoresService
    {
        public ProductoresViewModel ObtenerProductores()
        {
            using (var datos = new Data.npsEntities())
            {
                var _Productores = datos.Productores.Where(x => x.Borrado == null).Select(x => new AgregarProductorModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Apellido = x.Apellido,
                    CUIT = x.Cuit,
                    Domicilio = x.Direccion,
                    Email = x.Email,
                    Telefono = x.Telefono,
                    TelefonoAlternativo = x.TelefonoAlternativo,
                    Web = x.Web,
                    ActividadPrincipal = x.ActividadPrincipal,
                    ActividadSecundaria = x.ActividadSecundaria,
                    IdUsuario = x.IdUsuario
                }).ToList();

                return new ProductoresViewModel() { Lista = _Productores };
            }
        }

        public AgregarProductorResponse AgregarProductor(AgregarProductorRequest request)
        {
            using (var datos = new npsEntities())
            {
                var _Productor = new Productores
                {
                    Nombre = request.Nombre,
                    Cuit = request.CUIT,
                    Direccion = request.Domicilio,
                    Email = request.Email,
                    Telefono = request.Telefono,
                    TelefonoAlternativo = request.TelefonoAlternativo,
                    Web = request.Web,
                    ActividadPrincipal = request.ActividadPrincipal,
                    ActividadSecundaria = request.ActividadSecundaria
                };

                datos.Productores.Add(_Productor);
                datos.SaveChanges();
            }

            return new AgregarProductorResponse();
        }

        public ModificarProductorModel ObtenerProductor(int Id)
        {
            using (var datos = new npsEntities())
            {
                return datos.Productores.Where(x => x.Id == Id && x.Borrado == null).Select(x => new ModificarProductorModel
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    Apellido = x.Apellido,
                    CUIT = x.Cuit,
                    Domicilio = x.Direccion,
                    Email = x.Email,
                    Telefono = x.Telefono,
                    TelefonoAlternativo = x.TelefonoAlternativo,
                    Web = x.Web,
                    IdUsuario = x.IdUsuario,
                    ActividadPrincipal = x.ActividadPrincipal,
                    ActividadSecundaria = x.ActividadSecundaria
                }).FirstOrDefault();
            }
        }

        public ModificarProductorResponse ModificarProductor(ModificarProductorRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var _Productor = datos.Productores.Where(x => x.Id == request.Id).FirstOrDefault();

                if (_Productor != null)
                {
                    _Productor.Nombre = request.Nombre;
                    _Productor.Apellido = request.Apellido;
                    _Productor.Cuit = request.CUIT;
                    _Productor.Direccion = request.Domicilio;
                    _Productor.Email = request.Email;
                    _Productor.Telefono = request.Telefono;
                    _Productor.TelefonoAlternativo = request.TelefonoAlternativo;
                    _Productor.Web = request.Web;
                    _Productor.ActividadPrincipal = request.ActividadPrincipal;
                    _Productor.ActividadSecundaria = request.ActividadSecundaria;

                    datos.SaveChanges();
                }

                return new ModificarProductorResponse { };
            }
        }

        public void EliminarProductor(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var usuario = datos.Productores.Where(x => x.Id == Id).First();

                usuario.Borrado = DateTime.Today;

                datos.SaveChanges();
            }
        }

        public AgregarUsuarioProductorResponse AgregarUsuarioProductor(AgregarUsuarioProductorRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var _usuario = datos.Usuarios.Where(x => x.Id == request.Id).FirstOrDefault();

                if (_usuario != null)
                {
                    // Retorno 0 porque la persona ya existe como usuario
                    return new AgregarUsuarioProductorResponse { Id = 0 };
                }
                else
                {
                    try
                    {
                        var user = new Usuarios
                        {
                            Nombre = request.Nombre,
                            Apellido = request.Apellido,
                            Usuario = request.Usuario,
                            Pass = request.Pass,
                        };

                        datos.Usuarios.Add(user);
                        datos.SaveChanges();

                        var nivel = new NivelesUsuarios
                        {
                            IdUsuario = user.Id,
                            IdNivel = Niveles.Productor.GetHashCode(),
                        };

                        //grabar nivel
                        datos.NivelesUsuarios.Add(nivel);
                        datos.SaveChanges();

                        //grabar id usuario en cliente
                        var productor = datos.Productores.Where(x => x.Id == request.IdProductor).FirstOrDefault();
                        productor.IdUsuario = user.Id;
                        if (string.IsNullOrEmpty(productor.Apellido))
                            productor.Apellido = request.Apellido;

                        if (string.IsNullOrEmpty(productor.Email))
                            productor.Email = request.Email;
                        datos.SaveChanges();

                        return new AgregarUsuarioProductorResponse { Id = user.Id };
                    }
                    catch (Exception e)
                    {
                        return new AgregarUsuarioProductorResponse { Id = 0 };
                    }

                }
            }
        }


        public AgregarUsuarioProductorModel ObtenerUsuarioCliente(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var IdUsuario = datos.Productores.Where(x => x.Id == Id && x.Borrado == null).Select(y => y.IdUsuario).FirstOrDefault();

                if (IdUsuario != null)
                {
                    return datos.Usuarios.Where(x => x.Id == IdUsuario && x.Borrado == null).Select(x => new AgregarUsuarioProductorModel()
                    {
                        Id = x.Id,
                        Apellido = x.Apellido,
                        Nombre = x.Nombre,
                        Usuario = x.Usuario
                    }).FirstOrDefault();
                }
                else
                    return null;
            }
        }

        public bool ExisteUsuario(string name)
        {
            using (var datos = new Data.npsEntities())
            {
                var _usuario = datos.Usuarios.Where(x => x.Usuario == name).FirstOrDefault();

                return _usuario != null ? true : false;

            }
        }

        public bool BorrarUsuarioProductor(int IdUsuario, int IdProductor)
        {
            using (var datos = new Data.npsEntities())
            {
                var _usuario = datos.Usuarios.Where(x => x.Id == IdUsuario).FirstOrDefault();

                if (_usuario != null)
                {
                    _usuario.Borrado = DateTime.Today;

                    var _productor = datos.Productores.Where(x => x.Id == IdProductor).FirstOrDefault();
                    _productor.IdUsuario = null;

                    datos.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool BlanquearPassUsuarioProductor(int IdUsuario)
        {
            using (var datos = new Data.npsEntities())
            {
                var _usuario = datos.Usuarios.Where(x => x.Id == IdUsuario).FirstOrDefault();

                if (_usuario != null)
                {
                    _usuario.Pass = NIXO_Framework.Seguridad.BlankPassword;

                    datos.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}