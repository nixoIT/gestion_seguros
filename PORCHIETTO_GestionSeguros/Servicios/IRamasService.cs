﻿using PORCHIETTO_GestionSeguros.Models.Ramas;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface IRamasService
    {
        RamaModel ObtenerRama();

        AgregarRamaModel ObtenerRama(int Id);

        AgregarRamaResponse AgregarRama(AgregarRamaRequest request);

        ModificarRamaResponse ModificarRama(ModificarRamaRequest request);

        void EliminarRama(int Id);

        RamaModel ObtenerRamasEmpresa(int Id);
    }
}