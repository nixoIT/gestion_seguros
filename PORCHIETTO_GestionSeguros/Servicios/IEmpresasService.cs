﻿using PORCHIETTO_GestionSeguros.Models.Empresas;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface IEmpresasService : IServicioBase
    {
        EmpresasViewModel ObtenerEmpresas();
        AgregarEmpresaResponse AgregarEmpresa(AgregarEmpresaRequest request);
        ModificarEmpresaViewModel ObtenerEmpresa(int Id);
        ModificarEmpresaResponse ModificarEmpresa(ModificarEmpresaRequest request);
        void EliminarEmpresa(int Id);

        RequerimientosModel ObtenerRequerimientos();
        AgregarRequerimientoModel ObtenerRequerimiento(int Id);
        AgregarRequerimientoResponse AgregarRequerimiento(AgregarRequerimientoRequest request);
        ModificarRequerimientoResponse ModificarRequerimiento(ModificarRequerimientoRequest request);
        void EliminarRequerimiento(int Id);

        RequerimientosModel ObtenerRequerimientosEmpresa(int Id);
        ListarRequerimientosEmpresaModel ListarRequerimientosEmpresa(int Id);
        AgregarRequerimientoEmpresaResponse AgregarRequerimientoEmpresa(AgregarRequerimientoEmpresaRequest request);
        void EliminarRequerimientoEmpresa(int Id, int IdEmpresa);
    }
}