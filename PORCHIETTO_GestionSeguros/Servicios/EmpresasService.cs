﻿using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Empresas;
using System;
using System.Linq;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public class EmpresasService : IEmpresasService
    {
        #region EMPRESAS
        public EmpresasViewModel ObtenerEmpresas()
        {
            using (var datos = new Data.npsEntities())
            {
                var _Empresas = datos.Empresas.Where(x => x.Borrado == null).Select(x => new AgregarEmpresaViewModel()
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    CUIT = x.Cuit,
                    Domicilio = x.Direccion,
                    Telefono = x.Telefono,
                    Web = x.Web,
                    ActividadPrincipal = x.ActividadPrincipal,
                    ActividadSecundaria = x.ActividadSecundaria
                }).ToList();

                return new EmpresasViewModel() { Lista = _Empresas };
            }
        }

        public AgregarEmpresaResponse AgregarEmpresa(AgregarEmpresaRequest request)
        {
            using (var datos = new npsEntities())
            {
                var _Empresa = new Empresas
                {
                    Nombre = request.Nombre,
                    Cuit = request.CUIT,
                    Direccion = request.Domicilio,
                    Telefono = request.Telefono,
                    Web = request.Web,
                    ActividadPrincipal = request.ActividadPrincipal,
                    ActividadSecundaria = request.ActividadSecundaria
                };

                datos.Empresas.Add(_Empresa);
                datos.SaveChanges();
            }

            return new AgregarEmpresaResponse();
        }

        public ModificarEmpresaViewModel ObtenerEmpresa(int Id)
        {
            using (var datos = new npsEntities())
            {
                return datos.Empresas.Where(x => x.Id == Id && x.Borrado == null).Select(x => new ModificarEmpresaViewModel
                {
                    Id = x.Id,
                    Nombre = x.Nombre,
                    CUIT = x.Cuit,
                    Domicilio = x.Direccion,
                    Telefono = x.Telefono,
                    Web = x.Web,
                    ActividadPrincipal = x.ActividadPrincipal,
                    ActividadSecundaria = x.ActividadSecundaria
                }).FirstOrDefault();
            }
        }

        public ModificarEmpresaResponse ModificarEmpresa(ModificarEmpresaRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var _empresa = datos.Empresas.Where(x => x.Id == request.Id).FirstOrDefault();

                if (_empresa != null)
                {
                    _empresa.Nombre = request.Nombre;
                    _empresa.Cuit = request.CUIT;
                    _empresa.Direccion = request.Domicilio;
                    _empresa.Telefono = request.Telefono;
                    _empresa.Web = request.Web;
                    _empresa.ActividadPrincipal = request.ActividadPrincipal;
                    _empresa.ActividadSecundaria = request.ActividadSecundaria;

                    datos.SaveChanges();
                }

                return new ModificarEmpresaResponse { };
            }
        }

        public void EliminarEmpresa(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var usuario = datos.Empresas.Where(x => x.Id == Id).First();

                usuario.Borrado = DateTime.Today;

                datos.SaveChanges();
            }
        }
        #endregion

        #region REQUERIMIENTOS
        public RequerimientosModel ObtenerRequerimientos()
        {
            using (var datos = new Data.npsEntities())
            {
                var requerimientos = datos.Requerimientos.Where(x => x.Borrado == null).Select(x => new AgregarRequerimientoModel()
                {
                    Id = x.Id,
                    Nombre = x.Requerimiento,
                }).ToList();

                return new RequerimientosModel() { Lista = requerimientos };
            }
        }

        public AgregarRequerimientoModel ObtenerRequerimiento(int Id)
        {
            using (var datos = new npsEntities())
            {
                return datos.Requerimientos.Where(x => x.Id == Id && x.Borrado == null).Select(x => new AgregarRequerimientoModel
                {
                    Id = x.Id,
                    Nombre = x.Requerimiento
                }).FirstOrDefault();
            }
        }


        public AgregarRequerimientoResponse AgregarRequerimiento(AgregarRequerimientoRequest request)
        {
            using (var datos = new npsEntities())
            {
                var _Requerimiento = new Requerimientos
                {
                    Requerimiento = request.Nombre
                };

                datos.Requerimientos.Add(_Requerimiento);
                datos.SaveChanges();
                return new AgregarRequerimientoResponse { Id = _Requerimiento.Id};
            }
        }

        public ModificarRequerimientoResponse ModificarRequerimiento(ModificarRequerimientoRequest request)
        {
            using (var datos = new Data.npsEntities())
            {
                var _Requerimiento = datos.Requerimientos.Where(x => x.Id == request.Id).FirstOrDefault();

                if (_Requerimiento != null)
                {
                    _Requerimiento.Requerimiento = request.Nombre;

                    datos.SaveChanges();
                }

                return new ModificarRequerimientoResponse { Id = _Requerimiento.Id};
            }
        }

        public void EliminarRequerimiento(int Id)
        {
            using (var datos = new Data.npsEntities())
            {
                var requerimiento = datos.Requerimientos.Where(x => x.Id == Id).First();

                requerimiento.Borrado = DateTime.Today;

                datos.SaveChanges();
            }
        }
        #endregion

        #region REQUERIMIENTOS-EMPRESAS
        public RequerimientosModel ObtenerRequerimientosEmpresa(int Id)
        {
            using (var datos = new npsEntities())
            {
                var requerimientos = datos.RequerimientosEmpresas.Where(x => x.Id_Empresa == Id && x.Borrado == null).Select(x => new AgregarRequerimientoModel()
                {
                    Id = x.Requerimientos.Id,
                    Nombre = x.Requerimientos.Requerimiento
                }).ToList();

                return new RequerimientosModel() { Lista = requerimientos };
            }
        }

        public ListarRequerimientosEmpresaModel ListarRequerimientosEmpresa(int Id)
        {
            using (var datos = new npsEntities())
            {
                var requerimientos = datos.RequerimientosEmpresas.Where(x => x.Id_Empresa == Id && x.Borrado == null).Select(x => new RequerimientoEmpresaModel()
                {
                    Id = x.Requerimientos.Id,
                    Requerimiento = x.Requerimientos.Requerimiento,
                    Id_Rama = x.Id_Rama,
                    Rama = x.Ramas.Nombre
                }).ToList();

                return new ListarRequerimientosEmpresaModel() { Lista = requerimientos };
            }
        }

        public AgregarRequerimientoEmpresaResponse AgregarRequerimientoEmpresa(AgregarRequerimientoEmpresaRequest request)
        {
            using (var datos = new npsEntities())
            {
                var _RequerimientoEmpresa = new RequerimientosEmpresas
                {
                    Id_Empresa = request.Id_Empresa,
                    Id_Rama = request.Id_Rama,
                    Id_Requerimiento = request.Id_Requerimiento
                };

                datos.RequerimientosEmpresas.Add(_RequerimientoEmpresa);
                datos.SaveChanges();
                return new AgregarRequerimientoEmpresaResponse { Id = _RequerimientoEmpresa.Id };
            }
        }

        /// <summary>
        /// Elimina un requerimiento seteado a una empresa
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="IdEmpresa"></param>
        public  void EliminarRequerimientoEmpresa(int Id, int IdEmpresa)
        {
            using (var datos = new Data.npsEntities())
            {
                var requerimiento = datos.RequerimientosEmpresas.Where(x => x.Id_Requerimiento == Id && x.Id_Empresa == IdEmpresa).First();

                requerimiento.Borrado = DateTime.Today;

                datos.SaveChanges();
            }
        }
        #endregion
    }
}