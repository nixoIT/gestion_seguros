﻿using PORCHIETTO_GestionSeguros.Models.Clientes;
using PORCHIETTO_GestionSeguros.Models.MediosDeCobranza;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface IMediosDeCobranzaService
    {
        MediosDeCobranzaModel ObtenerMediosDeCobranza();
        AgregarMediosDeCobranzaModel ObtenerMediosDeCobranza(int Id);
        ModificarMediosDeCobranzaModel ObtenerMediosDeCobranzaAEditar(int Id);
        AgregarMediosDeCobranzaResponse AgregarMediosDeCobranza(AgregarMediosDeCobranzaRequest request);
        void EliminarMediosDeCobranza(int Id);
        ModificarMediosDeCobranzaResponse ModificarMediosDeCobranza(ModificarMediosDeCobranzaRequest request);
        ReporteClientesModel GetReportesPorCliente(GetReportePorClienteRequest request);
    }
}