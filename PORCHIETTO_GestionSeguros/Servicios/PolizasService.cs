﻿using Excel;
using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Clientes;
using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using PORCHIETTO_GestionSeguros.Models.Polizas;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace PORCHIETTO_GestionSeguros.Servicios
{
	public class PolizasService : IPolizasService
	{
		#region Polizas
		public PolizaModel ObtenerPolizas(PolizaListadoRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var db = request.IdProductor == 0 ? datos.Polizas : datos.Polizas.Where(x => x.IdProductor == request.IdProductor);

				var polizas = db.Where(x => x.Borrado == null && x.Vencimiento >= DateTime.Today).Select(x => new AgregarPolizaModel()
				{
					Id = x.Id,
					Numero = x.Numero,
					IdCliente = x.IdCliente,
					NombreCliente = (string.IsNullOrEmpty(x.Clientes.Nombre.Trim()) ? x.Clientes.Apellido.ToUpper().ToString() : x.Clientes.Apellido.ToUpper().ToString() + ", " + x.Clientes.Nombre.ToString()),
					Cobertura = x.Cobertura,
					IdEmpresa = x.IdEmpresa,
					NombreEmpresa = x.Empresas.Nombre,
					IdProductor = x.IdProductor,
					NombreProductor = x.Productores.Nombre,
					IdRama = x.IdRama,
					NombreTipoPoliza = x.Ramas.Nombre,
					Suplemento = x.Suplemento,
					Vencimiento = x.Vencimiento,
					Importada = x.Importada,
					Dominio = x.Dominio,
					Unidad_Nomina = x.Unidad_Nomina
				});

				return new PolizaModel() { Lista = polizas.ToList(), IdProductor = request.IdProductor};
			}
		}

		public PolizaModel ObtenerMisPolizas(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var polizas = datos.Polizas.Where(x => x.Borrado == null && x.Vencimiento >= DateTime.Today && x.IdCliente == Id).Select(x => new AgregarPolizaModel()
				{
					Id = x.Id,
					Numero = x.Numero,
					IdCliente = x.IdCliente,
					NombreCliente = (string.IsNullOrEmpty(x.Clientes.Nombre.Trim()) ? x.Clientes.Apellido.ToUpper().ToString() : x.Clientes.Apellido.ToUpper().ToString() + ", " + x.Clientes.Nombre.ToString()),
					Cobertura = x.Cobertura,
					IdEmpresa = x.IdEmpresa,
					NombreEmpresa = x.Empresas.Nombre,
					IdProductor = x.IdProductor,
					NombreProductor = x.Productores.Nombre,
					IdRama = x.IdRama,
					NombreTipoPoliza = x.Ramas.Nombre,
					Suplemento = x.Suplemento,
					Vencimiento = x.Vencimiento,
					Importada = x.Importada,
					Dominio = x.Dominio,
					Unidad_Nomina = x.Unidad_Nomina
				});

				return new PolizaModel() { Lista = polizas.ToList() };
			}
		}

		public ModificarPolizaModel ObtenerPoliza(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				return datos.Polizas.Where(x => x.Borrado == null && x.Id == Id).Select(x => new ModificarPolizaModel()
				{
					Id = x.Id,
					Numero = x.Numero,
					IdCliente = x.IdCliente,
					Cobertura = x.Cobertura,
					IdEmpresa = x.IdEmpresa,
					IdProductor = x.IdProductor,
					IdRama = x.IdRama,
					Suplemento = x.Suplemento,
					Vencimiento = x.Vencimiento,
					Unidad_Nomina = x.Unidad_Nomina,
					Dominio = x.Dominio
				}).FirstOrDefault();
			}
		}

		public AgregarPolizaResponse AgregarPoliza(AgregarPolizaRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var poliza = datos.Polizas.Where(x => x.Id == request.Id).FirstOrDefault();

				if (poliza != null)
					return new AgregarPolizaResponse { Id = 0 };
				else
				{
					Polizas nueva_poliza = new Polizas
					{
						Id = request.Id,
						Numero = request.Numero,
						IdCliente = request.IdCliente,
						Cobertura = request.Cobertura,
						IdEmpresa = request.IdEmpresa,
						IdProductor = request.IdProductor,
						IdRama = request.IdRama,
						Suplemento = request.Suplemento,
						Vencimiento = request.Vencimiento,
						Dominio = request.Dominio,
						Unidad_Nomina = request.Unidad_Nomina
					};

					datos.Polizas.Add(nueva_poliza);
					datos.SaveChanges();

					return new AgregarPolizaResponse { Id = nueva_poliza.Id };
				}
			}
		}

		public ModificarPolizaResponse ModificarPoliza(ModificarPolizaRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var poliza = datos.Polizas.Where(x => x.Id == request.Id).FirstOrDefault();

				if (poliza != null)
				{
					poliza.Id = request.Id;
					poliza.IdCliente = request.IdCliente;
					poliza.Cobertura = request.Cobertura;
					poliza.IdEmpresa = request.IdEmpresa;
					poliza.IdProductor = request.IdProductor;
					poliza.IdRama = request.IdRama;
					poliza.Suplemento = request.Suplemento;
					poliza.Vencimiento = request.Vencimiento;
					poliza.Dominio = request.Dominio;
					poliza.Unidad_Nomina = request.Unidad_Nomina;

					datos.SaveChanges();

					return new ModificarPolizaResponse { Id = poliza.Id };
				}
				else
				{
					return new ModificarPolizaResponse { Id = 0 };
				}
			}
		}

		public void EliminarPoliza(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var poliza = datos.Polizas.Where(x => x.Id == Id).First();

				poliza.Borrado = DateTime.Today;

				datos.SaveChanges();
			}
		}

		public PolizaModel ObtenerPolizasBorradas(PolizaListadoRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var db = request.IdProductor == 0 ? datos.Polizas : datos.Polizas.Where(x => x.IdProductor == request.IdProductor);
				var polizas = db.Where(x => x.Borrado != null).OrderByDescending(y => y.Borrado).Select(x => new AgregarPolizaModel()
				{
					Id = x.Id,
					Numero = x.Numero,
					IdCliente = x.IdCliente,
					NombreCliente = string.IsNullOrEmpty(x.Clientes.Nombre) ? x.Clientes.Apellido.ToUpper() : x.Clientes.Apellido.ToUpper() + ", " + x.Clientes.Nombre,
					Cobertura = x.Cobertura,
					IdEmpresa = x.IdEmpresa,
					NombreEmpresa = x.Empresas.Nombre,
					IdProductor = x.IdProductor,
					NombreProductor = x.Productores.Nombre,
					IdRama = x.IdRama,
					NombreTipoPoliza = x.Ramas.Nombre,
					Suplemento = x.Suplemento,
					Vencimiento = x.Vencimiento,
					Importada = x.Importada,
					Borrado = x.Borrado,
					Dominio = x.Dominio,
					Unidad_Nomina = x.Unidad_Nomina,
				});

				return new PolizaModel() { Lista = polizas.ToList(), IdProductor = request.IdProductor };
			}
		}

		public PolizaModel ObtenerPolizasVencidas(PolizaListadoRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var db = request.IdProductor == 0 ? datos.Polizas : datos.Polizas.Where(x => x.IdProductor == request.IdProductor);
				var polizas = db.Where(x => x.Vencimiento < DateTime.Today).OrderByDescending(y => y.Borrado).Select(x => new AgregarPolizaModel()
				{
					Id = x.Id,
					Numero = x.Numero,
					IdCliente = x.IdCliente,
					NombreCliente = string.IsNullOrEmpty(x.Clientes.Nombre) ? x.Clientes.Apellido.ToUpper() : x.Clientes.Apellido.ToUpper() + ", " + x.Clientes.Nombre,
					Cobertura = x.Cobertura,
					IdEmpresa = x.IdEmpresa,
					NombreEmpresa = x.Empresas.Nombre,
					IdProductor = x.IdProductor,
					NombreProductor = x.Productores.Nombre,
					IdRama = x.IdRama,
					NombreTipoPoliza = x.Ramas.Nombre,
					Suplemento = x.Suplemento,
					Vencimiento = x.Vencimiento,
					Importada = x.Importada,
					Borrado = x.Borrado,
					Dominio = x.Dominio,
					Unidad_Nomina = x.Unidad_Nomina,
				});

				return new PolizaModel() { Lista = polizas.ToList(), IdProductor = request.IdProductor };
			}
		}

		public List<AgregarPolizaModel> ObtenerPolizasDeCliente(int IdCliente)
		{
			using (var datos = new npsEntities())
			{
				var rto = string.Empty;
				return datos.Polizas.Where(x => x.IdCliente == IdCliente && x.Borrado == null && x.Vencimiento > DateTime.Today).Select(x => new AgregarPolizaModel()
				{
					Id = x.Id,
					Numero = x.Numero,
					IdCliente = x.IdCliente,
					NombreCliente = string.IsNullOrEmpty(x.Clientes.Nombre) ? x.Clientes.Apellido.ToUpper() : x.Clientes.Apellido.ToUpper() + ", " + x.Clientes.Nombre,
					Cobertura = x.Cobertura,
					IdEmpresa = x.IdEmpresa,
					NombreEmpresa = x.Empresas.Nombre,
					IdProductor = x.IdProductor,
					NombreProductor = x.Productores.Nombre,
					IdRama = x.IdRama,
					NombreTipoPoliza = x.Ramas.Nombre,
					Suplemento = x.Suplemento,
					Vencimiento = x.Vencimiento,
					Importada = x.Importada,
					Dominio = x.Dominio,
					Unidad_Nomina = x.Unidad_Nomina
				}).ToList();
			}
		}
		#endregion

		#region Cargar Poliza
		public bool ValidarArchivo(TipoPlantilla tipoPlantilla, string archivo, int IdEmpresa, out List<ImportarPolizaModel> Listado)
		{
			var ListaProvincias = ObtenerProvincias(new ObtenerProvinciasRequest { });
			Listado = new List<ImportarPolizaModel> { };
			bool valid = false;
			MemoryStream mStream = new MemoryStream(System.IO.File.ReadAllBytes(archivo));
			IExcelDataReader excelReader = null;
			if (Path.GetExtension(archivo) == ".xlsx")
				excelReader = ExcelReaderFactory.CreateOpenXmlReader(mStream);
			else if (Path.GetExtension(archivo) == ".xls")
				excelReader = ExcelReaderFactory.CreateBinaryReader(mStream);
			else
				return false;

			try
			{
				DataSet result = excelReader.AsDataSet();
				DataTable table = result.Tables[0];

				if (tipoPlantilla == TipoPlantilla.POLIZAS)
				{
					if (table.Rows[0].ItemArray[0].ToString().Trim().ToUpper() == "RAMA"
						&& table.Rows[0].ItemArray[1].ToString().Trim().ToUpper() == "POLIZA"
						&& table.Rows[0].ItemArray[2].ToString().Trim().ToUpper() == "SUPL."
						&& table.Rows[0].ItemArray[3].ToString().Trim().ToUpper() == "ASEGURADO"
						&& table.Rows[0].ItemArray[17].ToString().Trim().ToUpper() == "INICIO VIGENCIA"
						&& table.Rows[0].ItemArray[18].ToString().Trim().ToUpper() == "FIN VIGENCIA")
					{
						var cont = 0;
						foreach (DataRow filas in table.Rows)
						{
							cont++;

							if (cont > 1 && !string.IsNullOrEmpty(filas.ItemArray[1].ToString()))
							{
								var numeroDNI = filas.ItemArray[7].ToString();
								var numeroCUIT = filas.ItemArray[5].ToString();
								var cliente = ObtenerClientePorDNIoCUIT(numeroDNI, numeroCUIT);
								var rama = ObtenerRama(IdEmpresa, int.Parse(filas.ItemArray[0].ToString()));

								if (cliente != null)
								{
									Listado.Add(new ImportarPolizaModel
									{
										Numero = filas.ItemArray[1].ToString(),
										IdCliente = cliente.Id,
										IdRama = rama,
										Cobertura = filas.ItemArray[16].ToString(),
										Suplemento = int.Parse(filas.ItemArray[2].ToString()),
										Vencimiento = DateTime.Parse(filas.ItemArray[18].ToString())
									});
								}
								else
								{
									var tmpNombreCompleto = filas.ItemArray[3].ToString().Trim();
									var tmpApellido = filas.ItemArray[3].ToString().Split(',')[0].Trim();
									var tmpNombre = string.Empty;
									if (filas.ItemArray[3].ToString().Split(',').Length > 1)
										tmpNombre = filas.ItemArray[3].ToString().Split(',')[1].Trim();
									var tmpDomicilio = ObtenerDomicilio(filas.ItemArray[8].ToString());
									var tmpProvincia = ObtenerProvincia(filas.ItemArray[11].ToString(), ListaProvincias.Provincias);
									var tmpLocalidad = ObtenerLocalidad(filas.ItemArray[10].ToString(), tmpProvincia);
									//Si no reconoce la provincia o la localidad, salteo el registro.
									if (tmpProvincia == 0 || tmpLocalidad == 0)
										continue;
									var tmpEmail = filas.ItemArray[15].ToString().Trim();

									var clienteNuevoRequest = new AgregarClienteModel
									{
										Apellido = tmpApellido,
										Nombre = tmpNombre,
										NombreCompleto = tmpNombreCompleto,
										CUIT = numeroCUIT,
										DNI = numeroDNI,
										FechaNacimiento = null,
										Fecha = DateTime.Now,
										DomicilioCalle = tmpDomicilio[0],
										DomicilioNumero = tmpDomicilio[1],
										DomicilioCobroCalle = tmpDomicilio[0],
										DomicilioCobroNumero = tmpDomicilio[1],
										CodigoProvincia = tmpProvincia,
										CodigoProvinciaCobro = tmpProvincia,
										CodigoLocalidad = tmpLocalidad,
										CodigoLocalidadCobro = tmpLocalidad,
										email = tmpEmail,
										IdMediodeCobranza = 1
									};

									var _id = ImportarClientePoliza(clienteNuevoRequest);

									Listado.Add(new ImportarPolizaModel
									{
										Numero = filas.ItemArray[1].ToString(),
										IdCliente = _id,
										IdRama = int.Parse(filas.ItemArray[0].ToString()),
										Cobertura = filas.ItemArray[16].ToString(),
										Suplemento = int.Parse(filas.ItemArray[2].ToString()),
										Vencimiento = DateTime.Parse(filas.ItemArray[18].ToString())
									});
								}
							}
						}

						valid = true;
					}
				}
			}
			catch (Exception e)
			{
				var error = e.Message;
			}

			return valid;
		}

		public bool ImportarPlantillaPoliza(List<ImportarPolizaModel> Polizas)
		{
			using (var datos = new npsEntities())
			{
				var Empresa = Polizas.First().IdEmpresa;
				var PolizasExistentes = datos.Polizas.Where(x => x.Borrado == null && x.Vencimiento >= DateTime.Now && x.IdEmpresa == Empresa).ToList();

				foreach (var poliza in Polizas)
				{
					if (!PolizasExistentes.Any(x => x.IdRama == poliza.IdRama && x.Numero == poliza.Numero && x.Suplemento == poliza.Suplemento))
					{
						var _poliza = new Polizas
						{
							IdCliente = poliza.IdCliente,
							Numero = poliza.Numero,
							IdEmpresa = poliza.IdEmpresa,
							IdProductor = poliza.IdProductor,
							IdRama = poliza.IdRama,
							Cobertura = poliza.Cobertura,
							Suplemento = poliza.Suplemento,
							Vencimiento = poliza.Vencimiento,
							Importada = true,
							Unidad_Nomina = string.Empty,
							Dominio = string.Empty
						};

						datos.Polizas.Add(_poliza);
					}
				}

				datos.SaveChanges();
			}

			return true;
		}

		private AgregarClienteModel ObtenerClientePorDNIoCUIT(string numeroDNI, string numeroCUIT)
		{
			using (var datos = new npsEntities())
			{
				if (!string.IsNullOrEmpty(numeroDNI) && numeroDNI != "0")
				{
					return datos.Clientes.Where(x => x.Borrado == null && x.DNI == numeroDNI).Select(y => new AgregarClienteModel
					{
						Id = y.Id,
						Apellido = y.Apellido,
						Nombre = y.Nombre
					}).FirstOrDefault();
				}
				else if (!string.IsNullOrEmpty(numeroCUIT) && numeroCUIT != "0")
				{
					return datos.Clientes.Where(x => x.Borrado == null && x.CUIT == numeroCUIT).Select(y => new AgregarClienteModel
					{
						Id = y.Id,
						Apellido = y.Apellido,
						Nombre = y.Nombre
					}).FirstOrDefault();
				}
				else
				{
					return new AgregarClienteModel { };
				}
			}
		}

		private int ImportarClientePoliza(AgregarClienteModel ClienteNuevo)
		{
			using (var datos = new npsEntities())
			{
				var _cliente = new Clientes
				{
					Apellido = ClienteNuevo.Apellido,
					Nombre = ClienteNuevo.Nombre,
					Carpeta = ClienteNuevo.Carpeta,
					CodigoLocalidad = ClienteNuevo.CodigoLocalidad,
					CodigoLocalidadCobro = ClienteNuevo.CodigoLocalidadCobro,
					CodigoProvincia = ClienteNuevo.CodigoProvincia,
					CodigoProvinciaCobro = ClienteNuevo.CodigoProvinciaCobro,
					CUIT = ClienteNuevo.CUIT,
					DNI = ClienteNuevo.DNI,
					DomicilioCalle = ClienteNuevo.DomicilioCalle,
					DomicilioCobroCalle = ClienteNuevo.DomicilioCobroCalle,
					DomicilioNumero = ClienteNuevo.DomicilioNumero,
					DomicilioCobroNumero = ClienteNuevo.DomicilioCobroNumero,
					Email = ClienteNuevo.email,
					EsCabezadeGrupo = ClienteNuevo.EsCabezadeGrupo,
					FechaAlta = ClienteNuevo.Fecha,
					FechaNacimiento = ClienteNuevo.FechaNacimiento,
					IdGrupodeCobranza = ClienteNuevo.IdGrupodeCobranza,
					IdMediosdeCobranza = ClienteNuevo.IdMediodeCobranza,
					TelefonoFijo = ClienteNuevo.TelefonoFijo,
					TelefonoMovil = ClienteNuevo.TelefonoMovil,
				};

				datos.Clientes.Add(_cliente);
				datos.SaveChanges();

				return _cliente.Id;
			}
		}

		private string[] ObtenerDomicilio(string Domicilio)
		{
			var resultado = new string[2];

			try
			{
				var tmpCadena = Domicilio.Split(' ').Where(x => !string.IsNullOrEmpty(x.Trim())).ToList();

				for (var i = 0; i < tmpCadena.Count - 1; i++)
				{
					resultado[0] += " " + tmpCadena[i];
				}

				resultado[0] = resultado[0].Trim();
				resultado[1] = tmpCadena[tmpCadena.Count - 1].Trim();
			}
			catch
			{
				resultado[0] = Domicilio;
			}
			return resultado;
		}

		public ObtenerProvinciasResponse ObtenerProvincias(ObtenerProvinciasRequest request)
		{
			using (var datos = new npsEntities())
			{
				if (request.Codigo.HasValue)
				{
					return new ObtenerProvinciasResponse()
					{
						Provincias = datos.Provincias.Where(x => x.Codigo == request.Codigo).Select(x => new ProvinciasModel()
						{
							Id = x.Id,
							Codigo = x.Codigo,
							Nombre = x.Nombre
						}).OrderBy(x => x.Nombre).ToList()
					};
				}
				else
				{
					return new ObtenerProvinciasResponse()
					{
						Provincias = datos.Provincias.Select(x => new ProvinciasModel()
						{
							Id = x.Id,
							Codigo = x.Codigo,
							Nombre = x.Nombre
						}).OrderBy(x => x.Nombre).ToList()
					};
				}
			}
		}

		private int ObtenerProvincia(string NombreProvincia, List<ProvinciasModel> ListaProvincias)
		{
			foreach (var prov in ListaProvincias)
			{
				prov.Nombre = prov.Nombre.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");
				if (string.Equals(prov.Nombre, NombreProvincia, StringComparison.CurrentCultureIgnoreCase))
				{
					return prov.Codigo;
				}
			}

			return 0;
		}

		private int ObtenerLocalidad(string NombreLocalidad, int CodigoProvincia)
		{
			using (var datos = new npsEntities())
			{
				var ListaLocalidades = datos.Localidades.Where(x => x.CodigoProvincia == CodigoProvincia).ToList();

				foreach (var localidad in ListaLocalidades)
				{
					localidad.Nombre = localidad.Nombre.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").Replace("ú", "u");
					if (string.Equals(localidad.Nombre, NombreLocalidad, StringComparison.CurrentCultureIgnoreCase))
					{
						return localidad.Codigo;
					}
				}

				return 0;
			}
		}

		private int ObtenerRama(int IdEmpresa, int CodigoRama)
		{
			using (var datos = new npsEntities())
			{
				return datos.Ramas.Where(x => x.Id_Empresa == IdEmpresa && x.Codigo == CodigoRama && x.Borrado == null).Select(y => y.Id).FirstOrDefault();
			}
		}
		#endregion

		#region Notificaciones Pólizas

		public void AgregarNotificaciones(NotificacionesPolizasModel request)
		{
			using (var datos = new npsEntities())
			{
				Data.NotificacionesPolizas notificacion = new Data.NotificacionesPolizas
				{
					IdPoliza = request.IdPoliza,
					FechaInicio = request.FechaInicio,
					FechaFin = request.FechaFinal,
					Mensaje = request.Mensaje,
					VisibleCliente = request.VisibleCliente,
					IdUsuario = request.IdUsuario,
				};

				datos.NotificacionesPolizas.Add(notificacion);
				datos.SaveChanges();
			}
		}

		public InformeNotificacionesPolizas ObtenerInformeNotificacionesPolizas(int IdPoliza)
		{
			using (var datos = new npsEntities())
			{
				return new InformeNotificacionesPolizas
				{
					IdPoliza = IdPoliza,
					Cliente = datos.Polizas.Where(x => x.Id == IdPoliza).Select(y => y.Clientes.Nombre + " " + y.Clientes.Apellido).FirstOrDefault(),
					Lista = datos.NotificacionesPolizas.Where(x => x.IdPoliza == IdPoliza && x.Borrado == null).Select(y => new Models.Notificaciones.NotificacionesPolizas
					{
						Id = y.Id,
						IdPoliza = y.IdPoliza,
						Mensaje = y.Mensaje,
						FechaInicio = y.FechaInicio,
						FechaFin = y.FechaFin,
						VisibleCliente = y.VisibleCliente,
						IdUsuario = y.IdUsuario ?? 0,
						Usuario = y.Usuarios.Usuario
					}).ToList()
				};
			}
		}

		public InformeNotificacionesPolizas ObtenerNotificacionesPolizasCliente(int IdPoliza)
		{
			using (var datos = new npsEntities())
			{
				return new InformeNotificacionesPolizas
				{
					IdPoliza = IdPoliza,
					Cliente = datos.Polizas.Where(x => x.Id == IdPoliza).Select(y => y.Clientes.Nombre + " " + y.Clientes.Apellido).FirstOrDefault(),
					Lista = datos.NotificacionesPolizas.Where(x => x.IdPoliza == IdPoliza && x.Borrado == null && x.VisibleCliente == true).Select(y => new Models.Notificaciones.NotificacionesPolizas
					{
						Id = y.Id,
						IdPoliza = y.IdPoliza,
						Mensaje = y.Mensaje,
						FechaInicio = y.FechaInicio,
						FechaFin = y.FechaFin,
						VisibleCliente = y.VisibleCliente,
						IdUsuario = y.IdUsuario ?? 0,
						Usuario = y.Usuarios.Usuario
					}).ToList()
				};
			}
		}

		public void EliminarNotificacionPolizas(int Id)
		{
			using (var datos = new npsEntities())
			{
				var Poliza = datos.NotificacionesPolizas.Where(x => x.Id == Id).First();

				Poliza.Borrado = DateTime.Today;

				datos.SaveChanges();
			}
		}

		#endregion

	}
}