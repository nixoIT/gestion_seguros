﻿using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using PORCHIETTO_GestionSeguros.Models.Polizas;
using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface IPolizasService : IServicioBase
    {
        #region Polizas
        PolizaModel ObtenerPolizas(PolizaListadoRequest request);
        PolizaModel ObtenerMisPolizas(int Id);
        ModificarPolizaModel ObtenerPoliza(int Id);
        AgregarPolizaResponse AgregarPoliza(AgregarPolizaRequest request);
        ModificarPolizaResponse ModificarPoliza(ModificarPolizaRequest request);
        void EliminarPoliza(int Id);
        PolizaModel ObtenerPolizasBorradas(PolizaListadoRequest request);
        PolizaModel ObtenerPolizasVencidas(PolizaListadoRequest request);
        List<AgregarPolizaModel> ObtenerPolizasDeCliente(int IdCliente);
        #endregion

        #region Cargar Poliza
        bool ValidarArchivo(TipoPlantilla tipoPlantilla, string archivo, int IdEmpresa, out List<ImportarPolizaModel> Listado);
        bool ImportarPlantillaPoliza(List<ImportarPolizaModel> Polizas);
        #endregion

        #region Notificaciones Pólizas
        void AgregarNotificaciones(NotificacionesPolizasModel request);
        InformeNotificacionesPolizas ObtenerInformeNotificacionesPolizas(int IdPoliza);
		InformeNotificacionesPolizas ObtenerNotificacionesPolizasCliente(int IdPoliza);

		void EliminarNotificacionPolizas(int Id);
        #endregion

    }
}