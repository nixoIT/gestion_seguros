﻿using PORCHIETTO_GestionSeguros.Models.Productores;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface IProductoresService : IServicioBase
    {
        ProductoresViewModel ObtenerProductores();
        AgregarProductorResponse AgregarProductor(AgregarProductorRequest request);
        ModificarProductorModel ObtenerProductor(int Id);
        ModificarProductorResponse ModificarProductor(ModificarProductorRequest request);
        void EliminarProductor(int Id);
        AgregarUsuarioProductorResponse AgregarUsuarioProductor(AgregarUsuarioProductorRequest request);
        AgregarUsuarioProductorModel ObtenerUsuarioCliente(int Id);
        bool ExisteUsuario(string name);
        bool BorrarUsuarioProductor(int IdUsuario, int IdProductor);
        bool BlanquearPassUsuarioProductor(int IdUsuario);
    }
}