﻿using PORCHIETTO_GestionSeguros.Models.Login;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using NIXO_Framework;
using System.Linq;
using PORCHIETTO_GestionSeguros.Models.Clientes;

namespace PORCHIETTO_GestionSeguros.Servicios
{
	public class LoginService : ILoginService
	{
		public LoginResponse LoguearUsuario(LoginRequest Request)
		{
			string cadena = Seguridad.GetHashSha256(Request.Pass);

			var Resultado = new Usuario();
			Resultado = ObtenerUsuario(new ObtenerUsuarioRequest() { Usuario = Request.Usuario });

			if (Resultado != null && cadena == Resultado.Pass)
			{
				return new LoginResponse
				{
					Aceptado = true,
					Niveles = Resultado.Niveles,
					NombreUsuario = Resultado.NombreUsuario,
					IdUsuario = Resultado.IdUsuario,
					CambiarPass = cadena == Seguridad.BlankPassword ? true : false
				};
			}
			else
			{
				return new LoginResponse { Aceptado = false };
			}
		}

		private Usuario ObtenerUsuario(ObtenerUsuarioRequest Request)
		{
			using (var datos = new Data.npsEntities())
			{
				var Respuesta = datos.Usuarios.Where(x => x.Usuario == Request.Usuario).Select(x => new Usuario()
				{
					IdUsuario = x.Id,
					NombreUsuario = x.Nombre,
					Pass = x.Pass,
					Niveles = datos.NivelesUsuarios.Where(a => a.IdUsuario == x.Id && a.Borrado == null).Select(y => new NivelModel() { Id = y.IdNivel, Nombre = y.Niveles.Nivel }).ToList()
				}).FirstOrDefault();

				//Respuesta.Niveles = datos.NivelesUsuarios.Where(x => x.IdUsuario == Respuesta.IdUsuario && x.Borrado == null).Select(y => new NivelModel()
				//{
				//	Id = y.IdNivel,
				//	Nombre = y.Niveles.Nivel
				//}).ToList();

				return Respuesta;
			}
		}

		public int ObtenerIdCliente(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				return datos.Clientes.Where(x => x.IdUsuario == Id).FirstOrDefault().Id;
			}
		}

		public int ObtenerIdProductor(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				return datos.Productores.Where(x => x.IdUsuario == Id).FirstOrDefault().Id;
			}
		}

		public AgregarClienteModel ExisteCorreo(string email)
		{
			using (var datos = new Data.npsEntities())
			{
				AgregarClienteModel _cliente = datos.Clientes
					.Where(x => x.Email == email)
					.Select(x => new AgregarClienteModel()
					{
						Id = x.Id,
						Apellido = x.Apellido,
						Nombre = x.Nombre,
						NombreCompleto = (string.IsNullOrEmpty(x.Nombre.Trim()) ? x.Apellido.ToUpper() : x.Apellido.ToUpper() + ", " + x.Nombre),
						email = x.Email,
						TelefonoFijo = x.TelefonoFijo,
						TelefonoMovil = x.TelefonoMovil,
						DomicilioCalle = x.DomicilioCalle,
						DomicilioNumero = x.DomicilioNumero,
						Fecha = x.FechaAlta,
						IdMediodeCobranza = x.IdMediosdeCobranza.Value,
						NombreMediodeCobranza = x.MediosdeCobranza.Nombre,
						Carpeta = x.Carpeta,
						CUIT = x.CUIT,
						DNI = x.DNI,
						FechaNacimiento = x.FechaNacimiento,
						Observaciones = x.ObservacionesClientes.Where(y => y.Id_Cliente == x.Id && y.Borrado == null).Select(y => y.Observacion).FirstOrDefault(),
						IdUsuario = x.IdUsuario
					})
					.FirstOrDefault();

				return _cliente;
			}
		}
	}
}