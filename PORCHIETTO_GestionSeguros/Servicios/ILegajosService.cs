﻿using PORCHIETTO_GestionSeguros.Models.Legajos;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface ILegajosService : IServicioBase
    {
        LegajosViewModel ObtenerLegajos();
        AgregarLegajoResponse AgregarLegajo(AgregarLegajoRequest request);
        ModificarLegajoResponse ModificarLegajo(ModificarLegajoRequest request);
        ModificarLegajoModel ObtenerLegajo(int Id);
        void EliminarLegajo(int Id);
    }
}