﻿using System;
using System.Linq;
using PORCHIETTO_GestionSeguros.Models.Clientes;
using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.MediosDeCobranza;
using System.Collections.Generic;
using NIXO_Framework;
using Niveles = NIXO_Framework.Niveles;

namespace PORCHIETTO_GestionSeguros.Servicios
{
	public class ClientesService : IClientesService
	{
		public ClientesModel ObtenerClientes()
		{
			using (var datos = new Data.npsEntities())
			{
				var _Clientes = datos.Clientes.Where(x => x.Borrado == null).Select(x => new AgregarClienteModel()
				{
					Id = x.Id,
					Apellido = x.Apellido,
					Nombre = x.Nombre,
					NombreCompleto = (string.IsNullOrEmpty(x.Nombre.Trim()) ? x.Apellido.ToUpper() : x.Apellido.ToUpper() + ", " + x.Nombre),
					email = x.Email,
					TelefonoFijo = x.TelefonoFijo,
					TelefonoMovil = x.TelefonoMovil,
					DomicilioCalle = x.DomicilioCalle,
					DomicilioNumero = x.DomicilioNumero,
					Fecha = x.FechaAlta,
					IdMediodeCobranza = x.IdMediosdeCobranza.Value,
					NombreMediodeCobranza = x.MediosdeCobranza.Nombre,
					Carpeta = x.Carpeta,
					CUIT = x.CUIT,
					DNI = x.DNI,
					FechaNacimiento = x.FechaNacimiento,
					Observaciones = x.ObservacionesClientes.Where(y => y.Id_Cliente == x.Id && y.Borrado == null).Select(y => y.Observacion).FirstOrDefault()
				}).ToList();

				//Agrego los datos de domicilios de cobro.
				foreach (var cli in _Clientes)
				{
					var domicilios = ObtenerDomicilioCobro(new ObtenerDomicilioCobroRequest { Cliente = cli });

					if (domicilios != null)
					{
						cli.DomicilioCobroCalle = domicilios.DocmicilioCalle;
						cli.DomicilioCobroNumero = domicilios.DomicilioNumero;
						cli.CodigoLocalidadCobro = domicilios.CodigoLocalidad;
					}
				}

				return new ClientesModel() { Lista = _Clientes };
			}
		}

		public ClientesListadoModel ObtenerClientesListado(ClientesListadoRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var db = request.IdProductor == 0 ? datos.Clientes : datos.Clientes.Where(x => x.IdProductor == request.IdProductor);

				var _Clientes = db.Where(x => x.Borrado == null).Select(x => new ClientesListado()
				{
					Id = x.Id,
					Nombre = (string.IsNullOrEmpty(x.Nombre.Trim()) ? x.Apellido.ToUpper().ToString() : x.Apellido.ToUpper().ToString() + ", " + x.Nombre.ToString()),
					Email = x.Email,
					Telefono = x.TelefonoFijo,
					TelefonoAlternativo = x.TelefonoMovil,
					Domicilio = x.DomicilioCalle + " " + x.DomicilioNumero,
					IdUsuario = x.IdUsuario
				}).OrderBy(x => x.Nombre).ToList();

				return new ClientesListadoModel() { Lista = _Clientes, IdProductor = request.IdProductor };
			}
		}

		public AgregarClienteModel ObtenerCliente(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var _cliente = datos.Clientes.Where(x => x.Id == Id).Select(x => new AgregarClienteModel()
				{
					Id = x.Id,
					Apellido = x.Apellido,
					Nombre = x.Nombre,
					email = x.Email,
					TelefonoFijo = x.TelefonoFijo,
					TelefonoMovil = x.TelefonoMovil,
					DomicilioCalle = x.DomicilioCalle,
					DomicilioNumero = x.DomicilioNumero,
					CodigoLocalidad = x.CodigoLocalidad,
					CodigoProvincia = x.CodigoProvincia,
					DomicilioCobroCalle = x.DomicilioCobroCalle,
					DomicilioCobroNumero = x.DomicilioCobroNumero,
					CodigoLocalidadCobro = x.CodigoLocalidadCobro,
					CodigoProvinciaCobro = x.CodigoProvinciaCobro,
					IdMediodeCobranza = x.IdMediosdeCobranza.Value,
					NombreMediodeCobranza = x.MediosdeCobranza.Nombre,
					Fecha = x.FechaAlta,
					Carpeta = x.Carpeta,
					CUIT = x.CUIT,
					DNI = x.DNI,
					FechaNacimiento = x.FechaNacimiento,
					Observaciones = x.ObservacionesClientes.Where(y => y.Id_Cliente == x.Id && y.Borrado == null).Select(y => y.Observacion).FirstOrDefault(),
					Productor = x.Productores.Nombre + " " + x.Productores.Apellido
				}).FirstOrDefault();

				//Agrego datos de cobranza.
				var domicilios = ObtenerDomicilioCobro(new ObtenerDomicilioCobroRequest { Cliente = _cliente });

				if (domicilios != null)
				{
					_cliente.DomicilioCobroCalle = domicilios.DocmicilioCalle;
					_cliente.DomicilioCobroNumero = domicilios.DomicilioNumero;
					_cliente.CodigoLocalidadCobro = domicilios.CodigoLocalidad;
					_cliente.CodigoProvinciaCobro = domicilios.CodigoProvincia;
				}

				return _cliente;
			}
		}

		public AgregarClienteModel ObtenerClienteAEditar(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var _cliente = datos.Clientes.Where(x => x.Id == Id).Select(x => new AgregarClienteModel()
				{
					Id = x.Id,
					Apellido = x.Apellido,
					Nombre = x.Nombre,
					email = x.Email,
					TelefonoFijo = x.TelefonoFijo,
					TelefonoMovil = x.TelefonoMovil,
					DomicilioCalle = x.DomicilioCalle,
					DomicilioNumero = x.DomicilioNumero,
					CodigoLocalidad = x.CodigoLocalidad,
					CodigoProvincia = x.CodigoProvincia,
					DomicilioCobroCalle = x.DomicilioCobroCalle,
					DomicilioCobroNumero = x.DomicilioCobroNumero,
					CodigoLocalidadCobro = x.CodigoLocalidadCobro,
					CodigoProvinciaCobro = x.CodigoProvinciaCobro,
					IdMediodeCobranza = x.IdMediosdeCobranza.Value,
					IdGrupodeCobranza = x.IdGrupodeCobranza,
					EsCabezadeGrupo = x.EsCabezadeGrupo ?? false,
					Fecha = x.FechaAlta,
					Carpeta = x.Carpeta,
					CUIT = x.CUIT,
					DNI = x.DNI,
					FechaNacimiento = x.FechaNacimiento,
					Observaciones = x.ObservacionesClientes.Where(y => y.Id_Cliente == x.Id && y.Borrado == null).Select(y => y.Observacion).FirstOrDefault()
				}).FirstOrDefault();

				//Agrego datos de cobranza.
				var domicilios = ObtenerDomicilioCobro(new ObtenerDomicilioCobroRequest { Cliente = _cliente });

				if (domicilios != null)
				{
					_cliente.DomicilioCobroCalle = domicilios.DocmicilioCalle;
					_cliente.DomicilioCobroNumero = domicilios.DomicilioNumero;
					_cliente.CodigoLocalidadCobro = domicilios.CodigoLocalidad;
					_cliente.CodigoProvinciaCobro = domicilios.CodigoProvincia;
				}

				return _cliente;
			}
		}

		public AgregarClienteResponse AgregarCliente(AgregarClienteRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var _cliente = datos.Clientes.Where(x => x.Id == request.Id).FirstOrDefault();

				if (_cliente != null)
				{
					// Retorno 0 porque la persona ya existe como usuario
					//TODO: Informar la excepción.
					return new AgregarClienteResponse { Id = 0 };
				}
				else
				{
					//Grabo los datos del cliente.
					Clientes cliente = new Clientes
					{
						Apellido = request.Apellido,
						Nombre = request.Nombre,
						Email = request.Email,
						TelefonoFijo = request.TelefonoFijo,
						TelefonoMovil = request.TelefonoMovil,
						DomicilioCalle = request.DomicilioCalle,
						DomicilioNumero = request.DomicilioNumero,
						CodigoLocalidad = request.CodigoLocalidad,
						CodigoProvincia = request.CodigoProvincia,
						IdMediosdeCobranza = request.IdMediodeCobranza,
						FechaAlta = request.Fecha,
						DomicilioCobroCalle = request.DomicilioCobroCalle,
						DomicilioCobroNumero = request.DomicilioCobroNumero,
						CodigoLocalidadCobro = request.CodigoLocalidadCobro,
						CodigoProvinciaCobro = request.CodigoProvinciaCobro,
						Carpeta = request.Carpeta,
						CUIT = request.CUIT,
						DNI = request.DNI,
						FechaNacimiento = request.FechaNacimiento,
						IdGrupodeCobranza = request.IdGrupodeCobranza,
						EsCabezadeGrupo = request.EsCabezadeGrupo
					};

					datos.Clientes.Add(cliente);
					datos.SaveChanges();

					//Grabo las observaciones.
					if (!String.IsNullOrEmpty(request.Observaciones))
					{
						ObservacionesClientes Observaciones = new ObservacionesClientes
						{
							Id_Cliente = cliente.Id,
							Observacion = request.Observaciones
						};

						datos.ObservacionesClientes.Add(Observaciones);
						datos.SaveChanges();
					}

					return new AgregarClienteResponse { Id = cliente.Id };
				}
			}
		}

		public void EliminarCliente(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var usuario = datos.Clientes.Where(x => x.Id == Id).First();

				usuario.Borrado = DateTime.Today;

				datos.SaveChanges();
			}
		}

		public ModificarClienteResponse ModificarCliente(ModificarClienteRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var _cliente = datos.Clientes.Where(x => x.Id == request.Id).FirstOrDefault();

				if (_cliente != null)
				{
					_cliente.Apellido = request.Apellido;
					_cliente.Nombre = request.Nombre;
					_cliente.Email = request.Email;
					_cliente.DNI = request.DNI;
					_cliente.CUIT = request.CUIT;
					_cliente.FechaNacimiento = request.FechaNacimiento;
					_cliente.TelefonoFijo = request.TelefonoFijo;
					_cliente.TelefonoMovil = request.TelefonoMovil;
					_cliente.DomicilioCalle = request.DomicilioCalle;
					_cliente.DomicilioNumero = request.DomicilioNumero;
					_cliente.CodigoLocalidad = request.CodigoLocalidad;
					_cliente.CodigoProvincia = request.CodigoProvincia;
					_cliente.IdMediosdeCobranza = request.IdMediodeCobranza;
					_cliente.DomicilioCobroCalle = request.DomicilioCobroCalle;
					_cliente.DomicilioCobroNumero = request.DomicilioCobroNumero;
					_cliente.CodigoLocalidadCobro = request.CodigoLocalidadCobro;
					_cliente.CodigoProvinciaCobro = request.CodigoProvinciaCobro;
					_cliente.IdGrupodeCobranza = request.IdGrupodeCobranza;
					_cliente.EsCabezadeGrupo = request.EsCabezadeGrupo;
					_cliente.Carpeta = request.Carpeta;

					var _observaciones = datos.ObservacionesClientes.Where(y => y.Id_Cliente == request.Id && y.Borrado == null).FirstOrDefault();

					if (_observaciones != null && !string.IsNullOrEmpty(request.Observaciones))
					{
						_observaciones.Observacion = request.Observaciones;
					}
					else if (!string.IsNullOrEmpty(request.Observaciones))
					{
						var observacion = new ObservacionesClientes
						{
							Id_Cliente = request.Id,
							Observacion = request.Observaciones
						};

						datos.ObservacionesClientes.Add(observacion);
					}

					datos.SaveChanges();
					return new ModificarClienteResponse { Id = _cliente.Id };
				}
				else
				{
					// Retorna 0 porque no existe, es muy probable que 
					// no se de el caso pero puede pasar
					return new ModificarClienteResponse { Id = 0 };
				}
			}
		}

		public ObtenerLocalidadesResponse ObtenerLocalidades(ObtenerLocalidadesRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				//Obtiene una ciudad determinada por Código.
				if (request.Codigo.HasValue)
				{
					return new ObtenerLocalidadesResponse()
					{
						Localidades = datos.Localidades.Where(x => x.Codigo == request.Codigo).Select(x => new LocalidadesModel
						{
							Id = x.Id,
							//Codigo = x.CodigoProvincia + "-" + x.Codigo,
							Codigo = x.Codigo.ToString(),
							Nombre = x.Nombre,
							CodigoPostal = x.CodigoPostal,
							CodigoProvincia = x.CodigoProvincia,
							NombreProvincia = x.Provincias.Nombre.ToLower()
						}).OrderBy(x => x.Nombre).ToList()
					};
				}
				//Obtiene las ciudades de una provincia determinada.
				else if (request.CodigoProvincia.HasValue)
				{
					return new ObtenerLocalidadesResponse()
					{
						Localidades = datos.Localidades.Where(x => x.CodigoProvincia == request.CodigoProvincia).Select(x => new LocalidadesModel
						{
							Id = x.Id,
							//Codigo = x.CodigoProvincia + "-" + x.Codigo,
							Codigo = x.Codigo.ToString(),
							Nombre = x.Nombre,
							CodigoPostal = x.CodigoPostal,
							CodigoProvincia = x.CodigoProvincia,
							NombreProvincia = x.Provincias.Nombre

						}).OrderBy(x => x.Nombre).ToList()
					};
				}
				//Obtiene una ciudad determinada por CP.
				else if (!string.IsNullOrEmpty(request.CodigoPostal))
				{
					return new ObtenerLocalidadesResponse()
					{
						Localidades = datos.Localidades.Where(x => x.CodigoPostal == request.CodigoPostal).Select(x => new LocalidadesModel
						{
							Id = x.Id,
							//Codigo = x.CodigoProvincia + "-" + x.Codigo,
							Codigo = x.Codigo.ToString(),
							Nombre = x.Nombre,
							CodigoPostal = x.CodigoPostal,
							CodigoProvincia = x.CodigoProvincia,
							NombreProvincia = x.Provincias.Nombre.ToLower()

						}).OrderBy(x => x.Nombre).ToList()
					};
				}
				//Obtiene todas las ciudades.
				else
				{
					return new ObtenerLocalidadesResponse()
					{
						Localidades = datos.Localidades.Select(x => new LocalidadesModel
						{
							Id = x.Id,
							//Codigo = x.CodigoProvincia + "-" + x.Codigo,
							Codigo = x.Codigo.ToString(),
							Nombre = x.Nombre,
							CodigoPostal = x.CodigoPostal,
							CodigoProvincia = x.CodigoProvincia,
							NombreProvincia = x.Provincias.Nombre.ToLower()

						}).OrderBy(x => x.Nombre).ToList()
					};
				}
			}
		}

		public ObtenerProvinciasResponse ObtenerProvincias(ObtenerProvinciasRequest request)
		{
			using (var datos = new npsEntities())
			{
				if (request.Codigo.HasValue)
				{
					return new ObtenerProvinciasResponse()
					{
						Provincias = datos.Provincias.Where(x => x.Codigo == request.Codigo).Select(x => new ProvinciasModel()
						{
							Id = x.Id,
							Codigo = x.Codigo,
							Nombre = x.Nombre
						}).OrderBy(x => x.Nombre).ToList()
					};
				}
				else
				{
					return new ObtenerProvinciasResponse()
					{
						Provincias = datos.Provincias.Select(x => new ProvinciasModel()
						{
							Id = x.Id,
							Codigo = x.Codigo,
							Nombre = x.Nombre
						}).OrderBy(x => x.Nombre).ToList()
					};
				}
			}
		}

		public List<string> ListadoReferidos(int IdCliente)
		{
			List<string> ListaAsociados = new List<string>();

			using (var datos = new Data.npsEntities())
			{
				// Busco los clientes referidos al referente de pago y agrego los nombres a la lista
				var referidos = datos.Clientes.Where(x => x.Borrado == null && x.IdGrupodeCobranza == IdCliente && x.Id != IdCliente).OrderBy(x => x.Apellido).ThenByDescending(x => x.Apellido).ToList();
				foreach (var itemref in referidos)
				{
					var asociado = (itemref.Apellido ?? "") + " " + (itemref.Nombre ?? "");
					var asociadoPolizas = (new MediosDeCobranzaService()).ObtenerListadoNumerosDePolizasCliente(itemref.Id);

					// Si el asociado posee polizas activas lo agrego
					if (asociadoPolizas.Length > 0)
					{
						ListaAsociados.Add(asociado + " ( Polizas: " + asociadoPolizas + " )");
					}
				}

				if (ListaAsociados.Count == 0)
				{
					ListaAsociados.Add("No se encontraron asociados");
				}

				return ListaAsociados;
			}
		}

		public AgregarUsuarioClienteResponse AgregarUsuarioCliente(AgregarUsuarioClienteRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var _usuario = datos.Usuarios.Where(x => x.Id == request.Id).FirstOrDefault();

				if (_usuario != null)
				{
					// Retorno 0 porque la persona ya existe como usuario
					return new AgregarUsuarioClienteResponse { Id = 0 };
				}
				else
				{
					try
					{

						var user = new Usuarios
						{
							Nombre = request.Nombre,
							Apellido = request.Apellido,
							Usuario = request.Usuario,
							Pass = request.Pass,
						};

						datos.Usuarios.Add(user);
						datos.SaveChanges();

						var nivel = new NivelesUsuarios
						{
							IdUsuario = user.Id,
							IdNivel = Niveles.Cliente.GetHashCode(),
						};

						//grabar nivel
						datos.NivelesUsuarios.Add(nivel);
						datos.SaveChanges();

						//grabar id usuario en cliente
						var cliente = datos.Clientes.Where(x => x.Id == request.IdCliente).FirstOrDefault();
						cliente.IdUsuario = user.Id;
						if (string.IsNullOrEmpty(cliente.Email))
							cliente.Email = request.Email;
						datos.SaveChanges();

						return new AgregarUsuarioClienteResponse { Id = user.Id };
					}
					catch (Exception e)
					{
						return new AgregarUsuarioClienteResponse { Id = 0 };
					}

				}
			}
		}

		public AgregarUsuarioClienteModel ObtenerUsuarioCliente(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var IdUsuario = datos.Clientes.Where(x => x.Id == Id && x.Borrado == null).Select(y => y.IdUsuario).FirstOrDefault();

				if (IdUsuario != null)
				{
					return datos.Usuarios.Where(x => x.Id == IdUsuario && x.Borrado == null).Select(x => new AgregarUsuarioClienteModel()
					{
						Id = x.Id,
						Apellido = x.Apellido,
						Nombre = x.Nombre,
						Usuario = x.Usuario
					}).FirstOrDefault();
				}
				else
					return null;
			}
		}

		public bool ExisteUsuario(string name)
		{
			using (var datos = new Data.npsEntities())
			{
				var _usuario = datos.Usuarios.Where(x => x.Usuario == name).FirstOrDefault();

				return _usuario != null ? true : false;

			}
		}

		public bool BorrarUsuarioCliente(int IdUsuario, int IdCliente)
		{
			using (var datos = new Data.npsEntities())
			{
				var _usuario = datos.Usuarios.Where(x => x.Id == IdUsuario).FirstOrDefault();

				if (_usuario != null)
				{
					_usuario.Borrado = DateTime.Today;

					var _cliente = datos.Clientes.Where(x => x.Id == IdCliente).FirstOrDefault();
					_cliente.IdUsuario = null;

					datos.SaveChanges();
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public bool BlanquearPassUsuarioCliente(int IdUsuario)
		{
			using (var datos = new Data.npsEntities())
			{
				var _usuario = datos.Usuarios.Where(x => x.Id == IdUsuario).FirstOrDefault();

				if (_usuario != null)
				{
					_usuario.Pass = NIXO_Framework.Seguridad.BlankPassword;

					datos.SaveChanges();
					return true;
				}
				else
				{
					return false;
				}
			}
		}



		#region Cobranza
		public ObtenerDomicilioReferenteResponse ObtenerDomicilioReferente(int Id)
		{
			using (var datos = new npsEntities())
			{
				return datos.Clientes.Where(x => x.Id == Id && x.Borrado == null).Select(x => new ObtenerDomicilioReferenteResponse
				{
					DomicilioCalle = x.DomicilioCobroCalle,
					DomicilioNumero = x.DomicilioCobroNumero,
					CodigoLocalidad = x.CodigoLocalidadCobro,
					CodigoProvincia = x.CodigoProvinciaCobro
				}).SingleOrDefault();
			}
		}

		public MediosDeCobranzaModel ObtenerMediosDePago()
		{
			using (var datos = new Data.npsEntities())
			{
				return new MediosDeCobranzaModel
				{
					Lista = datos.MediosdeCobranza.Where(x => x.Borrado == null).Select(y => new AgregarMediosDeCobranzaModel
					{
						Id = y.Id,
						Nombre = y.Nombre
					}).ToList()
				};
			}
		}

		public ObtenerGruposdeCobranzaResponse ObtenerGruposCobranza()
		{
			using (var datos = new Data.npsEntities())
			{
				return new ObtenerGruposdeCobranzaResponse
				{
					Lista = datos.Clientes.Where(x => x.Borrado == null && x.EsCabezadeGrupo == true).Select(y => new AgregarClienteModel
					{
						Id = y.Id,
						Nombre = (string.IsNullOrEmpty(y.Nombre.Trim()) ? y.Apellido.ToUpper().ToString() : y.Apellido.ToUpper().ToString() + ", " + y.Nombre.ToString()),
						DomicilioCobroCalle = y.DomicilioCobroCalle,
						DomicilioCobroNumero = y.DomicilioCobroNumero
					}).ToList()
				};
			}
		}

		public ObtenerDomicilioCobroResponse ObtenerDomicilioCobro(ObtenerDomicilioCobroRequest request)
		{
			if (request.Cliente.EsCabezadeGrupo || (!request.Cliente.EsCabezadeGrupo && !request.Cliente.IdGrupodeCobranza.HasValue))
			{
				return new ObtenerDomicilioCobroResponse
				{
					DocmicilioCalle = request.Cliente.DomicilioCobroCalle ?? "",
					DomicilioNumero = request.Cliente.DomicilioCobroNumero ?? "",
					CodigoLocalidad = request.Cliente.CodigoLocalidadCobro ?? null,
					CodigoProvincia = request.Cliente.CodigoProvinciaCobro ?? null
				};
			}
			else
			{
				using (var datos = new npsEntities())
				{
					var response = datos.Clientes.Where(x => x.Id == request.Cliente.IdGrupodeCobranza).Select(y => new ObtenerDomicilioCobroResponse
					{
						DocmicilioCalle = y.DomicilioCobroCalle,
						DomicilioNumero = y.DomicilioCobroNumero,
						CodigoLocalidad = y.CodigoLocalidadCobro.Value,
						CodigoProvincia = y.CodigoProvinciaCobro.Value
					}).SingleOrDefault();

					return response;
				}
			}
		}
		#endregion
	}
}