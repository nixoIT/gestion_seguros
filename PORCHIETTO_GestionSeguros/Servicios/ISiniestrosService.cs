﻿using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using PORCHIETTO_GestionSeguros.Models.Siniestros;
using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface ISiniestrosService
    {
        #region Siniestros
        SiniestroModel ObtenerSiniestros(SiniestroListadoRequest request);
        AgregarSiniestroModel ObtenerSiniestros(int Id);
        SiniestroModel ObtenerMisSiniestros(int IdCliente);
        SiniestroModel ObtenerHistoricoSiniestros(SiniestroListadoRequest request);
        AgregarSiniestroResponse AgregarSiniestro(AgregarSiniestroRequest request);
        ModificarSiniestroResponse ModificarSiniestro(ModificarSiniestroRequest request);
        void EliminarSiniestro(int Id);
        void FinalizarSiniestro(int Id);
        #endregion

        #region Requerimientos Siniestros
        RequerimientosSiniestroModel ObtenerRequerimientosSiniestro(int Id);
        void AgregarRequerimientosSiniestro(List<RequerimientoSiniestroEmpresa> request);
        #endregion

        #region Notificaciones Sinietros
        void AgregarNotificaciones(NotificacionesSiniestrosModel request);
        InformeNotificacionesSiniestros ObtenerInformeNotificacionesSiniestros(int IdSiniestro);
		InformeNotificacionesSiniestros ObtenerNotificacionesSiniestrosCliente(int IdSiniestro);

		void EliminarNotificacionSiniestro(int Id);
        #endregion
    }
}