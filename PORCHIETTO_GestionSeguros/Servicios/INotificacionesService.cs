﻿using PORCHIETTO_GestionSeguros.Models.Notificaciones;
using static PORCHIETTO_GestionSeguros.Models.Notificaciones.NotificacionesModel;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface INotificacionesService
    {
        ObtenerNotificacionesResponse ObtenerNotificaciones(ObtenerNotificacionesRequest request);
        InformeViewModel ObtenerInformeNotificaciones(ObtenerInformeRequest request);
        MarcarResueltosResponse MarcarResueltos(MarcarResueltosRequest request);
    }
}