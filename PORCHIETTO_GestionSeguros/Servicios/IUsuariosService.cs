﻿using PORCHIETTO_GestionSeguros.Data;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface IUsuariosService : IServicioBase
    {
        UsuariosModel ObtenerUsuarios();

        AgregarUsuarioModel ObtenerUsuario(int Id);

		AgregarUsuarioModel ObtenerUsuarioCliente(int Id);

		ModificarUsuarioModel ObtenerUsuarioAEditar(int Id);

        AgregarUsuarioResponse AgregarUsuario(AgregarUsuarioRequest request);

        void EliminarUsuario(int Id);

		ModificarUsuarioResponse ModificarUsuario(ModificarUsuarioRequest request);

        ModificarPassResponse ModificarPass(ModificarPassRequest request);

        ModificarPassResponse ModificarPassCliente(ModificarPassRequest request);

        ModificarPassResponse ModificarPassProductor(ModificarPassRequest request);

        string ObtenerPass(int Id);

        string ObtenerPassCliente(int Id);

        string ObtenerPassProductor(int Id);

        NivelesUsuariosModel ObtenerNivelesUsuario(int Id);

        NivelesUsuariosModel ObtenerNiveles();

        void AgregarNivelUsuario(int IdUsuario, int IdNivel);

        void EliminarNivelUsuario(int IdUsuario, int IdNivel);
    }
}