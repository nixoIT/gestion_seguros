﻿using PORCHIETTO_GestionSeguros.Models.Clientes;
using PORCHIETTO_GestionSeguros.Models.Login;

namespace PORCHIETTO_GestionSeguros.Servicios
{
    public interface ILoginService : IServicioBase
    {
        LoginResponse LoguearUsuario(LoginRequest Request);
		int ObtenerIdCliente(int Id);
		int ObtenerIdProductor(int Id);
		AgregarClienteModel ExisteCorreo(string email);
	}
}