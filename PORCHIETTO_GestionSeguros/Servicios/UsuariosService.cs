﻿using System;
using System.Linq;
using PORCHIETTO_GestionSeguros.Models.Usuarios;
using PORCHIETTO_GestionSeguros.Data;
using System.Collections.Generic;

namespace PORCHIETTO_GestionSeguros.Servicios
{
	public class UsuariosService : IUsuariosService
	{
		public UsuariosModel ObtenerUsuarios()
		{
			using (var datos = new Data.npsEntities())
			{
				var usuarios = datos.Usuarios.Where(x => x.Borrado == null).Select(x => new AgregarUsuarioModel()
				{
					Id = x.Id,
					Apellido = x.Apellido,
					Nombre = x.Nombre,
					Usuario = x.Usuario,
					Niveles = x.NivelesUsuarios.Select(y => y.Niveles.Nivel).ToList()
				});

				return new UsuariosModel() { Lista = usuarios.ToList() };
			}
		}

		public AgregarUsuarioModel ObtenerUsuario(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				return datos.Usuarios.Where(x => x.Id == Id && x.Borrado == null).Select(x => new AgregarUsuarioModel()
				{
					Id = x.Id,
					Apellido = x.Apellido,
					Nombre = x.Nombre,
					Usuario = x.Usuario,
					Niveles = x.NivelesUsuarios.Select(y => y.Niveles.Nivel).ToList()
				}).FirstOrDefault();
			}
		}

		public AgregarUsuarioModel ObtenerUsuarioCliente(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var IdUsuario = datos.Clientes.Where(x => x.Id == Id && x.Borrado == null).Select(y => y.IdUsuario).FirstOrDefault();

				if (IdUsuario != null)
				{
					return datos.Usuarios.Where(x => x.Id == IdUsuario && x.Borrado == null).Select(x => new AgregarUsuarioModel()
					{
						Id = x.Id,
						Apellido = x.Apellido,
						Nombre = x.Nombre,
						Usuario = x.Usuario,
						Niveles = x.NivelesUsuarios.Select(y => y.Niveles.Nivel).ToList()
					}).FirstOrDefault();
				}
				else
					return null;
			}
		}

		public ModificarUsuarioModel ObtenerUsuarioAEditar(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				return datos.Usuarios.Where(x => x.Id == Id && x.Borrado == null).Select(x => new ModificarUsuarioModel()
				{
					Id = x.Id,
					Apellido = x.Apellido,
					Nombre = x.Nombre,
				}).FirstOrDefault();
			}
		}

		public AgregarUsuarioResponse AgregarUsuario(AgregarUsuarioRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var _persona = datos.Usuarios.Where(x => x.Id == request.Id).FirstOrDefault();

				if (_persona != null)
				{
					// Retorno 0 porque la persona ya existe como usuario
					return new AgregarUsuarioResponse { Id = 0 };
				}
				else
				{
					var user = new Usuarios
					{
						Nombre = request.Nombre,
						Apellido = request.Apellido,
						Usuario = request.Usuario,
						Pass = request.Pass,
					};

					datos.Usuarios.Add(user);
					datos.SaveChanges();
					return new AgregarUsuarioResponse { Id = user.Id };
				}
			}
		}

		public void EliminarUsuario(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var usuario = datos.Usuarios.Where(x => x.Id == Id).First();

				usuario.Borrado = DateTime.Today;

				datos.SaveChanges();
			}
		}

		public ModificarUsuarioResponse ModificarUsuario(ModificarUsuarioRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var persona = datos.Usuarios.Where(x => x.Id == request.Id).FirstOrDefault();

				if (persona != null)
				{
					persona.Nombre = request.Nombre;
					persona.Apellido = request.Apellido;

					datos.SaveChanges();
					return new ModificarUsuarioResponse { Id = persona.Id };
				}
				else
				{
					// Retorna 0 porque no existe, es muy probable que 
					// no se de el caso pero puede pasar
					return new ModificarUsuarioResponse { Id = 0 };
				}
			}
		}

		public ModificarPassResponse ModificarPass(ModificarPassRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var persona = datos.Usuarios.Where(x => x.Id == request.Id).FirstOrDefault();

				if (persona != null)
				{
					persona.Pass = request.Pass;

					datos.SaveChanges();
					return new ModificarPassResponse { Id = persona.Id };
				}
				else
				{
					// Retorna 0 porque no existe, es muy probable que 
					// no se de el caso pero puede pasar
					return new ModificarPassResponse { Id = 0 };
				}
			}
		}

		public ModificarPassResponse ModificarPassCliente(ModificarPassRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var cliente = datos.Clientes.Where(x => x.Id == request.Id).First();
				var persona = datos.Usuarios.Where(x => x.Id == cliente.IdUsuario).FirstOrDefault();

				if (persona != null)
				{
					persona.Pass = request.Pass;

					datos.SaveChanges();
					return new ModificarPassResponse { Id = persona.Id };
				}
				else
				{
					// Retorna 0 porque no existe, es muy probable que 
					// no se de el caso pero puede pasar
					return new ModificarPassResponse { Id = 0 };
				}
			}
		}
		public ModificarPassResponse ModificarPassProductor(ModificarPassRequest request)
		{
			using (var datos = new Data.npsEntities())
			{
				var productores = datos.Productores.Where(x => x.Id == request.Id).First();
				var persona = datos.Usuarios.Where(x => x.Id == productores.IdUsuario).FirstOrDefault();

				if (persona != null)
				{
					persona.Pass = request.Pass;

					datos.SaveChanges();
					return new ModificarPassResponse { Id = persona.Id };
				}
				else
				{
					// Retorna 0 porque no existe, es muy probable que 
					// no se de el caso pero puede pasar
					return new ModificarPassResponse { Id = 0 };
				}
			}
		}

		public string ObtenerPass(int Id)
		{
			string pass = "";

			using (var datos = new Data.npsEntities())
			{
				var usuario = datos.Usuarios.Where(x => x.Id == Id).First();

				pass = usuario.Pass;
			}

			return pass;
		}

		public string ObtenerPassCliente(int Id)
		{
			string pass = "";

			using (var datos = new Data.npsEntities())
			{
				var cliente = datos.Clientes.Where(x => x.Id == Id).First();

				var usuario = datos.Usuarios.Where(x => x.Id == cliente.IdUsuario).First();

				pass = usuario.Pass;
			}

			return pass;
		}

		public string ObtenerPassProductor(int Id)
		{
			string pass = "";

			using (var datos = new Data.npsEntities())
			{
				var productores = datos.Productores.Where(x => x.Id == Id).First();

				var usuario = datos.Usuarios.Where(x => x.Id == productores.IdUsuario).First();

				pass = usuario.Pass;
			}

			return pass;
		}

		/// <summary>
		/// Obtiene los niveles de un Usuario
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public NivelesUsuariosModel ObtenerNivelesUsuario(int Id)
		{
			using (var datos = new Data.npsEntities())
			{
				var rto = datos.NivelesUsuarios.Where(x => x.IdUsuario == Id && x.Borrado == null).Select(x => new NivelModel()
				{
					Id = x.Id,
					Nombre = x.Niveles.Nivel
				});

				return new NivelesUsuariosModel() { Lista = rto.ToList() };
			}
		}

		/// <summary>
		/// Obtiene los niveles activos
		/// </summary>
		/// <returns></returns>
		public NivelesUsuariosModel ObtenerNiveles()
		{
			using (var datos = new Data.npsEntities())
			{
				var rto = datos.Niveles.Where(x => x.Borrado == null).Select(x => new NivelModel()
				{
					Id = x.Id,
					Nombre = x.Nivel
				});

				return new NivelesUsuariosModel() { Lista = rto.ToList() };
			}
		}

		public void AgregarNivelUsuario(int IdUsuario, int IdNivel)
		{
			using (var datos = new Data.npsEntities())
			{
				var nivelusuario = datos.NivelesUsuarios.Where(x => x.IdUsuario == IdUsuario && x.IdNivel == IdNivel).FirstOrDefault();

				if (nivelusuario != null)
				{
					// Retorno 0 porque la relacion ya esta en la base
					return;
				}
				else
				{

					var nu = new NivelesUsuarios()
					{
						IdUsuario = IdUsuario,
						IdNivel = IdNivel
					};

					datos.NivelesUsuarios.Add(nu);
					datos.SaveChanges();
				}
			}
		}

		public void EliminarNivelUsuario(int IdUsuario, int IdNivel)
		{
			using (var datos = new Data.npsEntities())
			{
				var nivel = datos.NivelesUsuarios.Where(x => x.Id == IdNivel && x.IdUsuario == IdUsuario).First();

				nivel.Borrado = DateTime.Today;

				datos.SaveChanges();
			}
		}
	}
}