﻿using System.Web.Mvc;
using System.Web.Routing;

namespace PORCHIETTO_GestionSeguros
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Reporte",
                url: "{controller}/{action}/{idMedio}/{idProductor}",
                defaults: new { controller = "Home", action = "Index", idMedio = UrlParameter.Optional, IdProductor = UrlParameter.Optional }
            );

        }
    }
}
