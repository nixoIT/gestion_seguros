﻿using System.Web.Optimization;

namespace PORCHIETTO_GestionSeguros
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Optimización. Comentar para debuguear .js
            BundleTable.EnableOptimizations = false;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.3.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/blockUI").Include(
                        "~/Scripts/jquery.blockUI.js"));

            bundles.Add(new ScriptBundle("~/bundles/Gentelella").Include(
                "~/Scripts/fastclick.js",
                "~/Libs/Gentelella/js/custom.js"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/Coberturas").Include(
                        "~/ViewScripts/Coberturas/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/Home").Include(
                        "~/ViewScripts/Home/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/Polizas").Include(
                        "~/ViewScripts/Polizas/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/Ramas").Include(
                        "~/ViewScripts/Ramas/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/CargarPolizas").Include(
                        "~/ViewScripts/Polizas/CargarPoliza.js"));

            bundles.Add(new ScriptBundle("~/bundles/Empresas").Include(
                        "~/ViewScripts/Empresas/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/Productores").Include(
                        "~/ViewScripts/Productores/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/Legajos").Include(
                        "~/ViewScripts/Legajos/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/MediosDeCobranzas").Include(
                        "~/ViewScripts/MediosDeCobranzas/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/ReportesMediosDePago").Include(
                        "~/ViewScripts/MediosDeCobranzas/Reportes.js"));

            bundles.Add(new ScriptBundle("~/bundles/Clientes").Include(
                        "~/ViewScripts/Clientes/Index.js"));

			bundles.Add(new ScriptBundle("~/bundles/Usuarios").Include(
						"~/ViewScripts/Usuarios/Index.js"));

			bundles.Add(new ScriptBundle("~/bundles/Niveles").Include(
                        "~/ViewScripts/Niveles/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/Siniestros").Include(
                        "~/ViewScripts/Siniestros/Index.js"));

            bundles.Add(new ScriptBundle("~/bundles/NotificacionesSiniestros").Include(
                        "~/ViewScripts/Siniestros/Notificaciones.js",
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/NotificacionesPolizas").Include(
                        "~/ViewScripts/Polizas/Notificaciones.js",
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/TipoSiniestros").Include(
                        "~/ViewScripts/Siniestros/ListadoTipoSiniestros.js"));

            bundles.Add(new ScriptBundle("~/bundles/Requerimientos").Include(
                        "~/ViewScripts/Empresas/Requerimientos.js"));

            bundles.Add(new ScriptBundle("~/bundles/AgregarCliente").Include(
                        "~/ViewScripts/Clientes/AgregarCliente.js",
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/DetalleCliente").Include(
                        "~/ViewScripts/Clientes/DetalleCliente.js"));

            bundles.Add(new ScriptBundle("~/bundles/AgregarPoliza").Include(
                        "~/ViewScripts/Polizas/AgregarPoliza.js",
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/AgregarSiniestro").Include(
                        "~/ViewScripts/Siniestros/AgregarSiniestro.js",
                        "~/Scripts/moment-with-locales.min.js",
                        "~/Scripts/bootstrap-datetimepicker.js"));

            #region Bootstrap-Table
            bundles.Add(new ScriptBundle("~/bundles/BootstrapTable").Include(
                       "~/Scripts/bootstrap-table.js",
                       "~/Scripts/bootstrap-table-es-AR.js",
                       "~/Scripts/Common.js"));

            bundles.Add(new StyleBundle("~/Content/BootstrapTable").Include(
                      "~/Content/bootstrap-table.css"));
            #endregion

            #region Bootstrap-Select
            bundles.Add(new StyleBundle("~/Content/SelectPicker").Include(
                      "~/Content/bootstrap-select.css"));

            bundles.Add(new ScriptBundle("~/bundles/SelectPicker").Include(
                      "~/Scripts/bootstrap-select.js"));
            #endregion

            #region Notificaciones
            bundles.Add(new ScriptBundle("~/bundles/Notify").Include(
                        "~/Scripts/bootstrap-notify.min.js"));

            bundles.Add(new StyleBundle("~/Content/Animate").Include(
                      "~/Content/animate.css"));

            bundles.Add(new ScriptBundle("~/bundles/Notificaciones").Include(
                        "~/ViewScripts/Notificaciones/Index.js"));
            #endregion

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/DatePicker").Include(
                      "~/Content/bootstrap-datetimepicker.min.css"));

            bundles.Add(new StyleBundle("~/bundles/ExportXLS").Include(
                      "~/Scripts/tableExport.min.js"));
        }
    }
}
