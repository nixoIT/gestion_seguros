﻿using System.Web.Mvc;

namespace PORCHIETTO_GestionSeguros
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
