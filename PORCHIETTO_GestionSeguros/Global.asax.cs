﻿using PORCHIETTO_GestionSeguros.Infrastructure.IoC;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PORCHIETTO_GestionSeguros
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Configuración del container IoC
            IoC.InitializeInstance();

            // Delega manejo de instancias de los controllers al container IoC
            var ControllerFactory = new WindsorControllerFactory(IoC.Instance);
            ControllerBuilder.Current.SetControllerFactory(ControllerFactory);
        }

        protected void Application_End()
        {
            this.TeardownContainer();
        }

        private void TeardownContainer()
        {
            IoC.Instance.Dispose();
        }

        public void Application_PreRequestHandlerExecute(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;

            // use an if statement to make sure the request is not for a static file (js/css/html etc.)
            if (context != null)
            {
                // use context to work the session
                //Session["username"] = string.Empty;
                //Session["adminstatus"] = null;
            }
        }
    }
}
